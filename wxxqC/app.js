//app.js
var app = getApp()
App({
  globalData: {
    userInfo: null,
    isIphoneX: false,
    code: null,
    web_url: "", //
    web_url1: "http://images.haoyizhu.cn", //图片和链接 地址域名
    com_url: 'https://miniprogram.haoyizhu.cn/uhotel/',
    share_url:"/wxxq/images/home/share.jpg",//转发图片
    hotel:10044,//酒店id
    wxcfgid:9,//微信wxcfgid
    roomdetail:2,//房间详情页优惠券与余额显示控制开关 1 显示 2 不显示
    myself:2,//个人中心 充值按钮开关/订单开关/优惠券开关
    balance:2,//账户余额页面充值按钮开关  1 显示  2 不显示
  },

  onLaunch: function () {
    var web_url = "https://miniprogram.haoyizhu.cn/uhotel/rest"; //接口地址域名
    this.globalData.web_url = web_url
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    var that = this;
    const updateManager = wx.getUpdateManager()  
    updateManager.onCheckForUpdate(function (res) {    
      // 请求完新版本信息的回调    
      console.log(res.hasUpdate)
    }) 
    updateManager.onUpdateReady(function () {    
      // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启 
      updateManager.applyUpdate()   
    }) 
    updateManager.onUpdateFailed(function () {    
      // 新的版本下载失败    
      wx.showModal({        
        title: '更新提示',        
        content: '新版本下载失败',        
        showCancel:false    
      })
    })
  
  },

  onShow: function () {
    let that = this;
    wx.getSystemInfo({
      success: res => {
        // console.log('手机信息res'+res.model)
        let modelmes = res.model;
        if (modelmes.search('iPhone X') != -1) {
          that.globalData.isIphoneX = true
        }
      }
    })
  },
})
