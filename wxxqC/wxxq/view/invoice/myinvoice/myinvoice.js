// wxxq/view/invoice/addres/addres.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr: [],
    flag:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/invoice/getInvoiceTitleInfo"; //今日
    var sessionId = wx.getStorageSync('sessionId');
    //今日可用
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      success: function (res) {
        //console.log(res)
        if (res.data.state == 1 && res.data.data.length != 0) {
          that.setData({
            arr: res.data.data,
            flag:false,
          })
        }else{
          that.setData({
            arr: [],
            flag: true,
          })
        }
      },
      fail: function (e) {

      }
    });
    wx.hideLoading();
  },
  onShow: function () {
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/invoice/getInvoiceTitleInfo"; //今日
    var sessionId = wx.getStorageSync('sessionId');
    //今日可用
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      success: function (res) {
        //console.log(res)
        if (res.data.state == 1 && res.data.data.length != 0) {
          that.setData({
            arr: res.data.data,
            flag: false,
          })
        } else {
          that.setData({
            arr:[],
            flag: true,
          })
        }
      },
      fail: function (e) {

      }
    });
  },
  //返回上一页
  goBack: function (e) {
    wx.setStorage({
      key: 'TITLE',
      data: e.currentTarget.dataset.js,
    })
    wx.navigateTo({
      url: '/wxxq/view/invoice/addresDeatil/deatil',
    })
  },
  add: function () {
    wx.navigateTo({
      url: '../addInvoice/addinvoice',
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})