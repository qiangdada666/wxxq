// wxxq/view/invoice/home/home.wxhl.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title:"增值税电子普通发票",
    checkImg:"../../../image/icon1.png",
    checkedImg:'../../../image/icon2.png',
    flag1:1, //企业单位
    flag2:0,//个人
    money:12, //发票金额
    showModalStatus: true,
    isFold: false,
    textVlaue:'',
    orderId:'',
    n1:0,
    n2:0,
    n3:0,
    n4:0,
    type:1,// 发票类型：1 ：为企业  2 ：为个人
    account: '',
    address: '',
    bank: "",
    hotelId: '',
    phone: '',
    taxNo: '',
    title: '',
    mobile:'',
    email:'',
    f : true,//发票开关
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var hotelId = app.globalData.hotel;
    var that = this;
    that.setData({
      money:options.price,
      orderId:options.orderId,
      hotelId: hotelId,
    })
      wx.getStorage({
        key: 'Invoice',
        success: function(res) {
          if (res.bank != undefined) {
            console.log(res.bank)
          }
          if (res.bank != undefined && res.phone != undefined && res.account != undefined && res.address != undefined){
            that.setData({
              textVlaue: "共4项 已填写4项"
            })
          } else if (res.bank != undefined && res.phone != undefined && res.account != undefined) {
            that.setData({
              textVlaue: "共4项 已填写3项"
            })
          } else if (res.bank != undefined && res.phone != undefined) {
            that.setData({
              textVlaue: "共4项 已填写2项"
            })  
          } else if (res.bank != undefined) {
            that.setData({
              textVlaue: "共4项 已填写1项"
            })
          }else{
            textVlaue: ""
          }
          if (res.data.type == 1) {
            that.setData({
              flag1: 1, //企业单位
              flag2: 0,//个人
              type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
              account: res.data.account,
              address: res.data.address,
              bank: res.data.bank,
              
              phone: res.data.phone,
              taxNo: res.data.taxNo,
              title: res.data.title,
              id:res.data.id,
            })
          } else {
            that.setData({
              flag1: 0, //企业单位
              flag2: 1,//个人
              type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
              account: res.data.account,
              address: res.data.address,
              bank: res.data.bank,
              
              phone: res.data.phone,
              taxNo: res.data.taxNo,
              title: res.data.title,
              id: res.data.id,
            })
          }
        },
      })
  },
  onShow: function (options) {
    var that = this;
    wx.getStorage({
      key: 'Invoice',
      success: function (res) {
       if(res.bank == ""){
         console.log(res.bank)
       }
        if (res.bank != undefined && res.phone != undefined && res.account != undefined && res.address != undefined) {
          that.setData({
            textVlaue: "共4项 已填写4项"
          })
        } else if (res.bank != undefined && res.phone != undefined && res.account != undefined) {
          that.setData({
            textVlaue: "共4项 已填写3项"
          })
        } else if (res.bank != undefined && res.phone != undefined) {
          that.setData({
            textVlaue: "共4项 已填写2项"
          })
        } else if (res.bank != undefined) {
          that.setData({
            textVlaue: "共4项 已填写1项"
          })
        } else {
          textVlaue: ""
        }
        if (res.data.type == 1) {
          that.setData({
            flag1: 1, //企业单位
            flag2: 0,//个人
            type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
            account: res.data.account,
            address: res.data.address,
            bank: res.data.bank,
            phone: res.data.phone,
            taxNo: res.data.taxNo,
            title: res.data.title,
            id: res.data.id,
          })
        } else {
          that.setData({
            flag1: 0, //企业单位
            flag2: 1,//个人
            type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
            account: res.data.account,
            address: res.data.address,
            bank: res.data.bank,
            phone: res.data.phone,
            taxNo: res.data.taxNo,
            title: res.data.title,
            id: res.data.id,
          })
        }
      },
    })
  },
  checkEmail:function(e){
    this.setData({
      email: e.detail.value
    })
  },
  checkp:function(e){
    this.setData({
      mobile: e.detail.value
    })
  },
  //企业
  check1:function(){
    this.setData({
      flag1:1,
      flag2:0,
      type:1,
      account: '',
      address: '',
      bank: "",
     
      phone: '',
      taxNo: '',
      title: '',
      textVlaue: '',
      email:'',

    })
    wx.removeStorage({
      key: 'Invoice'
    })
  },
  //个人
  check2:function(){
    this.setData({
      flag1:0,
      flag2:1,
      type:2,
      account: '',
      address: '',
      bank: "",
      
      phone: '',
      taxNo: '',
      title: '',
      textVlaue: '',
    })
    wx.removeStorage({
      key: 'Invoice'
    })
  },
  //失去焦点
  blur1:function(e){
    if(e.detail.value){
      this.setData({
        n1:1
      })
    }else{
      this.setData({
        n1:0
      })
    }
  },
  blur2: function (e) {
    if (e.detail.value) {
      this.setData({
        n2: 1
      })
    } else {
      this.setData({
        n2: 0
      })
    }
  },
  blur3: function (e) {
    if (e.detail.value) {
      this.setData({
        n3: 1
      })
    } else {
      this.setData({
        n3: 0
      })
    }
  },
  blur4: function (e) {
    if (e.detail.value) {
      this.setData({
        n4: 1
      })
    } else {
      this.setData({
        n4: 0
      })
    }
  },
  // 更多信息由下向上划出
  powerDrawer: function (e) {
    var currentStatu = '';

    if (this.data.showModalStatus) {
      currentStatu = 'close';
    } else {
      currentStatu = 'open';
    }
    this.util(currentStatu)
    // this.setData({
    //   n4: 0,
    //   n3:0,
    //   n2:0,n1:0
    // })
  },
  // 更多信息确定保存由下向上划出
  powerOk: function (e) {
    let currentStatu = '';
    if (this.data.showModalStatus) {
      currentStatu = 'close';
    } else {
      currentStatu = 'open';
    }
    this.util(currentStatu)
    //更多信息综合
    // let num = this.data.n1+this.data.n2+this.data.n3+this.data.n4;
    // if(num > 0){
    //   this.setData({
    //     textVlaue:"共4项 已填写"+num+"项"
    //   })
    // }else{
    //   this.setData({
    //     textVlaue: ''
    //   })
    // }
  },
  util: function (currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例 
    var animation = wx.createAnimation({
      duration: 200, //动画时长
      timingFunction: "linear", //线性
      delay: 0 //0则不延迟
    });

    // 第2步：这个动画实例赋给当前的动画实例
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画
    setTimeout(function () {
      // 执行第二组动画：Y轴不偏移，停
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象
      this.setData({
        animationData: animation
      })

      //关闭抽屉
      if (currentStatu == "close") {
        this.setData({
          showModalStatus: false,
          isFold: false,
        });
      }

      // 显示抽屉
      if (currentStatu == "open") {
        this.setData({
          showModalStatus: true,
          isFold: true,
        });
      }

    }.bind(this), 200)


  },
  //提交申请
  submit:function(){
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/invoice/addInvoiceApplicationInfo";
    let amount = that.data.money;
    let email = that.data.email;
    let invoiceNo = '';
    let hotelId = that.data.hotelId;
    let mobile = that.data.mobile;
    let orderId = that.data.orderId;
    let titleId = that.data.id;
    if (!mobile) {
      wx.showToast({
        title: '请填写手机号',
        icon: "none",
        duration: 2000
      })
      return;
    }else{
      var reg = new RegExp('^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$');
      var phoneVar = reg.test(mobile);
      if(!phoneVar){
        wx.showToast({
          title: '手机号格式有误',
          icon: "none",
          duration: 2000
        })
        return;
      }
    }
    if (!email) {
      wx.showToast({
        title: '请填写邮箱号',
        icon: "none",
        duration: 2000
      })
      return;
    } else {
      var reg = new RegExp('^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$');
      var emailVar = reg.test(email);
      if (!phoneVar) {
        wx.showToast({
          title: '邮箱格式有误',
          icon: "none",
          duration: 2000
        })
        return;
      }
    }
    if (!titleId) {
      wx.showToast({
        title: '请填写发票信息',
        icon: "none",
        duration: 2000
      })
      return;

    }
    //发票
    if(that.data.f){
      that.setData({
        f:false,
      })
      c._get({
        url: web_url1, //仅为示例，并非真实的接口地址
        data: {
          amount: amount,
          email: email,
          invoiceNo: '',
          hotelId: hotelId,
          mobile: mobile,
          orderId: orderId,
          titleId: that.data.id,
        },
        success: function (res) {
          // console.log(res)
          if (res.data.state == 1) {
            wx.redirectTo({
              url: '../submitInvoice/submitInvoice?type=&orderId=' + that.data.orderId,
            })
          } else {

            wx.redirectTo({
              url: '../submitInvoice/submitInvoice?type=3&orderId=' + that.data.orderId,
            })

          }

        },
        fail: function (e) {
          wx.redirectTo({
            url: '../submitInvoice/submitInvoice?type=3&orderId=' + that.data.orderId,
          })
        }
      });
    }
  },
  //地址列表
  goAddress:function(){
      wx.navigateTo({
        url: '../addres/addres',
      })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})