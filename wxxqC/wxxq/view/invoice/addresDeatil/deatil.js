// wxxq/view/invoice/addresDeatil/deatil.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
     
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'TITLE',
      success: function (res) {
        console.log(res.data)
        if (res.data.type == 1) {
          that.setData({
            flag1: 1, //企业单位
            flag2: 0,//个人
            type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
            account: res.data.account,
            address: res.data.address,
            bank: res.data.bank,
            phone: res.data.phone,
            taxNo: res.data.taxNo,
            title: res.data.title,
            id: res.data.id,
            flag: true,
          })
        } else {
          that.setData({
            flag1: 0, //企业单位
            flag2: 1,//个人
            type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
            account: res.data.account,
            address: res.data.address,
            bank: res.data.bank,
            phone: res.data.phone,
            taxNo: res.data.taxNo,
            title: res.data.title,
            id: res.data.id,
            flag:false,
          })
        }

      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {
    var that = this;
    wx.getStorage({
      key: 'TITLE',
      success: function (res) {
        console.log(res.data)
        if (res.data.type == 1) {
          that.setData({
            flag1: 1, //企业单位
            flag2: 0,//个人
            type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
            account: res.data.account,
            address: res.data.address,
            bank: res.data.bank,
            hotelId: res.data.hotelId,
            phone: res.data.phone,
            taxNo: res.data.taxNo,
            title: res.data.title,
            id: res.data.id,
            flag: true,
          })
        } else {
          that.setData({
            flag1: 0, //企业单位
            flag2: 1,//个人
            type: res.data.type,// 发票类型：1 ：为企业  2 ：为个人
            account: res.data.account,
            address: res.data.address,
            bank: res.data.bank,
            hotelId: res.data.hotelId,
            phone: res.data.phone,
            taxNo: res.data.taxNo,
            title: res.data.title,
            id: res.data.id,
            flag: false,
          })
        }

      },
    })
  },
  to:function(){
    wx.navigateTo({
      url: '../redact/redact',
    })
  },
  del:function(){
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/invoice/deleteInvoiceTitleInfo"; //今日
    //今日可用
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data:{
        id:that.data.id
      },
      success: function (res) {
        //console.log(res)
        if (res.data.state == 1) {
          wx.navigateBack({
            delta:1,
          })
        }
      },
      fail: function (e) {

      }
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})