// wxxq/view/invoice/redact/redact.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkImg: "../../../image/icon1.png",
    checkedImg: '../../../image/icon2.png',
    flag1: 1, //企业单位
    flag2: 0,//个人
    type: 1,// 发票类型：1 ：为企业  2 ：为个人
    account: '',
    address: '',
    bank: "",
    hotelId: app.globalData.hotelId,
    phone: '',
    taxNo: '',
    title: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
  },
  check1: function () {
    this.setData({
      flag1: 1,
      flag2: 0,
      type: 1,
    })
  },
  check2: function () {
    this.setData({
      flag1: 0,
      flag2: 1,
      type: 2,
    })
  },
  //input输入存至
  addtitle: function (e) {
    this.setData({
      title: e.detail.value
    })

  },
  addtaxNo: function (e) {
    this.setData({
      taxNo: e.detail.value
    })

  },
  bank: function (e) {
    this.setData({
      bank: e.detail.value
    })

  },
  account: function (e) {
    this.setData({
      account: e.detail.value
    })

  },
  phone: function (e) {
    this.setData({
      phone: e.detail.value
    })

  },
  address: function (e) {
    this.setData({
      address: e.detail.value
    })

  },
  //添加
  submit_ok: function () {
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/invoice/addInvoiceTitleInfo";
    let account = that.data.account;
    let address = that.data.address;
    let bank = that.data.bank;
    let hotelId = that.data.hotelId;
    let phone = that.data.phone;
    let taxNo = that.data.taxNo;
    let title = that.data.title;
    let type = that.data.type;
    let patrn = /^[0-9A-Z]+$/;
    if (!taxNo && type == 1) {
      wx.showToast({
        title: '请填写发票税号',
        icon: "none",
        duration: 2000
      })
      return;
    }
    console.log((taxNo.length == 15 && patrn.test(taxNo)))
    console.log((taxNo.length == 18 && patrn.test(taxNo)))
    console.log((taxNo.length == 20 && patrn.test(taxNo)))
    if(type == 1){
      if ((taxNo.length == 15 && patrn.test(taxNo)) || (taxNo.length == 18 && patrn.test(taxNo)) || (taxNo.length == 20 && patrn.test(taxNo))) {

      } else {
        wx.showToast({
          title: '请正确填写税号',
          icon: "none",
          duration: 2000
        })
        return;
      }
    }
    if (!title) {
      wx.showToast({
        title: '请填写发票抬头',
        icon: "none",
        duration: 2000
      })
      return;

    }
    //添加
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        account: that.data.account,
        address: that.data.address,
        bank: that.data.bank,
        hotelId: that.data.hotelId,
        phone: that.data.phone,
        taxNo: that.data.taxNo,
        title: that.data.title,
        type: type,
      },
      success: function (res) {
        if (res.data.state == 1) {
          wx.navigateBack({
            delta: 1,
          })
        }
      },
      fail: function (e) {

      }
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})