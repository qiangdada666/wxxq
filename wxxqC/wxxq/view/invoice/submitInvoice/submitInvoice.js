// wxxq/view/invoice/submitInvoice/submitInvoice.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
      titleName:"开票申请已提交",
      hImg:'../../../image/ok.png', //失败图 ../../../image/failure.png
      orderNum:'', //订单号
      titleClass:"单位名称",
      tel:"",
      hotel:"",
      taxNo:'',
      title:'',
      type:0,// 0:提交  1：提交成功  2：提交失败  3：开票失败
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    var o = options.orderId;
    var that = this;
    wx.getStorage({
      key: 'Invoice',
      success: function(res) {
        that.setData({
          taxNo:res.data.taxNo,
          orderNum: o,
          title:res.data.title
        })
      },
    })
    wx.getStorage({
      key: 'hotelName',
      success: function (res) {
        that.setData({
          hotel: res.data,
        
        })
      },
    })
    wx.getStorage({
      key: 'telphone1',
      success: function (res) {
        console.log(res)
        that.setData({
          tel: res.data,

        })
      },
    })
    if(options.type != ""){
      that.setData({
        titleName: "开票申请失败",
        hImg:'../../../image/failure.png',
        type:3,
      })

    }else{
      
      var web_url = app.globalData.web_url;
      var web_url1 = web_url + "/invoice/getInvoiceApplicationStatus";
      //今日可用
      c._get({
        url: web_url1, //仅为示例，并非真实的接口地址
        data: {
          orderId: o,
        },
        success: function (res) {
          console.log(res)
          if(res.data.data.openType == 1){
            that.setData({
              orderNum: o,
              taxNo: res.data.data.taxNo,
              title:res.data.data.title
            })
          }else{
            that.setData({
              orderNum: o,
              taxNo: res.data.data.taxNo,
              title: res.data.data.title
            })
          }
          if(res.data.data.status == 1){
            that.setData({
              titleName: "开票申请已完成",
              type: res.data.data.status,
            })
          }
          if (res.data.data.status == 0) {
            that.setData({

              type: res.data.data.status,
            })
          }
          if (res.data.data.status == 2) {
            that.setData({
              titleName: "开票申请已失败",
              type: res.data.data.status,
            })
          }

        },
        fail: function (e) {

        }
      });
    }
    
  },
  toOrder:function(){
    wx.removeStorage({
      key: 'Invoice',
      success: function(res) {
        wx.redirectTo({
          url: '/wxxq/view/mine/order/allOrder?index=3',
        })
      },
    })
  },
  goB:function(){
    wx.navigateBack({
      delta:1,
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})