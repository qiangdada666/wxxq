var c = require("../../common/common.js");
var time = require("../../../../utils/util.js");
var app = getApp()
Page({
  /**
   * 页面的初始数据
   */
 
  data: {
  	roomNum: '',
    invoice_type: 1,
    items: [
      { name: '普票', value: '普票', checked: 'true'},
      { name: '专票', value: '专票'},
      { name: '电子发票', value: '电子发票' }
    ],
    times: '',
    invoiceTitle: '',
    taxNumber: '',
    companyAddress: '',
    telephone: '',
    bankName: '',
    bankAccount: '',
    email: '',
    checkoutTime: '',
    u_mailbox: '',
    p_t:'入住人姓名，每房一人',
    p_p: "用于接收预定通知短信",
    m_username: "",
    hotelName: "",
    active: true,
  
    "isName0": true,
    "isName1": true,
    "isName2": true,
    isPhone: false,

    u_num: 1,
    u_num1: 0,

    roomType: "",
    num: "",
    limit: "",

    day: "",
    roomNum: "",

    checkInDate: "",
    checkOutDate: "",

    // 用户信息start
    jiantou: "../../../images/home/jt@2x.png",
    telphone: "../../../images/mine/telphone@2x.png",
    top: "../../../images/common/top@2x.png",
    down: "../../../images/common/bottom@2x.png",
    showModalStatus: false,
    isFold: false,
    discount: 0,
    vipDiscount: "",
    cardDiscount: 0,

    isShow: 1,
    isUsable: 1,
    noCard: "../../../images/mine/discount@2x.png",
    cardList: [],
    hotelId: "",
    roomId: "",
    sum: "",
    u_num: 1,
    coupon: "",
    //房间信息
    allmsg: '',
    //应付价格
    pay_price: 0,
    firstValue: 0,
    useBalance: 0,
    vipMoney: 0,
    //
    couponType: "小程序专用",
    endTime: '',
    //优惠券id
    coupon_id: '',
    // 轮播图默认地址
    picUrl: "",
    flage:true,
  },
  /**
   * 生命周期函数--监听页面加载
   * 
   */
  chooseItems: function() {
    var that = this;
    var h_Id = app.globalData.hotel;
    var type = wx.getStorageSync('type');
    c._get({
      url: app.globalData.web_url + "/hotels/" + h_Id,
      data: {
        type: type,
      },
      method: "GET",
      success: function (res) {
        console.log(res)
        if (res.data.success == true) {
          var invoices = res.data.data.invoiceType
          //var invoices = [1,3]
          var items = []
          if (invoices == null) {
            var invoice_type = 0
          } else {
            console.log(invoices.indexOf(1));
            if (invoices.indexOf("1") != -1) {
              items.push({ name: '普票', value: '普票' })
            }
            if (invoices.indexOf("3") != -1) {
              items.push({ name: '专票', value: '专票' })
            }
            if (invoices.indexOf("2") != -1) {
              items.push({ name: '电子发票', value: '电子发票' })
            }
            items[0].checked = 'true';
            var invoice_type = invoices[0]
          }
          that.setData({
            items: items,
            invoice_type: invoice_type,
          })
        }
      }
    })
  },

  //  点击时间组件确定事件  
  bindTimeChange: function (e) {
    this.setData({
      times: e.detail.value
    })
  },

  bindroomNumChange: function (e) {
    this.setData({
      roomNum: e.detail.value
    })
  },

  submit: function(e){
    var that = this;
    var roomNo = that.data.roomNum;
    var invoiceTitle = that.data.invoiceTitle;
    var invoiceType = that.data.invoice_type;
    var check_tel = /^[1][3,4,5,6,7,8,9][0-9]{9}$/;//手机号验证正则；
    var check_email = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/
    var check_roomNo = /^\d{1,5}$/; 
    var sessionId = wx.getStorageSync('sessionId');//获取sessionID
    var u_name = that.data.m_username;
    var u_telphone = that.data.u_telphone;//订单人手机
    var telephone = that.data.telephone;//订单人手机
    //    var u_telphone = 15011433925//订单人手机
    var u_mailbox = that.data.u_mailbox;//邮箱
    var taxNumber = that.data.taxNumber;//退款
    var companyAddress = that.data.companyAddress;
    var bankName = that.data.bankName;//总计
    var bankAccount = Number(that.data.bankAccount);//支付类型
    var time = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkinTime = time.checkInDate;
    var email = that.data.u_mailbox;
    var checkoutTime = that.data.times;
    var formId = e.detail.formId;
    //校验
    var name_reg = /^[a-zA-Z\u4e00-\u9fa5\.]+$/;
    if (!name_reg.test(u_name)) {//姓名校验
      wx.showToast({//提示框
        title: '请输入正确的入住人姓名',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (!check_tel.test(u_telphone)) {//姓名校验
      wx.showToast({//提示框
        title: '请输入正确的手机号',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (!check_roomNo.test(roomNo)){
      wx.showToast({//提示框
        title: '请输入正确的房间号',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (checkoutTime == "") {
      wx.showToast({//提示框
        title: '退房时间不能为空',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (invoiceTitle == "") {
      wx.showToast({//提示框
        title: '发票抬头不能为空',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (taxNumber == "") {
      wx.showToast({//提示框
        title: '税号不能为空',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if(invoiceType=="3"){
      if (companyAddress == "") {
        wx.showToast({//提示框
          title: '单位地址不能为空',
          icon: 'none',
          duration: 2000
        })
        return;
      }
      if (telephone == "") {
        wx.showToast({//提示框
          title: '联系电话不能为空',
          icon: 'none',
          duration: 2000
        })
        return;
      }
      if (bankName == "") {
        wx.showToast({//提示框
          title: '开户银行不能为空',
          icon: 'none',
          duration: 2000
        })
        return;
      }
      if (bankAccount == "") {
        wx.showToast({//提示框
          title: '银行账户不能为空',
          icon: 'none',
          duration: 2000
        })
        return;
      }
      

    } else if (invoiceType == "2"){
      if (email == "") {
        wx.showToast({//提示框
          title: '电子邮箱不能为空',
          icon: 'none',
          duration: 2000
        })
        return;
      }
    }
    if(that.data.flage){
      that.setData({
        flage: false,
      })
      c._post({
        url: app.globalData.web_url + "/memberInvoice",
        data: {
          //房间号
          roomNo: roomNo,
          //折扣后价格
          title: invoiceTitle,
          //使用余额的额度
          type: invoiceType,
          //入住人
          name: u_name,
          //电话
          mobile: u_telphone,
          //邮箱
          dutyNo: taxNumber,
          //formid
          address: companyAddress,
          //房间数
          companyTel: telephone,
          //入住时间
          bankName: bankName,
          //结束时间
          bankNo: bankAccount,
          //折扣价格
          email: email,
          //退款
          checkoutTime: checkoutTime,
          //房间名字
          formId: formId,
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          'sessionId': sessionId,
          // 默认值
        },
        method: "POST",
        success: function (res) {
          that.setData({
            flage:true,
          })
          if (res.data.success == true) {
            wx.redirectTo({
              url: '/wxxq/view/invoice/result/result?resultType=1',
            })
          } else {
            wx.redirectTo({
              url: '/wxxq/view/invoice/result/result?resultType=0',
            })
            return false;
          }
        }
      })
    }
    
  },

  chooseInvoiceTitle() {
    wx.chooseInvoiceTitle({
      success: (res) => {
        console.log(res)
        this.setData({
          type: res.type,
          invoiceTitle: res.title,
          taxNumber: res.taxNumber,
          companyAddress: res.companyAddress,
          telephone: res.telephone,
          bankName: res.bankName,
          bankAccount: res.bankAccount
        })
      },
      fail: (err) => {
        console.error(err)
      }
    })
  },

  
  changeType: function (e) {
    var id = e.target.dataset.index;
    var selected = e.target.dataset.checks ? false : true;
    var invoice_type = 1;
    if(e.detail.value == '普票'){
      invoice_type = 1
    } else if (e.detail.value == '专票'){
      invoice_type = 3
    } else if (e.detail.value == '电子发票') {
      invoice_type = 2
    }
    this.setData({
      invoice_type: invoice_type
    })
  },

  onLoad: function(options) {
    this.chooseItems();
    var web_url = app.globalData.web_url;
    //判断授权
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
        } else {
          //授权
          wx.authorize({
            scope: 'scope.userInfo',
            success: function () {
              wx.redirectTo({
                url: "/wxxq/view/invoice/order/order"
              })
            },
            fail: function () {
              wx.redirectTo({
                url: "/wxxq/view/home/author/author"
              })
            }
          })
          return;
        }
      }
    })
    c._is_login({
      web_url: web_url,
      success: function (e) {
      }
    })
    var status = wx.getStorageSync('type');//登入状态1为未登录
    console.log(status)
    if (status == 1) {//未登录
      wx.redirectTo({
        url: '/wxxq/view/home/login/index?nav=5',
      })
    }
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
  bindPayChange: function(e) {
    var that = this;
    that.setData({
      pay_type: e.detail.value,
    })
  },
  bindNameFocus: function(e) {
    var index = e.currentTarget.dataset.index;
    var tipsshow22 = this.data.isPhone? "none": "block"
    var isName11 = this.data.isName1? "none": "block"
    var isName22 = this.data.isName2? "none": "block"
    var isName33 = this.data.isName3? "none": "block"
    var isName = "isName" + index
    console.log(isName)
    this.setData({
      [isName]: false,
      config:{
        tipsshow1: "none",
        tipsshow2: tipsshow22
      },
      isName:{
        isName1: isName11,
        isName2: isName22,
        isName3: isName33,
      }
    })
  },
  //姓名start
  bindUsernameChange: function(e) {
    console.log(e)
    this.setData({
      m_username: e.detail.value
    })
  },
  bindPhoneFocus: function(e) {
    var tipsshow11 = this.data.isName? "none": "block"
    this.setData({
      isPhone: true,
      config:{
        tipsshow1: tipsshow11,
        tipsshow2: "none"
        //tipsshow2: "none"
      },
    })
  },

  //电话start
  bindtitleChange: function (e) {
    this.setData({
      invoiceTitle: e.detail.value
    })
  },
  //电话start
  bindtaxNumberChange: function (e) {
    this.setData({
      taxNumber: e.detail.value
    })
  },
  //电话start
  bindcompanyAddressChange: function (e) {
    this.setData({
      companyAddress: e.detail.value
    })
  },
  //电话start
  bindtelephoneChange: function (e) {
    this.setData({
      telephone: e.detail.value
    })
  },
  //电话start
  bindbankNameChange: function (e) {
    this.setData({
      bankName: e.detail.value
    })
  },
  bindbankAccountChange: function (e) {
    this.setData({
      bankAccount: e.detail.value
    })
  },
  //电话start
  bindtelphoneChange: function(e) {
    this.setData({
      u_telphone: e.detail.value
    })
  },
  //邮箱start
  bindmailboxChange: function(e) {
    this.setData({
      u_mailbox: e.detail.value
    })
  },
  //获取用户名
  u_names:function(e){
    var that = this;
    var reg = /^[a-zA-Z\u4e00-\u9fa5\.]+$/;
    if (!reg.test(e.detail.value)) {//姓名校验
      wx.showToast({//提示框
        title: '请输入正确的联系人姓名',
        icon: 'none',
        duration: 2000
      })
      this.setData({
    	isName0: true,
      })
      return;
    }
    var names = e.detail.value;
    this.setData({
    	m_username: names,
    })
  },
  u_phones:function(e){
    var obj = e.detail.value
    if(typeof obj == "undefined" || obj == null || obj == ""){
      var tipsshow11 = this.data.isName? "none": "block"
      this.setData({
        isPhone: false,
        config:{
          tipsshow1: tipsshow11,
          tipsshow2: "block"
          //tipsshow2: "none"
        },
      })
      return;
    }
  }
})