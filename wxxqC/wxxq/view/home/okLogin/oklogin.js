// wxxq/view/home/okLogin/oklogin.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url:'',
    flag:"",
    headIMG:"",
    mobile:'',
    mobile2: '',
    toUrl: '/wxxq/view/home/index/index',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'HEADIMG',
      success: function(res) {
        that.setData({
          flag:1,
          headIMG:res.data
        })
      },
      fail:function(){
        that.setData({
          flag: 0,
          
        })
      }
    })
    wx.getStorage({
      key: 'MOBILE',
      success: function (res) {
        that.setData({
          flag: 1,
          mobile:res.data,
        })
      },
      fail:function(){
        that.setData({
          flag: 0,

        })
      }
    })
    wx.getStorage({
      key: 'Mobile2',
      success: function (res) {
        that.setData({
          mobile2: res.data,
        })
      }
    })
    var nav = options;
    if(nav.nav != 0){
      this.setData({
        url: "/wxxq/view/home/login/index?nav=" + nav.nav,
      })
    }else{
      this.setData({
        url: "/wxxq/view/home/login/index?nav=0&hotelId=" + nav.hotelId + " &roomId=" + nav.roomId + "&vip=" + nav.vip + "&price=" + nav.price + "& beginTimeStr=" + nav.beginTimeStr + "&endTimeStr=" + nav.endTimeStr + "&book=" + nav.book
      })
    }
    //下单
    if (nav.nav == 0) {
      // that.setData({
      //   // toUrl: '/wxxq/view/home/reserve/reserve?nav=0&hotelId=' + nav.hotelId + " &roomId=" + nav.roomId + "&vip=" + nav.vip + "&price=" + nav.price + "&beginTimeStr=" + nav.beginTimeStr + "&endTimeStr=" + nav.endTimeStr + "&book=" + nav.book
      // })
    }
    //订单
    if (nav.nav == 2) {
      that.setData({
        toUrl: '/wxxq/view/mine/order/allOrder'
      })
    }
    //早餐
    if (nav.nav == 1) {
      that.setData({
        toUrl: '/wxxq/view/break/breakfast/breakfast'
      })
    }
    //我的
    if (nav.nav == 3) {
      that.setData({
        toUrl: '/wxxq/view/mine/mine/mine'
      })
    }
  },
  showL:function(){
    wx.showLoading({
      title: '登录中',
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url2 = web_url + "/members/bindmobile";
    console.log(web_url2);
    var sessionId = wx.getStorageSync('sessionId');
    var type = wx.getStorageSync('type')

    c._get({
      url: web_url2,
      data: {
        mobile: that.data.mobile2,
        vercode: "wxxq",
      },
      success: function (res) {
        console.log(res)
        if ((parseInt(res.statusCode) == 200)) {
          if (res.data.success) {

            wx.setStorage({
              key: "sessionId",
              data: res.data.data.sessionId,

            })
            wx.setStorage({
              key: "type",
              data: res.data.data.type,
            })
            wx.showToast({
              title: '登录成功',
              icon: 'success'
            })
            //跳转至首页
            wx.reLaunch({
              url: that.data.toUrl,
            })
          } else {
            wx.showToast({
              icon: 'none',
              title: "验证码错误",
              duration: 1000

            })
          }

        } else {
          wx.showToast({
            icon: 'none',
            title: "登录失败",
            duration: 1000

          })

        }
      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  getPhoneNumber:function(e){
    var e = e;
    var that = this;
    wx.showLoading({
      title: '',
      mask:true,
    })
    wx.login({
      success:function(a){
        console.log(a)
        console.log(e)
        //授权手机号失败
        if (e.detail.errMsg == "getPhoneNumber:ok") {
          var web_url = app.globalData.web_url;
          var web_url2 = web_url + "/members/bindmobile"; //登录
          var web_url3 = web_url + "/members/loginagain"//获取手机号
          var hotelId = app.globalData.hotel;
          var code = a.code;
          var encryptionCode = e.detail.encryptedData;
          var iv = e.detail.iv;
          var sessionId = wx.getStorageSync('sessionId');
          var type = wx.getStorageSync('type');
          if(encryptionCode != undefined && iv != undefined){
            c._get({
              url: web_url3,
              data: {
                hotelId: hotelId,
                encryptionCode: encryptionCode,
                iv: iv,
                code: code,
              },
              success: function (res) {
                if ((parseInt(res.statusCode) == 200)) {
                  if (res.data.success) {
                    wx.setStorage({
                      key: "sessionId",
                      data: res.data.data.sessionId,

                    })
                    wx.setStorage({
                      key: "type",
                      data: res.data.data.type,
                    })
                    wx.showToast({
                      title: '登录成功',
                      icon: 'success'
                    })
                    //跳转至首页
                    wx.reLaunch({
                      url: that.data.toUrl,
                    })
                  }
                }

              },
              fail: function () {

              }
            })
          }
        } else {
          wx.hideLoading();
        }

      }
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})