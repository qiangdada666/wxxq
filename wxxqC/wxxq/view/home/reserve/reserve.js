// wxxq/view/home/reserve/reserve.js
var c = require("../../common/common.js");
var time = require("../../../../utils/util.js");
var app = getApp()
Page({
  /**
   * 页面的初始数据
   */
 
  data: {

    //l轮播图start  
    roomPics: [{
        picUrl: "../../../images/home/introduce@2x.jpg"
      },
      {
        picUrl: "../../../images/home/introduce@2x.jpg"
      }
    ],
    p_t:'入住人姓名，每房一人',
    p_p: "用于接收预定通知短信",
    hotelName: "",
    active: true,
    config: {
      tipsshow1: "block",
      tipsshow2: "block"
    },
    isName: {
      isName1: "block",
      isName2: "block",
      isName3: "block",
    },
    "isName0": true,
    "isName1": true,
    "isName2": true,
    isPhone: false,
    //是否采用衔接滑动  
    circular: true,
    //是否显示画板指示点  
    indicatorDots: true,

    indicatorColor: "rgba(0, 0, 0, .3)", //指示点颜色
    indicatorActiveColor: "#a4783b", //当前选中的指示点颜色
    //是否竖直  
    vertical: false,
    //是否自动切换  
    autoplay: true,
    //滑动动画时长毫秒  
    duration: 1000,
    //所有图片的高度  
    imgheights: [],
    //图片宽度  
    imgwidth: 750,
    //默认  
    current: 0,


    // 轮播图 结束

    // 下拉列表选择房间数目
    array: ['1', '2', '3'],
    u_num: 1,
    u_num1: 0,

    // 下拉列表选择支付类型
    paytype: ['', '到店支付', '担保支付', '全额支付'],

    pay_type: 2,

    status: "",
    time: "",
    vipPrice: "",
    originalPrice: "",
    discountPrice: "",
    refund: "../../../images/mine/refund@2x.png",
    cancle: "../../../images/mine/cancle@2x.png",
    roomType: "",
    num: "",
    limit: "",

    day: "",
    roomNum: "",
    stipulate: "入住时间14:00/退房时间12:00",
    faclityList: [],
    // 入住日期start
    checkInDate: "",
    checkOutDate: "",

    // 用户信息start
    jiantou: "../../../images/home/jt@2x.png",
    telphone: "../../../images/mine/telphone@2x.png",
    top: "../../../images/common/top@2x.png",
    down: "../../../images/common/bottom@2x.png",
    showModalStatus: false,
    isFold: false,
    discount: 0,
    vipDiscount: "",
    cardDiscount: 0,

    isShow: 1,
    isUsable: 1,
    noCard: "../../../images/mine/discount@2x.png",
    cardList: [],
    hotelId: "",
    roomId: "",
    sum: "",
    u_num: 1,
    coupon: "",
    //房间信息
    allmsg: '',
    //应付价格
    pay_price: 0,
    firstValue: 0,
    useBalance: 0,
    vipMoney: 0,
    //
    couponType: "小程序专用",
    endTime: '',
    //优惠券id
    coupon_id: '',
    // 轮播图默认地址
    picUrl: "",
    radio: {
      selected: '../../../images/mine/circle1.png',
      unselected: '../../../images/mine/circle2.png'
    },
    // 按钮切换
    isFold3: false,
    //预览原价
    prepare_price: 0,
    //是否满房
    book: 0,
    //未使用优惠券的原价
    price_daynum: 0,
    //可用优惠券数量
    coupon_num_use: 0,
    //姓名
    names:[],
    //预订 开关
    f : true,
  },
  // 选择优惠券，使用不使用按钮操作
  changeCard: function() {
    var that = this;
    var sum = that.data.sum;
    var discount = that.data.discount;
    that.setData({
      discount: 0,
      coupon_id: "",
      isShow: 1,
      sum: Number(sum) + Number(discount),
    })


  },
  /**
   * 生命周期函数--监听页面加载
   * 
   */
  // 切换余额是否选择
  changeAgree: function() {
    var that = this;
    var day = wx.getStorageSync("num");
    if (that.data.price_old * that.data.u_num * day > that.data.discount) {
      var flag = this.data.isFold3;
      var agree = wx.getStorageSync("sumMoney");
      var useBalance = 0;
      var totalprice = Number(that.data.sum);
      if (flag !== true) {
        if (totalprice <= Number(agree)) {
          useBalance = totalprice;
          totalprice = 0;
        } else {
          useBalance = Number(agree);
          totalprice = Number(totalprice) - useBalance;
        }
        this.setData({
          useBalance: useBalance,
          sum: totalprice,
        })
      } else {
        var num = wx.getStorageSync("num");
        var price = Number(that.data.allmsg.discountPrice) * num * that.data.u_num - Number(that.data.discount);
        this.setData({
          useBalance: 0,
          sum: price,
        })
      }
      this.setData({
        isFold3: !this.data.isFold3,
      })
    }
  },
  // 轮播图图片自适应
  imageLoad: function(e) {
    //获取图片真实宽度  
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比  
      ratio = imgwidth / imgheight;
    //计算的高度值  
    var viewHeight = 750 / ratio;
    var imgheight = viewHeight
    var imgheights = this.data.imgheights
    //把每一张图片的高度记录到数组里  
    imgheights.push(imgheight)
    this.setData({
      imgheights: imgheights,
    })
  },
  // 拨打酒店电话
  calling: function() {
    var telphone1 = wx.getStorageSync('telphone1');
    var telphones = telphone1;
    wx.makePhoneCall({
      phoneNumber: telphones //仅为示例，并非真实的电话号码
    })
  },
  onLoad: function(options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    //详情页显示控制
    var roomdetail_auth = app.globalData.roomdetail;
    that.setData({
      roomdetail_auth: roomdetail_auth,
    })
    //柱几晚
    var night = 1;
    var web_url = app.globalData.web_url;
    var g_url = app.globalData.web_url1;
    var web_url1 = web_url + "/rooms/getRoomInfo";
    var sessionId = wx.getStorageSync('sessionId');
    var web_url2 = web_url + "/membercoupons";
    // 获取传递过来的参数房间Id,酒店id
    var hotelId = options.hotelId;
    var roomId = options.roomId;
    var book = options.book;
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkInDate = getDate.checkInDate;
    var checkOutDate = getDate.checkOutDate;
    var day = that.getDays(checkInDate, checkOutDate);
    that.setData({
      day: day,
      book: book,
    })
    var u_num = that.data.u_num;
    var roomNum = that.data.array[u_num];
    var allmsg = '';
    var price_daynum = 0;
    //页面房间数据请求
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        sessionId: sessionId,
        hotelId: hotelId,
        roomId: roomId,
        beginTimeStr: checkInDate,
        endTimeStr: checkOutDate
      },

      success: function(res) {
        allmsg = res.data.data;
        var a = 1;
        var arr = [0];
        for (var i in res.data.data.facilities) {
          arr.push(res.data.data.facilities[i]); //属性
        }
        var day_num = wx.getStorageSync("num");
        var sum = (Number(res.data.data.discountPrice) * day_num * night - Number(that.data.discount)).toFixed(2);
        that.setData({
          picUrl: g_url,
          hotelId: res.data.data.hotelId,
          name: res.data.data.name,
          residentLimit: res.data.data.residentLimit,
          facility: arr,
          roomId: res.data.data.roomId,
          policies: res.data.data.policies,
          payType: res.data.data.payType,
          sum: sum,
          sum_old: sum,
          roomPics: res.data.data.roomPics,
          allmsg: res.data.data,
          price_old: Number(res.data.data.discountPrice) * day_num * night,
          prepare_price: Number(res.data.data.originalPrice) * day_num * night,
          price_daynum: Number(res.data.data.discountPrice) * day_num * night,
          //price_daynum:0,
        })
        price_daynum = Number(res.data.data.discountPrice) * day_num * night;
        //转换房间会员价浮点数 
        var originalPrice = allmsg.originalPrice;
        var discountPrice = allmsg.discountPrice;
        var memberDiscount = Number((res.data.data.originalPrice - res.data.data.discountPrice) * day_num * night).toFixed(2);
        that.setData({
          originalPrice: originalPrice,
          discountPrice: discountPrice,
          memberDiscount: memberDiscount
        })
        // 获取优惠券列表
        wx.request({
          url: web_url2,
          data: {

          },
          header: {
            'content-type': 'application/x-www-form-urlencoded',
            "sessionId": sessionId,
            // 默认值
          },
          success: function(res) {
            var num = 0;
            if (res.data !== "") {
              //可用优惠券判断
              for (var i = 0; i < res.data.data.length; i++) {
                if (res.data.data[i].status == "0") {
                  if (res.data.data[i].couponType == 1) {
                    //直减优惠券
                    if (res.data.data[i].mode == 1) {
                      if (that.data.price_daynum >0){
                        ++num;
                      }
                      res.data.data[i].type = 1;
                    } else {
                      //满减优惠券
                      res.data.data[i].type = 2;
                      if (that.data.price_daynum > res.data.data[i].firstValue) {
                        ++num;
                      }
                    }
                  } else {
                    //充值券
                    res.data.data[i].type = 3;
                  }
                }
              }
              that.setData({
                cardList: res.data.data,
                coupon_num_use: num,
              })
            } else {
              that.setData({
                cardList: res.data.data,
              })
            }
          }
        })
        wx.hideLoading();
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  // 查看明细由下向上划出
  powerDrawer: function(e) {
    var currentStatu = '';
    if (this.data.showModalStatus) {
      currentStatu = 'close';
    } else {
      currentStatu = 'open';
    }
    this.util(currentStatu)
  },
  util: function(currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例 
    var animation = wx.createAnimation({
      duration: 200, //动画时长
      timingFunction: "linear", //线性
      delay: 0 //0则不延迟
    });

    // 第2步：这个动画实例赋给当前的动画实例
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画
    setTimeout(function() {
      // 执行第二组动画：Y轴不偏移，停
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象
      this.setData({
        animationData: animation
      })

      //关闭抽屉
      if (currentStatu == "close") {
        this.setData({
          showModalStatus: false,
          isFold: false,
        });
      }

      // 显示抽屉
      if (currentStatu == "open") {
        this.setData({
          showModalStatus: true,
          isFold: true,
        });
      }
    }.bind(this), 200)


  },
  /**
   * 生命周期函数--监听页面显示
   */
  // 预定日期
  onShow: function(e) {
    var that = this
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkInDates = getDate.checkInDate.substring(5, 7) + "月" + getDate.checkInDate.substring(8, 11) + "日";
    var checkOutDates = getDate.checkOutDate.substring(5, 7) + "月" + getDate.checkOutDate.substring(8, 11) + "日";

    var inDate = getDate.checkInDate.substring(getDate.checkInDate.length - 2);
    var outDate = getDate.checkOutDate.substring(getDate.checkOutDate.length - 2);
    var day = that.getDays(inDate, outDate);
    //入住与离店日期计算截取
    that.setData({
      checkInDate: checkInDates,
      checkOutDate: checkOutDates,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
  // 优惠券获取值
  cardChange: function(e) {
    var that = this;
    that.setData({
      isShow: 2,

    })
    // 优惠券截止日期计算
    var coupon_list = that.data.cardList;
    for (var i = 0; i < coupon_list.length; i++) {
      var nowtime = (new Date().getTime()) / 1000; //获取当前时间戳
      var nowtime = nowtime.toFixed(0);
      var endTime = coupon_list[i].validEnd;
      var oldTime = (new Date(endTime)).getTime() / 1000;
      var nTime = (oldTime - nowtime) / 24 / 60 / 60;
      var nTime = nTime.toFixed(0);
      coupon_list[i].ntime = nTime;
      that.setData({
        cardList: coupon_list
      })
    }
  },
  // 获取当前选择的优惠券值
  userCoupon: function(e) {
    if (this.data.price_daynum <= 0){
      return;
    }
    var day = wx.getStorageSync("num");
    var ntime = e.currentTarget.dataset.ntime;
    if (Number(ntime) <= -1) return;
    var that = this;
    var sum = that.data.sum;

    var index = parseInt(e.currentTarget.dataset.index);
    var u_num = that.data.u_num;
    //值
    var firstValue = e.currentTarget.dataset.src;
    //门槛
    var first = e.currentTarget.dataset.fir;
    var totalPrice = sum;
    if (that.data.price_old * u_num * day < first) {
      return;
    }

    //直减券使用限制
    var mode = e.currentTarget.dataset.mode;
    if (mode == '1') {
      if (firstValue >= totalPrice) {
        firstValue = sum;
      }
    }
    //优惠券id 
    var id = e.currentTarget.dataset.id;
    that.setData({
      coupon_id: id,
    })
    var vipPrice = (that.data.price_old * u_num * day - firstValue).toFixed(2);
    that.setData({
      isShow: 1,
      discount: firstValue,
      sum: vipPrice,
      cardDiscount: firstValue,
    })
    var useBalance = 0;
    var totalprice = 0;
    var agree = wx.getStorageSync("sumMoney");
    //余额计算
    //优惠券满足使用的话使用余额
    if (that.data.price_old * u_num > e.currentTarget.dataset.src) {
      vipPrice = that.data.price_old * u_num - Number(e.currentTarget.dataset.src);
      if (that.data.isFold3 == true) {
        if (vipPrice <= Number(agree)) {
          useBalance = vipPrice;
          totalprice = 0;
        } else {
          useBalance = Number(agree);
          totalprice = Number(vipPrice) - useBalance;
        }
        this.setData({
          useBalance: useBalance,
          sum: totalprice,
          discount: e.currentTarget.dataset.src,
        })
      } else {
        this.setData({
          useBalance: useBalance,
          sum: vipPrice,
          discount: e.currentTarget.dataset.src,
        })
      }
    } else {
      that.setData({
        sum: 0,
        isFold3: false,
        useBalance: 0,
        //coupon_id:'',
      })
    }
  },
  //选择房间数start
  bindNumChange: function(e) {
    var that = this;
    var night = wx.getStorageSync("num");
    var sum = Number(that.data.price_old);
    var roomNum = Number(e.detail.value) + 1;
    var sumPrice = Number((that.data.allmsg.discountPrice * roomNum * night - that.data.discount)).toFixed(2);
    var memberDiscount = Number(that.data.memberDiscount);
    var sumMember = ((roomNum * memberDiscount)).toFixed(2);
    var originalPrice = Number(that.data.originalPrice);
    var sumoriginal = ((roomNum * originalPrice)).toFixed(2);
    var useBalance = 0;
    var agree = wx.getStorageSync("sumMoney");
    that.setData({
      u_num: roomNum,
      u_num1: e.detail.value,
      sum: sumPrice,
      memberDiscount: (that.data.allmsg.originalPrice - that.data.allmsg.discountPrice) * roomNum * night,
      originalPrice: sumoriginal,
      useBalance: useBalance,
      prepare_price: that.data.allmsg.originalPrice * roomNum * night,
      price_daynum: that.data.allmsg.discountPrice * roomNum * night,
    })
    var totalprice = 0;
    if (that.data.isFold3 == true) {
      if (sumPrice <= Number(agree)) {
        useBalance = sumPrice;
        totalprice = 0;
      } else {
        useBalance = Number(agree);
        totalprice = Number(sumPrice) - useBalance;
      }
      that.setData({
        useBalance: useBalance,
        sum: totalprice,
      })
    }
    var carlist = that.data.cardList;
    var num = 0;
    //房间数改变优惠券使用情况重新判断
    for (var i = 0; i < carlist.length; i++) {
      if (carlist[i].status == "0") {
        if (carlist[i].couponType == 1) {
          //直减优惠券
          if (carlist[i].mode == 1) {
            if (that.data.price_daynum > 0){
              console.log(that.data.price_daynum)
              ++num;
            }
            carlist[i].type = 1;
          } else {
            //满减优惠券
            carlist[i].type = 2;
            if (that.data.price_daynum > carlist[i].firstValue) {
              ++num;
            }
          }
        } else {
          //充值券
          carlist[i].type = 3;
        }
      }
    }
    var room_num = roomNum;
    for(var i= 0;i<room_num;i++){
      console.log("房间数量");
    }
    that.setData({
      coupon_num_use: num,
    })
  },
  bindPayChange: function(e) {
    var that = this;
    that.setData({
      pay_type: e.detail.value,
    })
  },
  bindNameFocus: function(e) {
    var index = e.currentTarget.dataset.index;
    var tipsshow22 = this.data.isPhone? "none": "block"
    var isName11 = this.data.isName1? "none": "block"
    var isName22 = this.data.isName2? "none": "block"
    var isName33 = this.data.isName3? "none": "block"
    var isName = "isName" + index
    console.log(isName)
    this.setData({
      [isName]: false,
      config:{
        tipsshow1: "none",
        tipsshow2: tipsshow22
      },
      isName:{
        isName1: isName11,
        isName2: isName22,
        isName3: isName33,
      }
    })
  },
  //姓名start
  bindUsernameChange: function(e) {
    console.log(e)
    this.setData({
      m_username: e.detail.value
    })
  },
  bindPhoneFocus: function(e) {
    var tipsshow11 = this.data.isName? "none": "block"
    this.setData({
      isPhone: true,
      config:{
        tipsshow1: tipsshow11,
        tipsshow2: "none"
        //tipsshow2: "none"
      },
    })
  },

  //电话start
  bindtelphoneChange: function(e) {
    this.setData({
      u_telphone: e.detail.value
    })
  },
  //邮箱start
  bindmailboxChange: function(e) {
    this.setData({
      u_mailbox: e.detail.value
    })
  },
  //获取用户名
  u_names:function(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    var obj = e.detail.value
      if(typeof obj == "undefined" || obj == null || obj == ""){
      var tipsshow22 = this.data.isPhone? "none": "block"
      var isName11 = this.data.isName1? "none": "block"
      var isName22 = this.data.isName2? "none": "block"
      var isName33 = this.data.isName3? "none": "block"
      var isName = "isName" + index
      console.log(isName)
      this.setData({
        [isName]: true,
        config:{
          tipsshow1: "block",
          tipsshow2: tipsshow22
        },
        isName:{
        isName1: isName11,
        isName2: isName22,
        isName3: isName33,
      }
      })
      return;
    }
    var names = that.data.names;
    var reg = /^[a-zA-Z\u4e00-\u9fa5\.]+$/;
    if (!reg.test(e.detail.value)) {//姓名校验
      wx.showToast({//提示框
        title: '请输入正确的联系人姓名',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    names[index] = e.detail.value;
  },
  u_phones:function(e){
    var obj = e.detail.value
    if(typeof obj == "undefined" || obj == null || obj == ""){
      var tipsshow11 = this.data.isName? "none": "block"
      this.setData({
        isPhone: false,
        config:{
          tipsshow1: tipsshow11,
          tipsshow2: "block"
          //tipsshow2: "none"
        },
      })
      return;
    }
  },
  // 用户预定信息提交
  //传入参数获取表单数据；
  bindChaneInfo: function(e) {
    var that = this;
    console.log(e)
    console.log(that.data.names)
    var u_num = that.data.u_num;
    var names = that.data.names
    if(names.length !== u_num){
      wx.showToast({
        title: '请输入合法姓名',
        icon:'none'
      })
      return;
    }
    for(var i = 0; i <names.length; i++){
      if (names[i] == "" || names[i] == null){
        wx.showToast({
          title: '请输入合法姓名',
          icon:'none'
        })
        return;
      }
    }
    var m_username = names.join(",");//订单人姓名
    var check_tel = /^[1][3,4,5,6,7,8,9][0-9]{9}$/;//手机号验证正则；
    var sessionId = wx.getStorageSync('sessionId');//获取sessionID
    var roomCount = that.data.u_num;
    var u_telphone = e.detail.value.mobile;//订单人手机
//    var u_telphone = 15011433925//订单人手机
    var o_mailbox = e.detail.value.mailebox;//邮箱
    var notCancle = e.detail.value.notCancle;//退款
    var name = e.detail.value.roomname;//房间名字
    var orderAmount = that.data.sum;//总计
    var paytype = Number(this.data.pay_type);//支付类型
    var formId = e.detail.formId;//formID
    // var coupon = that.data.coupon;
    var time = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkinTime = time.checkInDate;
    var checkoutTime = time.checkOutDate;
    var discountPrice = that.data.discountPrice;
    var totalprice = that.data.sum;
    var useBalance = that.data.useBalance;
    var roomId = this.data.roomId;
    if (!check_tel.test(u_telphone)) {
      wx.showToast({
        title: '请填写正确手机号',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var allmsg = that.data.allmsg;
    if (that.data.allmsg.payType == 2) {//逻辑计算
      totalprice = allmsg.discountPrice * that.data.u_num - that.data.discount - useBalance;
    }
    if(that.data.f){
      that.setData({
        f : false,
      })
      c._post({
        url: app.globalData.web_url + "/orders",
        data: {
          //总价格
          totalprice: totalprice,
          //折扣后价格
          discountPrice: discountPrice,
          //使用余额的额度
          useBalance: useBalance,
          //入住人
          resident: m_username,
          //电话
          mobile: u_telphone,
          //邮箱
          email: o_mailbox,
          //formid
          formId: formId,
          //房间数
          roomCount: roomCount,
          //入住时间
          checkinTime: checkinTime,
          //结束时间
          checkoutTime: checkoutTime,
          //折扣价格
          orderAmount: discountPrice,
          //退款
          notCancle: notCancle,
          //房间名字
          name: name,

          // 优惠券值
          discount: that.data.discount,
          payType: that.data.allmsg.payType,
          payWay: 1,
          roomId: roomId,
          coupon_id: that.data.coupon_id,
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          'sessionId': sessionId,
          // 默认值
        },
        method: "POST",
        success: function (res) {
          if (res.data.success == true) {
            wx.redirectTo({
              url: '/wxxq/view/mine/pay/pay?orderId=' + res.data.data + "&balance=" + useBalance + "&coupon=" + that.data.discount + "&paytype=" + paytype,
            })
          } else {
            wx.showToast({
              title: "创建订单失败",
              icon: 'none',
              duration: 2000,
            })
            return false;
          }

        }
      })
    }
    
  },
  //日期相减得到天数
  getDays: function(stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },

})