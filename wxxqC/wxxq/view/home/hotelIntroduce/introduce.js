// wxxq/view/home/hotelIntroduce/introduce.js
var app = getApp();
var hotelId = app.globalData.hotel;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    bannerPics: "../../../images/home/bg@2x.jpg",
    hotelName: "",
    advList: [],
    introduce: "",
    telphone: "",
    address: "",
    allmsg: '',
    web_url: '',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showLoading({
      title: '加载中',
      mask:true,
    })
    // 调取酒店信息数据接口
    var that = this;
    var web_url = app.globalData.web_url;
    var g_url = app.globalData.web_url1;
    var sessionId = wx.getStorageSync('sessionId');
    // 获取酒店id接口
    var web_url1 = web_url + "/hotels/"+hotelId+"?type=2";
    wx.request({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        hotelId: app.globalData.hotel,
        sessionId: "sessionId"


      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res);
        console.log(res.data.data.bannerPics[0].picUrl)
        that.setData({
          bannerPics: res.data.data.bannerPics[0].picUrl,
          telphone: res.data.data.contact,
          district: res.data.data.district,
          hotelName: res.data.data.hotelName,
          advList: res.data.data.tag,
          web_url: g_url,
          province: res.data.data.province,
          city: res.data.data.city,
          district: res.data.data.district,
          street: res.data.data.street,
          introduce: res.data.data.summary,
          allmsg: res.data.data,
          address: res.data.data.adress,
        })
        wx.hideLoading();
        console.log(that.data.bannerPics)
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})