// wxxq/view/home/reserve/reserve.js
var c = require("../../common/common.js");

var time = require("../../../../utils/util.js");

var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

    //l轮播图start  
    roomPics: [
      {
        // picUrl: "../../../images/home/introduce@2x.jpg"
        picUrl: "../../../images/home/roomdetialyin.jpg"
      }
    ],
    hotelName: "",
    active: true,

    //是否采用衔接滑动  
    circular: true,
    //是否显示画板指示点  
    indicatorDots: true,

    indicatorColor: "rgba(0, 0, 0, .3)", //指示点颜色
    indicatorActiveColor: "#a4783b", //当前选中的指示点颜色
    //是否竖直  
    vertical: false,
    //是否自动切换  
    autoplay: true,
    //滑动动画时长毫秒  
    duration: 1000,
    //所有图片的高度  
    imgheights: [],
    //图片宽度  
    imgwidth: 750,
    //默认  
    current: 0,


    // 轮播图 结束

    // 下拉列表选择房间数目
    array: ['1', '2', '3'],
    u_num: 1,
    u_num1: 0,

    // 下拉列表选择支付类型
    paytype: ['', '到店支付', '担保支付', '全额支付'],

    pay_type: 2,

    status: "",
    time: "",
    vipPrice: "",
    originalPrice: "",
    discountPrice: "",
    refund: "../../../images/mine/refund@2x.png",
    cancle: "../../../images/mine/cancle@2x.png",
    roomType: "",
    num: "",
    limit: "",

    day: "",
    roomNum: "",
    stipulate: "入住时间14:00/退房时间12:00",
    faclityList: [],
    // 入住日期start
    checkInDate: "",
    checkOutDate: "",
    // rili: "../../../images/home/sy_rl@2x.png",
    // 用户信息start
    jiantou: "../../../images/home/jt@2x.png",
    telphone: "../../../images/mine/telphone@2x.png",
    top: "../../../images/common/top@2x.png",
    down: "../../../images/common/bottom@2x.png",
    showModalStatus: false,
    isFold: false,
    discount: 0,
    vipDiscount: "",
    cardDiscount: 0,

    isShow: 1,
    isUsable: 1,
    noCard: "../../../images/mine/discount@2x.png",
    cardList: [],
    hotelId: "",
    roomId: "",
    sum: "",
    u_num: 1,
    coupon: "",
    //房间信息
    allmsg: '',
    //应付价格
    pay_price: 0,
    firstValue: 0,
    useBalance: 0,
    vipMoney: 0,
    //
    couponType: "小程序专用",
    endTime: '',
    //优惠券id
    coupon_id: '',
    // 轮播图默认地址
    picUrl: "",
    radio: {
      selected: '../../../images/mine/circle1.png',
      unselected: '../../../images/mine/circle2.png'
    },
    // 按钮切换
    isFold3: false,
    //预览原价
    prepare_price: 0,
    //是否满房
    book: 0,
    //未使用优惠券的原价
    price_daynum: 0,
    //可用优惠券数量
    coupon_num_use: 0,
  },
  /**
   * 生命周期函数--监听页面加载
   * 
   */
  // 轮播图图片自适应
  imageLoad: function (e) {
    //获取图片真实宽度  
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比  
      ratio = imgwidth / imgheight;
    //计算的高度值  
    var viewHeight = 750 / ratio;
    var imgheight = viewHeight
    var imgheights = this.data.imgheights
    //把每一张图片的高度记录到数组里  
    imgheights.push(imgheight)
    this.setData({
      imgheights: imgheights,
    })
  },
  // 拨打酒店电话
  calling: function () {
    var that = this;
    c._is_calling(that);
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    //柱几晚
    var night = 1;
    var web_url = app.globalData.web_url;
    var g_url = app.globalData.web_url1;
    var web_url1 = web_url + "/rooms/getRoomInfo";
    var sessionId = wx.getStorageSync('sessionId');
    var web_url2 = web_url + "/membercoupons";
    // 获取传递过来的参数房间Id,酒店id
    var hotelId = options.hotelId;
    var roomId = options.roomId;
    var book = options.book;
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkInDate = getDate.checkInDate;
    var checkOutDate = getDate.checkOutDate;
    var day = that.getDays(checkInDate, checkOutDate);
    that.setData({
      day: day,
      book: book,
    })
    var u_num = that.data.u_num;
    var roomNum = that.data.array[u_num];
    var allmsg = '';
    console.log(options)
    var price_daynum = 0;
    //预览房间初始化请求
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        sessionId: sessionId,
        hotelId: hotelId,
        roomId: roomId,
        beginTimeStr: checkInDate,
        endTimeStr: checkOutDate
      },

      success: function (res) {
        console.log(res)
        allmsg = res.data.data;
        var a = 1;
        var day_num = wx.getStorageSync("num");
        var sum = (Number(res.data.data.discountPrice) * day_num * night - Number(that.data.discount)).toFixed(2);
        that.setData({
          picUrl: g_url,
          hotelId: res.data.data.hotelId,
          name: res.data.data.name,
          residentLimit: res.data.data.residentLimit,
          facility: res.data.data.facilities,
          roomId: res.data.data.roomId,
          policies: res.data.data.policies,
          payType: res.data.data.payType,
          sum: sum,
          sum_old: sum,
          roomPics: res.data.data.roomPics,
          allmsg: res.data.data,
          price_old: Number(res.data.data.discountPrice) * day_num * night,
          prepare_price: Number(res.data.data.originalPrice) * day_num * night,
          price_daynum: Number(res.data.data.discountPrice) * day_num * night,
        })
        price_daynum = Number(res.data.data.discountPrice) * day_num * night;
        //转换房间会员价浮点数 
        var originalPrice = allmsg.originalPrice;
        var discountPrice = allmsg.discountPrice;
        var memberDiscount = Number((res.data.data.originalPrice - res.data.data.discountPrice) * day_num * night).toFixed(2);
        that.setData({
          originalPrice: originalPrice,
          discountPrice: discountPrice,
          memberDiscount: memberDiscount
        })
        wx.hideLoading();
      }
    })

  },

  /**
    * 生命周期函数--监听页面初次渲染完成
    */
  onReady: function () {

  },

  // 查看明细由下向上划出
  powerDrawer: function (e) {
    var currentStatu = '';

    if (this.data.showModalStatus) {
      currentStatu = 'close';
    } else {
      currentStatu = 'open';
    }
    this.util(currentStatu)
  },
  util: function (currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例 
    var animation = wx.createAnimation({
      duration: 200, //动画时长
      timingFunction: "linear", //线性
      delay: 0 //0则不延迟
    });

    // 第2步：这个动画实例赋给当前的动画实例
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画
    setTimeout(function () {
      // 执行第二组动画：Y轴不偏移，停
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象
      this.setData({
        animationData: animation
      })

      //关闭抽屉
      if (currentStatu == "close") {
        this.setData({
          showModalStatus: false,
          isFold: false,
        });
      }

      // 显示抽屉
      if (currentStatu == "open") {
        this.setData({
          showModalStatus: true,
          isFold: true,
        });
      }

    }.bind(this), 200)


  },
  /**
   * 生命周期函数--监听页面显示
   */
  // 预定日期
  onShow: function (e) {
    var that = this
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkInDates = getDate.checkInDate.substring(5, 7) + "月" + getDate.checkInDate.substring(8, 11) + "日";
    var checkOutDates = getDate.checkOutDate.substring(5, 7) + "月" + getDate.checkOutDate.substring(8, 11) + "日";

    var inDate = getDate.checkInDate.substring(getDate.checkInDate.length - 2);
    var outDate = getDate.checkOutDate.substring(getDate.checkOutDate.length - 2);
    var day = that.getDays(inDate, outDate);


    that.setData({
      checkInDate: checkInDates,
      checkOutDate: checkOutDates,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },

  //日期相减得到天数
  getDays: function (stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },

})