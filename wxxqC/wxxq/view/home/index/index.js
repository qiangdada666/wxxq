// wxxq/view/home/index/index.js
var Moment = require("../../../../utils/moment.js");
var c = require("../../common/common.js");

var DATE_LIST = [];
var DATE_YEAR = new Date().getFullYear()
var DATE_MONTH = new Date().getMonth() + 1
var DATE_DAY = new Date().getDate()
var app = getApp()
var h_Id = app.globalData.hotel;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    types: 1,
    curIdx: 0,
    // 判断是否满房
    isbookable: 1,
    // 判断拨打电话是否弹窗 
    showModalStatus: false,
    //判断是否为iPhone X 默认为值false，iPhone X 值为true
    isIphoneX: app.globalData.isIphoneX,
    // 判断是否可预订房间按钮
    // isShowOrder: false,

    //l轮播图start  
    bannerPics: [{
        picUrl: "../../../images/home/banner1@2x.jpg"
      },
      {
        picUrl: "../../../images/home/banner@2x.jpg"
      }
    ],
    hotelName: "",
    active: true,

    //是否采用衔接滑动  
    circular: true,
    //是否显示画板指示点  
    indicatorDots: true, 
    indicatorColor: "rgba(0, 0, 0,)", //指示点颜色
    indicatorActiveColor: "#a4783b", //当前选中的指示点颜色
    //是否竖直  
    vertical: false,
    //是否自动切换  
    autoplay: true,
    //滑动动画时长毫秒  
    duration: 1000,
    //所有图片的高度  
    imgheights: [], 
    //图片宽度  
    imgwidth: 750,
    //默认  
    current: 0,

    // 轮播图 结束

    // advtange start 
    advList: [],
    // 酒店 介绍
    introduce: "../../../images/home/sy_jdxq@2x.png",
    telphone: "../../../images/home/sy_dh@2x.png",
    jiantou: "../../../image/local.png",
    jt: "../../../image/gt.png",
    boxshow: "../../../images/home/boxshow2x.png",
    // 入住日期start
    checkInDate: "",
    checkOutDate: "",
    rili: "../../../images/home/sy_rl@2x.png",
    // 联系电话
    telphone1: "",


    // 底部导航

    roomList: "",
    facility: "",
    policyCon: "",
    allmsg: '',
    //web_url
    web_url: '',
    g_url:""

  },
  //调取一键拨打电话功能
  callBack: function() {
    var telphone1 = wx.getStorageSync('telphone1');
    var telphones =  telphone1;
    wx.makePhoneCall({
      phoneNumber: telphones //仅为示例，并非真实的电话号码
    })
  },


  // 判断是否登录
  count: function() {


  },

  isLogin: function(e) {
    var that = this;
    var isDetail = that.data.showModalStatus;
    var book = isDetail? that.data.bookable :e.currentTarget.dataset.book;
    var type = wx.getStorageSync('type');
    var hotelId = !isDetail ? e.currentTarget.dataset.hotelid : that.data.hotelId;
    console.log(hotelId);
    var roomId = isDetail? that.data.roomId : e.currentTarget.dataset.roomid;
    that.setData({
      hotelId: hotelId,
      roomId: roomId,
      book: book,
    })
    if (type == 1) {
      var date = wx.getStorageSync("ROOM_SOURCE_DATE");
      wx.navigateTo({
        url: '/wxxq/view/home/okLogin/oklogin?nav=0&hotelId=' + hotelId + "&roomId=" + roomId + "&vip=" + e.currentTarget.dataset.vip + "&price=" + e.currentTarget.dataset.price + "&beginTimeStr=" + date.checkInDate + "&endTimeStr=" + date.checkOutDate + "&book=" + book,
      })
    }
    if (type == 2) {
      if (book == "0") {
        
      } else {
        var date = wx.getStorageSync("ROOM_SOURCE_DATE");
        wx.navigateTo({

          url: '/wxxq/view/home/reserve/reserve?hotelId=' + hotelId + "&roomId=" + roomId + "&vip=" + e.currentTarget.dataset.vip + "&price=" + e.currentTarget.dataset.price + "&beginTimeStr=" + date.checkInDate + "&endTimeStr=" + date.checkOutDate + "&book=" + book,
        })
      }
    }
  },
  toRooms:function(e){
    var hotelId = e.currentTarget.dataset.hotelid;
    var roomId = e.currentTarget.dataset.roomid;
    var book = e.currentTarget.dataset.book;
    var date = wx.getStorageSync("ROOM_SOURCE_DATE");
    wx.navigateTo({

      url: '/wxxq/view/home/roomDetail/roomDetail?hotelId=' + hotelId + "&roomId=" + roomId + "&vip=" + e.currentTarget.dataset.vip + "&price=" + e.currentTarget.dataset.price + "&beginTimeStr=" + date.checkInDate + "&endTimeStr=" + date.checkOutDate + "&book=" + book,
    })
  },
  onLoad: function(options) {
    var web_url = app.globalData.web_url;
    var g_url = app.globalData.web_url1;
    var that = this;
    var type = wx.getStorageSync('type');
    that.setData({
      types: type,
      web_url: web_url,
      g_url: g_url,
    })
    //设缓存缓存起来的日期
    //var that = this;
    wx.setStorage({
      key: 'ROOM_SOURCE_DATE',
      data: {
        checkInDate: Moment(new Date()).format('YYYY-MM-DD'),
        checkOutDate: Moment(new Date()).add(1, 'day').format('YYYY-MM-DD')
      }
    });
    // 计算入住时间共几晚
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var num = this.getDays(getDate.checkInDate, getDate.checkOutDate);
    wx.setStorageSync('num', num);
    this.setData({
      checkInDate: getDate.checkInDate,
      checkOutDate: getDate.checkOutDate,
      num: num
    })
    // 设置iphoneX
    c._set_iphoneX(that);
    // 设置底部
    c._set_foot(that, 0);
    // 判断是否授权 
    c._is_author({
      success: function() {
        c._is_login({
          web_url: web_url,
          success: function(e) {
            wx.showLoading({
              title: '加载中',
              mask:true,
            })
            var type = wx.getStorageSync('type');
            //1. 调取酒店信息数据接口
            var web_url = app.globalData.web_url;
            var web_url1 = web_url + "/hotels/" + h_Id+"?type=1";
            var sessionId = wx.getStorageSync('sessionId');
            c._get({
              url: web_url1, //仅为示例，并非真实的接口地址
              data: {

                hotelId: app.globalData.hotel,
              },
              success: function(res) {
                
                if (res.data.success == true) {
                  that.setData({
                    allmsg: res.data.data,
                    bannerPics: res.data.data.bannerPics,
                    telphone1: res.data.data.contact,
                    district: res.data.data.district,
                    hotelName: res.data.data.hotelName,
                    latitude: res.data.data.latitude,
                    longitude: res.data.data.longitude,
                    facilities: res.data.data.facilities,
                    advList: res.data.data.tag,
                    province: res.data.data.province,
                    city: res.data.data.city,
                    district: res.data.data.district,
                    street: res.data.data.street,
                    policies: res.data.data.policies,
                    hotelId: res.data.data.hotelId,
                    
                  })
                  wx.setStorage({
                    key: "telphone1",
                    data: res.data.data.contact,
                  })
                  wx.setStorage({
                    key: "hotelName",
                    data: res.data.data.hotelName,
                  })
                  wx.setStorage({
                    key: "latitude",
                    data: res.data.data.latitude,
                  })
                  wx.setStorage({
                    key: "longitude",
                    data: res.data.data.longitude,
                  })
                  wx.hideLoading()
                }
              }
            })
            //2. 调取酒店房间列表数据接口
            var web_url2 = web_url + "/rooms?type=1";
            var sessionId = wx.getStorageSync('sessionId');
            wx.getStorage({
              key: 'hotelId',
              success: function(res) {
                that.setData({
                  hotelId: res.data,
                })
              }
            })
            let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
            var checkInDate = getDate.checkInDate;
            var checkOutDate = getDate.checkOutDate;
            c._get({
              url: web_url2, //仅为示例，并非真实的接口地址
              data: {
                hotelId: app.globalData.hotel,
                // sessionId: sessionId,
                beginTimeStr: checkInDate,
                endTimeStr: checkOutDate,
              },
              success: function(res) {
                if (res.data.success == true) {
                  that.setData({
                    roomList: res.data.data,
                    roomId: res.data.data[0].roomId,
                    hotelId: res.data.data[0].hotelId,
                  })
                }
              }
            })
          }
        })
      }
    });
    //获取订单数
    c._getN(that)

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    //获取订单数
    c._getN(that)
    // 计算入住时间共几晚
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var num = this.getDays(getDate.checkInDate, getDate.checkOutDate);
    wx.setStorageSync('num', num);
    this.setData({
      checkInDate: getDate.checkInDate,
      checkOutDate: getDate.checkOutDate,
      num: num
    })
    //2. 调取酒店房间列表数据接口
    var web_url2 = that.data.web_url + "/rooms?type=1";
    var sessionId = wx.getStorageSync('sessionId');
    wx.getStorage({
      key: 'hotelId',
      success: function(res) {
        that.setData({
          hotelId: res.data,
        })
      }
    })
    var checkInDate = getDate.checkInDate;
    var checkOutDate = getDate.checkOutDate;
    if (wx.getStorageSync("date") == 1) {
      c._get({
        url: web_url2, //仅为示例，并非真实的接口地址
        data: {
          hotelId: app.globalData.hotel,
          // sessionId: sessionId,
          beginTimeStr: checkInDate,
          endTimeStr: checkOutDate,
        },
        success: function(res) {
          if (res.data.success == true) {
            that.setData({
              roomList: res.data.data,
            })
          }
        }
      })
    }

  },
  //日期相减得到天数
  getDays: function(stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },
  // 轮播图图片自适应
  imageLoad: function(e) {
    //获取图片真实宽度  
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比  
      ratio = imgwidth / imgheight;
    //计算的高度值  
    var viewHeight = 750 / ratio;
    var imgheight = viewHeight
    var imgheights = this.data.imgheights
    //把每一张图片的高度记录到数组里  
    imgheights.push(imgheight)
    this.setData({
      imgheights: imgheights,
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl:app.globalData.share_url
    }
  },
  //地图
  toMap:function(e){
    console.log(e.currentTarget.dataset)
    var lat = e.currentTarget.dataset.latitude;
    var lng = e.currentTarget.dataset.longitude;
    var address = e.currentTarget.dataset.adress;
    var b = this.b2p(lng, lat);
    // wx.getLocation({//获取当前经纬度
    //   type: 'gcj02', //返回可以用于wx.openLocation的经纬度，官方提示bug: iOS 6.3.30 type 参数不生效，只会返回 wgs84 类型的坐标信息  
    //   success: function (res) {
        let latitude = b.lat;
        let longitude = b.lng;
        wx.openLocation({//​使用微信内置地图查看位置。
          latitude,//要去的纬度-地址
          longitude,//要去的经度-地址
          scale:14,
          name: address,

        })
    //   }
    // })
  },
  //百度转
  b2p: function (lng, lat) {
    let x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    let x = lng - 0.0065;
    let y = lat - 0.006;
    let z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
    let theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
    let lngs = z * Math.cos(theta);
    let lats = z * Math.sin(theta);
    return {
      lng: lngs,
      lat: lats
    }
  },

  // 房间详情 弹窗
  powerDrawer1: function(e) {
    var that = this;
    var isDetail = that.data.showModalStatus;
    console.log(that.data.roomList)
    var roomList = that.data.roomList
    console.log(e.currentTarget.dataset)
    var index = e.currentTarget.dataset.index;
    console.log(index)
    var currentStatu = '';
    var night = 1;
    var g_url = app.globalData.web_url1;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/rooms/getRoomInfo";
    var sessionId = wx.getStorageSync('sessionId');
    var bookable = isDetail ? that.data.bookable : roomList[index].bookable;
    var hotelId = !isDetail ? e.currentTarget.dataset.hotelid : that.data.hotelId;
    var roomId = isDetail ? that.data.roomId : e.currentTarget.dataset.roomid;
    //var book = isDetail ? roomList[index].bookable : e.currentTarget.dataset.book;
    //var hotelId = e.currentTarget.dataset.hotelid;
    //var roomId = e.currentTarget.dataset.roomid;
    //var book = e.currentTarget.dataset.book;
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkInDate = getDate.checkInDate;
    var checkOutDate = getDate.checkOutDate;
    var day = that.getDays(checkInDate, checkOutDate);
    var allmsg = '';
    var allmsg1 = '';
    var price_daynum = 0;
    that.setData({
      day: day,
      //book: book,
      bookable: bookable,
    })

    console.log(hotelId)
    if (this.data.showModalStatus) {
      currentStatu = 'close';
    } else {
      currentStatu = 'open';
    }
    this.util(currentStatu)

    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        sessionId: sessionId,
        hotelId: hotelId,
        roomId: roomId,
        beginTimeStr: checkInDate,
        endTimeStr: checkOutDate
      },

      success: function (res) {
        console.log(res)
        //allmsg = res.data.data;
        allmsg1 = res.data.data; 
        var a = 1;
        var day_num = wx.getStorageSync("num");
        var sum = (Number(res.data.data.discountPrice) * day_num * night - Number(that.data.discount)).toFixed(2);
        that.setData({
          picUrl: g_url,
          hotelId: res.data.data.hotelId,
          name: res.data.data.name,
          residentLimit: res.data.data.residentLimit,
          facility: res.data.data.facilities,
          roomId: res.data.data.roomId,
          policies: res.data.data.policies,
          payType: res.data.data.payType,
          sum: sum,
          sum_old: sum,
          roomPics: res.data.data.roomPics,
          //allmsg: res.data.data,
          allmsg1: res.data.data,
          price_old: Number(res.data.data.discountPrice) * day_num * night,
          prepare_price: Number(res.data.data.originalPrice) * day_num * night,
          price_daynum: Number(res.data.data.discountPrice) * day_num * night,
        })
        price_daynum = Number(res.data.data.discountPrice) * day_num * night;
        //转换房间会员价浮点数 
        //var originalPrice = allmsg.originalPrice;
        //var discountPrice = allmsg.discountPrice;
        var originalPrice = allmsg1.originalPrice;
        var discountPrice = allmsg1.discountPrice;
        var memberDiscount = Number((res.data.data.originalPrice - res.data.data.discountPrice) * day_num * night).toFixed(2);
        that.setData({
          originalPrice: originalPrice,
          discountPrice: discountPrice,
          memberDiscount: memberDiscount
        })
        wx.hideLoading();
      }
    })
  },

  // 拨打电话 弹窗
  powerDrawer: function(e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
    console.log(currentStatu)
  },
  util: function(currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例 
    var animation = wx.createAnimation({
      duration: 200, //动画时长
      timingFunction: "linear", //线性
      delay: 0 //0则不延迟
    });

    // 第2步：这个动画实例赋给当前的动画实例
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画
    setTimeout(function() {
      // 执行第二组动画：Y轴不偏移，停
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象
      this.setData({
        animationData: animation
      })

      //关闭抽屉
      if (currentStatu == "close") {
        this.setData({
          showModalStatus: false
        });
      }
    }.bind(this), 200)

    // 显示抽屉
    if (currentStatu == "open") {
      this.setData({
        showModalStatus: true
      });
    }
  }
})