Page({
  data: {
    ding:"../../../images/home/ding.png",
    latitude: 40.021951831527105,
    longitude: 116.37242205143485,
    markers: [{
      id: 0,
      latitude: 39.90758977846886,
      longitude: 116.41049903446694,
  
    }],
  },
  onReady: function (e) {
    this.mapCtx = wx.createMapContext('map')
  },
  getCenterLocation: function () {
    var  that = this;
    this.mapCtx.getCenterLocation({
      success: function (res) {
        console.log(res)
        // that.setData({
        //   latitude: 23.12920192171997,
        //   longitude: 113.3599680743408,
        // })
      }
    })
  },
  moveToLocation: function () {
    this.mapCtx.moveToLocation()
  },
  translateMarker: function () {
    this.mapCtx.translateMarker({
      markerId: 1,
      autoRotate: true,
      duration: 1000,
      destination: {
        latitude: 23.10229,
        longitude: 113.3345211,
      },
      animationEnd() {
        console.log('animation end')
      }
    })
  },
  includePoints: function () {
    this.mapCtx.includePoints({
      padding: [10],
      points: [{
        latitude: 23.10229,
        longitude: 113.3345211,
      }, {
        latitude: 23.00229,
        longitude: 113.3345211,
      }]
    })
  },
  click: function (e) {

  },
  //初始化地图   
  onLoad:function(e){
    //经纬度
    var latitude = e.latitude;
    var longitude = e.longitude;
    var address = e.address;
    var b = this.b2p(longitude,latitude);
    console.log(Number(latitude))
    console.log(b)
    this.setData({
      latitude: b.lat,
      longitude: b.lng,
      address: address,
      markers: [{
        id: 0,
        latitude: b.lat,
        longitude: b.lng,
        
      }],
    })
    var that = this;
    // wx.getLocation({//获取当前经纬度
    //   type: 'gcj02', //返回可以用于wx.openLocation的经纬度，官方提示bug: iOS 6.3.30 type 参数不生效，只会返回 wgs84 类型的坐标信息  
    //   success: function (res) {
    //     const latitude = that.data.latitude
    //     const longitude = that.data.longitude
    //     wx.openLocation({//​使用微信内置地图查看位置。
    //       latitude,//要去的纬度-地址
    //       longitude,//要去的经度-地址
    //       scale:28,
    //       name: address,
    //       address: address
    //     })
    //   }
    // })
  },
   b2p:function(lng, lat){
    let x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    let x = lng - 0.0065;
    let y = lat - 0.006;
    let z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
    let theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
    let lngs = z * Math.cos(theta);
    let lats = z * Math.sin(theta);
    return {
      lng: lngs,
      lat: lats
    }
  }

})
