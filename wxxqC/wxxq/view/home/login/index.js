// wxxq/view/home/login/index.js
var c = require("../../common/common.js");
var app = getApp()
Page({
  data: {
    send: false,
    alreadySend: false,
    second: 60,
    disabled: true,
    buttonType: '',
    phoneNum: '',
    code: '',
    otherInfo: '',
    mobiles: "../../../images/home/mobile@2x.png",
    codes: "../../../images/home/security@2x.png",
    //判断是否为iPhone X 默认为值false，iPhone X 值为true
    isIphoneX: app.globalData.isIphoneX,
    toUrl: '/wxxq/view/home/index/index',
  },
  onLoad: function(e) {
    var nav = e;
    var navIndex = e.nav;
    var that = this;
    //下单
    if (navIndex == 0) {
      // that.setData({
      //   // toUrl: '/wxxq/view/home/reserve/reserve?nav=0&hotelId=' + nav.hotelId + " &roomId=" + nav.roomId + "&vip=" + nav.vip + "&price=" + nav.price + "&beginTimeStr=" + nav.beginTimeStr + "&endTimeStr=" + nav.endTimeStr + "&book=" + nav.book
      // })
    }
    //订单
    if(navIndex == 2){
        that.setData({
          toUrl: '/wxxq/view/mine/order/allOrder'
        })
    }
    //早餐
    if (navIndex == 1) {
      that.setData({
        toUrl: '/wxxq/view/break/breakfast/breakfast'
      })
    }
    //我的
    if (navIndex == 3) {
      that.setData({
        toUrl: '/wxxq/view/mine/mine/mine'
      })
    }
    if (navIndex == 4) {
      that.setData({
        toUrl: '/wxxq/view/wifi/wifi'
      })
    }
    if (navIndex == 5) {
      that.setData({
        toUrl: '/wxxq/view/invoice/order/order'
      })
    }
    c._set_iphoneX(that);
    var sessionId = wx.getStorageSync('sessionId');

  },

  // 手机号部分
  inputPhoneNum: function(e) {
    let phoneNum = e.detail.value
    this.setData({
      phoneNum: phoneNum
    })
    this.showSendMsg()
    this.activeButton()
  },
  //手机号验证
  checkPhoneNum: function(phoneNum) {
    let str = /^[1][3,4,5,6,7,8,9][0-9]{9}$/
    if (!str.test(phoneNum)) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none',
        duration: 2000
      });
      return false
    } else {
      return true
    }
  },
  showSendMsg: function() {
    if (!this.data.alreadySend) {
      this.setData({
        send: true
      })
    }
  },

  hideSendMsg: function() {
    this.setData({
      send: false,
      disabled: false,

    })
  },
  // 调取发送验证码接口
  sendMsg: function() {
    var that = this;
    var phone = that.data.phoneNum;
    var check_res = this.checkPhoneNum(phone);
    if (!check_res) {
      send: false;
      return false;
    }
    var web_url = app.globalData.web_url;
    var sessionId = wx.getStorageSync('sessionId');


    var web_url1 = web_url + "/sms/sendvercode";
    c._post({
      url: web_url1,
      data: {
        sessionId: sessionId,
        mobile: phone,
        hotelId: app.globalData.hotel,

        type: "1",
        sign: "1",
      },
      success: function(res) {
        console.log(res)
      }
    })
    this.setData({
      alreadySend: true,
      send: false
    })
    this.timer()
  },
  // 60s倒计时
  timer: function() {
    let promise = new Promise((resolve, reject) => {
      let setTimer = setInterval(
        () => {
          this.setData({
            second: this.data.second - 1
          })
          if (this.data.second <= 0) {
            this.setData({
              second: 60,
              alreadySend: false,
              send: true
            })
            resolve(setTimer)
          }
        }, 1000)
    })
    promise.then((setTimer) => {
      clearInterval(setTimer)
    })
  },



  // 验证码
  addCode: function(e) {
    this.setData({
      code: e.detail.value
    })
    this.activeButton()
  },

  // 按钮
  activeButton: function() {
    let {
      phoneNum,
      code,

    } = this.data
    // 正式获取验证码时
    if (phoneNum && code) {
      // if (phoneNum) {
      this.setData({
        disabled: false,

      })
    } else {
      this.setData({
        disabled: true,

      })
    }
  },
  // 登录按钮提交
  onSubmit: function() {
    wx.showLoading({
      title: '登录中',
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url2 = web_url + "/members/bindmobile";
    console.log(web_url2);
    var sessionId = wx.getStorageSync('sessionId');
    var type = wx.getStorageSync('type')

    c._get({
      url: web_url2,
      data: {
        mobile: that.data.phoneNum,
        vercode: that.data.code,
      },
      success: function (res) {
        console.log(res)
        if ((parseInt(res.statusCode) == 200)) {
          if (res.data.success) {

            wx.setStorage({
              key: "sessionId",
              data: res.data.data.sessionId,

            })
            wx.setStorage({
              key: "type",
              data: res.data.data.type,
            })
            wx.showToast({
              title: '登录成功',
              icon: 'success'
            })
            //跳转至首页
            wx.reLaunch({
              url: that.data.toUrl,
            })
          } else {
            wx.showToast({
              icon: 'none',
              title: "验证码错误",
              duration: 1000

            })
          }

        } else {
          wx.showToast({
            icon: 'none',
            title: "登录失败",
            duration: 1000

          })

        }
      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },

})