// wxxq/view/wifi/wifi.js
var c = require("../common/common.js");
var app = getApp()
var h_Id = app.globalData.hotel;
Page({
  backIndex:function(){
    
    wx.redirectTo({
      url: '/wxxq/view/home/index/index',
    })
  },
  Reconnect: function () {
    var that = this;
    that.setData({
      // wifiS: "",
      show: false,

    })
  },
  connectWifi: function () {
    var that = this;

    //检测手机型号

    wx.getSystemInfo({

      success: function (res) {

        var system = '';

        if (res.platform == 'android') system = parseInt(res.system.substr(8));

        if (res.platform == 'ios') system = res.system.substring(4, 8);
        console.log(system)
        if (res.platform == 'android' && system < 6) {

          wx.showToast({

            title: '手机版本不支持',

          })

          return

        }

        if (res.platform == 'ios' && system < 11.2) {

          wx.showToast({

            title: '手机版本不支持',

          })

          return

        }



        //2.初始化 Wi-Fi 模块

        that.startWifi();

      }

    })

  },


  startWifi: function () {

    var that = this

    wx.startWifi({

      success: function () {



        that.Connected();

      },

      fail: function (res) {
            //  wx.showToast({

            //     title: '接口调用失败',

            //   })
        // this.setData({

         

        // });

      }

    })

  },



  Connected: function () {

    var that = this
    console.log(that.data.accountNumber)
    console.log(that.data.password)
    wx.connectWifi({
      SSID: that.data.accountNumber,
      BSSID: that.data.bssid,
      password: that.data.password,
      success: function (res) {
        console.log(res)
       
          wx.showToast({

            title: 'wifi连接成功',

          })
          that.setData({
            // wifiS: true,
            show: true,

          }) 
       

      },

      fail: function (res) {
        console.log("shibai")
        
        wx.showToast({
          title: 'wifi连接失败',
        })
        that.setData({
          // wifiS: false,
          show: true
        }) 
       
      }

    })

  },
 
  /**
   * 页面的初始数据
   */
  data: {
    status:true,
    hotelName:'',
    accountNumber: '',//Wi-Fi 的SSID，即账号
    bssid: '',//Wi-Fi 的ISSID
    password: '',//Wi-Fi 的密码
    // wifiS:true,
    show:false  
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/hotels/" + h_Id + "?type=1";
    // var sessionId = wx.getStorageSync('sessionId');
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {

        hotelId: app.globalData.hotel,
      },
      success: function (res) {
        
        if (res.data.success == true) {
          that.setData({
            hotelName: res.data.data.hotelName,
            accountNumber: res.data.data.wifiName,
            password: res.data.data.wifiPassword,//Wi-Fi 的密码

          })
       
          if (res.data.data.wifiStatus=='0'){
            that.setData({
              status:false
            })
          }else{
            that.setData({
              status: true
            })
          }
         
        }
      }
    })
    // 判断授权
      wx.getSetting({
        success: function (res) {
          if (res.authSetting['scope.userInfo']) {
            
          } else {
           
            //授权
            wx.authorize({
              scope: 'scope.userInfo',
              success:function(){
                wx.redirectTo({
                  url: "/wxxq/view/wifi/wifi"
                })
              },
              fail:function(){
                wx.redirectTo({
                  url: "/wxxq/view/home/author/author"
                })
              }
            })
            return;
          }
        }
      })

    var web_url = app.globalData.web_url;
    c._is_login({
      web_url: web_url,
      success: function (e) {
      }
    })
    var status = wx.getStorageSync('type');//登入状态1为未登录
    console.log(status)
    if (status==1){//未登录
      wx.redirectTo({
        url: '/wxxq/view/home/login/index?nav=4',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})