var c = require('../../common/common.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //判断是否为iPhone X 默认为值false，iPhone X 值为true
    isIphoneX: app.globalData.isIphoneX,
    today:'',
    homeId:808,
    flag:true,
    breakfast:1,
    array: [],
    index: 0,
    arNum:1,
    flag2: false,
    flag3:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    var web_url = app.globalData.web_url;
    // 设置iphoneX
    c._set_iphoneX(that);
    // 设置底部
    c._set_foot(that, 1);
    var type = wx.getStorageSync('type');
    if (type == 1) {
      this.setData({
        is_login: 1,
      })
      wx.hideLoading();
      return;
    }
    if (type == 2) {
      this.setData({
        is_login: 2
      })
    }
    that.setData({
      web_url: web_url,
    });
    //获取订单数
    c._getN(that)
    
    // 判断是否授权
    c._is_author({
      success: function () {
        c._is_login({ 
          web_url: web_url,
          success: function (e) {
            var web_url = app.globalData.web_url;
            var web_url1 = web_url + "/breakfastcoupon/count";
            var web_url2 = web_url + "/diningRoom/list";
            var sessionId = wx.getStorageSync('sessionId');
            c._get({
              url: web_url1, //仅为示例，并非真实的接口地址
              
              success: function (res) {
                if (res.data.success) {
                  var a = res.data.data || 0
                  that.setData({
                    today: a,
                    index:0,
                  })
                  //早餐卷使用张数
                  var breakN = a > 15 ? 15 : a;
                  // console.log(breakN)
                  var arr = []
                  for (var i = 1; i <= breakN; i++) {
                    arr.push(i)
                  }
                  that.setData({
                    array:arr,
                  })
                  wx.hideLoading();
                }
              },
              fail: function (e) {

              }
            });
            c._get({
              url: web_url2, //仅为示例，并非真实的接口地址
              success: function (res) {
                if (res.data.success && res.data.data.length != 0) {
                  wx.setStorage({
                    key: 'breakprice',
                    data: res.data.data[0].unitPrice,
                  })
                  that.setData({
                    flag3: true,
                  })
                  wx.hideLoading();
                } else {
                  that.setData({
                    flag2: true,
                  })
                  wx.hideLoading();
                }
              },
              fail: function (e) {

              }
            })
          }
        })
      }
    });
    wx.hideLoading();
  },
  onShow: function (options) {
    var that = this;
    var web_url = app.globalData.web_url;
    var type = wx.getStorageSync('type');
    if (type == 1) {
      this.setData({
        is_login: 1,
      })
      return;
    }
    if (type == 2) {
      this.setData({
        is_login: 2
      })
    }
    that.setData({
      web_url: web_url,
    });
    // 设置iphoneX
    c._set_iphoneX(that);
    // 设置底部
    c._set_foot(that, 1);
    // 判断是否授权
    c._is_author({
      success: function () {
        c._is_login({
          web_url: web_url,
          success: function (e) {
            var web_url = app.globalData.web_url;
            var web_url1 = web_url + "/breakfastcoupon/count";
            var web_url2 = web_url + "/diningRoom/list";
            var sessionId = wx.getStorageSync('sessionId');
            c._get({
              url: web_url1, //仅为示例，并非真实的接口地址

              success: function (res) {
                if (res.data.success) {
                  var a = res.data.data || 0
                  that.setData({
                    today: a,
                    index: 0,
                  })
                  //早餐卷使用张数
                  var breakN = a > 15 ? 15 : a;
                  // console.log(breakN)
                  var arr = []
                  for (var i = 1; i <= breakN; i++) {
                    arr.push(i)
                  }
                  that.setData({
                    array: arr,
                  })
                }
              },
              fail: function (e) {

              }
            });
            c._get({
              url: web_url2, //仅为示例，并非真实的接口地址
              success: function (res) {
                if (res.data.success && res.data.data.length != 0) {
                  wx.setStorage({
                    key: 'breakprice',
                    data: res.data.data[0].unitPrice,
                  })
                  that.setData({
                    flag3: true,
                  })

                } else {
                  that.setData({
                    flag2: true,
                  })
                }
              },
              fail: function (e) {

              }
            })
          }
        })
      }
    });

  },
  //餐厅列表
  toDining:function(){
    wx.navigateTo({
      url: '../diningList/diningList'
    })
  },
  //早餐列表
  toList:function(){
    wx.navigateTo({
      url: '../breakList/breakList',
    })
  },
  // 遮罩
  clickShade:function(){
    this.setData({
      flag:!this.data.flag,
    })
  },
  bindPickerChange: function (e) {
    this.setData({
      index: e.detail.value,
      arNum: this.data.array[e.detail.value]
    })
  },
  
  //优惠券使用
  tUse: function () {
    var time = new Date().getHours();
    if(time < 11){
      //遮罩显示
      this.setData({
        flag: !this.data.flag,
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '今日早餐时间已过',
        success(res) {
          if (res.confirm) {
           
          } else if (res.cancel) {

          }
        }
      })
    }
    
  },
  //优惠券使用跳转
  sub_commit: function () {
    wx.navigateTo({
      url: '/wxxq/view/break/breakfastDeatil/deatil?num='+this.data.arNum,
    })
    //遮罩隐藏
    this.setData({
      flag: !this.data.flag,
    })
  },
  //优惠券购买
  tPay: function () {
    wx:wx.navigateTo({
      url: '/wxxq/view/break/breakbuy/breakbuy',
    })
  },
  //使用购买
  tShow:function(){
    wx.showModal({
      title: '提示',
      content: '今日您没有可使用早餐券，是否前往购买',
      success(res) {
        if (res.confirm) {
          wx: wx.navigateTo({
            url: '/wxxq/view/break/breakbuy/breakbuy',
          })
        } else if (res.cancel) {
          
        }
      }
    })
  },
  //请先登录
  t_login:function(){
    wx.showToast({
      title: '请先登录',
      icon:"none",
      duration:1500,
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
  onShow:function(){
    //获取订单数
    c._getN(this)
  }

})