// wxxq/view/home/breakbuy/breakbuy.js
var util = require('../../../../utils/_util.js');
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
   
    array: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'],
    index:0,
    num:1,//早餐计数
    price:30,//早餐价格
    totalPrice:30, //总计早餐价
    sumNum:1, //早餐总数量
    numDaily:1,
    payFlag:true, //支付开关
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    let that = this;
    wx.getStorage({
      key: 'breakprice',
      success: function(res) {
        that.setData({
          price:res.data,
          totalPrice:res.data,
        })
      },
    })
    // 调用函数时，传入new Date()参数，返回值是日期和时间  
    // 再通过setData更改Page()里面的data，动态更新页面的数据
    var time = util.formatTime(new Date());
    this.setData({
      dateEnd: util.formatTime(new Date(new Date().getTime() + 24 * 60 * 60 * 1000)),
      dateStart: util.formatTime(new Date(new Date().getTime() + 24 * 60 * 60 * 1000)),
    });
    //设缓存缓存起来的日期
    wx.setStorage({
      key: 'BREAK_DATE',
      data: {
        checkInDate: util.formatTime(new Date(new Date().getTime() + 24 * 60 * 60 * 1000)),
        checkOutDate: util.formatTime(new Date(new Date().getTime() + 24 * 60 * 60 * 1000)),
      }
    });
    wx.hideLoading();
  },
 
  //早餐选择张数
  bindPickerChange: function (e) {
    this.setData({
      index: e.detail.value,
      numDaily:this.data.array[e.detail.value],
      sumNum:this.data.array[e.detail.value] * this.data.num,
      totalPrice: this.data.array[e.detail.value] * this.data.num * this.data.price,
    })
  },
  //早餐时间选择
  toTime:function(){
    wx.navigateTo({
      url: '/wxxq/view/break/calendar/index?checkInDate=' + this.data.dateStart + '&checkOutDate=' + this.data.dateEnd,
    })
  },
  onShow:function(){
    var that = this;
    // 计算入住时间共几晚
    let getDate = wx.getStorageSync("BREAK_DATE");
    console.log(getDate)
    var newDate = util.formatTime(new Date());
    var setNum = this.getDays(newDate,getDate.checkInDate);
    var num = this.getDays(getDate.checkInDate, getDate.checkOutDate);
    //早餐开始时间大于当天
    if (setNum == 0){
      
      if (num == 0 && (new Date().getHours()) < 11) {
        this.setData({
          dateStart: getDate.checkInDate,
          dateEnd: getDate.checkOutDate,
          num: 1,
          sumNum:1,
          totalPrice: 1 * that.data.price,
          index: 0,
        })
      }
      //多早餐并且包含当天早餐
      if (num > 0 && (new Date().getHours()) > 11) {
        this.setData({
          dateStart: getDate.checkInDate,
          dateEnd: getDate.checkOutDate,
          num: num,
          sumNum: num,
          totalPrice: num * that.data.price,
          index: 0,
        })
      }else{
        this.setData({
          dateStart: getDate.checkInDate,
          dateEnd: getDate.checkOutDate,
          num: num + 1,
          sumNum: num + 1,
          totalPrice: (num + 1) * that.data.price,
          index: 0,
        })
      }
      // console.log(num)
    }else{
      this.setData({
        dateStart: getDate.checkInDate,
        dateEnd: getDate.checkOutDate,
        num: num + 1,
        sumNum: num + 1,
        totalPrice: (num + 1) * that.data.price,
        index: 0,
      })
    }
   
  },
  //日期相减得到天数
  getDays: function (stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },
  pay:function(e){
    let that = this;
    let web_url = app.globalData.web_url;
    let web_url1 = web_url + "/orderBreakfast/buyAndPay"; //
    if(that.data.payFlag){
      that.setData({
         payFlag:false,
      })
      //今日可用
      c._post({
        url: web_url1, //仅为示例，并非真实的接口地址
        data: {
          validStart: that.data.dateStart,
          validEnd: that.data.dateEnd,
          amount: that.data.sumNum,
          formId: e.detail.formId,
          numDaily: that.data.numDaily,
        },
        success: function (e) {
          if (e.data.success) {
            wx.requestPayment({
              timeStamp: e.data.data['timeStamp'],
              nonceStr: e.data.data['nonceStr'],
              package: e.data.data['package'],
              signType: e.data.data['signType'],
              paySign: e.data.data['paySign'],
              success: function (res) {
              
                var notice = "支付成功";
                setTimeout(function () {
                  wx.redirectTo({
                    url: '/wxxq/view/mine/success/success?notice=' + notice + "&index=3&type=2",
                  })
                }, 500)
                
              },
              fail: function (res) {
                var notice = "支付失败";
                setTimeout(function() {
                  wx.redirectTo({
                    url: '/wxxq/view/mine/defeat/defeat?notice=' + notice + "&index=3&type=2",
                  })
                }, 500)
                
              },
              complete: function (res) { },
            })
          } else {
            var notice = "支付失败";
            setTimeout(function () {
              wx.redirectTo({
                url: '/wxxq/view/mine/success/success?notice=' + notice + "&index=3&type=2",
              })
            }, 2000)
          }
        },
        fail: function (e) {

        }
      });
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})