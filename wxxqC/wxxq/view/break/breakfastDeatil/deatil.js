// wxxq/view/home/breakfastDeatil/deatil.js
var c = require("../../common/common.js");
var util = require('../../../../utils/_util.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    eCode:'',
    time:'',
    room:'',
    num:''

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (o) {
    let that = this;
    let web_url = app.globalData.web_url;
    let web_url1 = web_url + "/breakfastcoupon/useBreakfastCoupon"; //
    let sessionId = wx.getStorageSync('sessionId');
    //今日可用
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data:{
       use_amount:o.num,
        use_date: util.formatTime(new Date()),
      },
      success: function (res) {
        if (res.data.success) {
          that.setData({
            eCode: app.globalData.com_url + res.data.data.url,
            time: res.data.data.useDate,
            num: res.data.data.useAmount,

          })
        }
      },
      fail: function (e) {

      }
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})