// wxxq/view/home/diningList/diningList.js
var c = require("../../common/common.js");
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr:[], //餐厅列表
    flag:true,
    page:2,
    flag2:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    // 设置iphoneX;
    c._set_iphoneX(that);
    // 判断是否授权
    
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/diningRoom/list";
    var sessionId = wx.getStorageSync('sessionId');
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        page: 1,
        rows: 5
      },
      success: function (res) {
        if(res.data.success && res.data.data.length > 0){
            that.setData({
                arr:res.data.data
            })
        }else{
            that.setData({
              flag: false,
            })
        }
        wx.hideLoading();
      },
      fail: function (e) {

      }
    })
       
  
  },
  onReachBottom:function(){
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/diningRoom/list";
    if(that.data.flag2){
      that.setData({
        flag2: false,
      })
      c._get({
        url: web_url1, //仅为示例，并非真实的接口地址
        data: {
          page: that.data.page,
          rows: 5,
        },
        success: function (res) {
          if (res.data.success && res.data.data.length > 0) {
            that.setData({
              arr: that.data.arr.concat(res.data.data),
              flag2: true,
              page:that.data.page + 1,
            })
          }
        },
        fail: function (e) {

        }
      })
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})