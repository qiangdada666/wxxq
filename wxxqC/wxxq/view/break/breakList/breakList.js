// wxxq/view/home/breakList/breakList.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navIndex:0,
    //当前列表
    arr:[], //今日可用
    arr2:'',//以后可用
    flag:true, //今日可用
    flag1: true,
    flag2: true,  //以后可用
    flag3: true,
    //历史
    usedarr: [],
    array: ['1'],
    index: 0,
    dateTime:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var time = new Date().getHours();
    var web_url1 = web_url + "/breakfastcoupon/today"; //今日
    var web_url2 = web_url + "/breakfastcoupon/after"; //以后
    var web_url3 = web_url + "/breakfastcoupon/history"
    var sessionId = wx.getStorageSync('sessionId');
    //今日可用
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      success: function (res) {
        if (res.data.success && res.data.data.length != 0) {
          that.setData({
            arr: res.data.data,
            dateTime:time,
          })
          
        }else{
          that.setData({
            flag2: !that.data.flag2,
          })
        }
        
      },
      fail: function (e) {

      }
    });
    //以后可用
    c._get({
      url: web_url2, //仅为示例，并非真实的接口地址
      success: function (res) {
        if (res.data.success && res.data.data.length != 0) {
          that.setData({
            arr2: res.data.data,
            flag: that.data.flag,
          })
        } else {
          that.setData({
            flag: !that.data.flag,
          })
        }
      },
      fail: function (e) {

      },
      
    });
    //历史
    c._get({
      url: web_url3, //仅为示例，并非真实的接口地址
      success: function (res) {
        if (res.data.success && res.data.data.length != 0) {
          that.setData({
            flag3: !that.data.flag3,
            usedarr: res.data.data,
          })
        }
      },
      fail: function (e) {

      }
    })
    wx.hideLoading();
  },
  //当前列表
  clickL:function(){
    this.setData({
      navIndex:0
    })
  },
  //历史
  clickR:function(){
    this.setData({
      navIndex: 1
    })
  },
  // 遮罩
  clickShade: function () {
    this.setData({
      flag1: !this.data.flag1,
    })
  },
  //优惠券使用
  tUse: function () {
    //遮罩显示
    this.setData({
      flag1: !this.data.flag1,
    })
  },
  //优惠券使用跳转
  sub_commit: function () {
    wx.redirectTo({
      url: '/wxxq/view/break/breakfastDeatil/deatil?num=1',
    })
    //遮罩隐藏
    this.setData({
      flag1: !this.data.flag1,
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },

})