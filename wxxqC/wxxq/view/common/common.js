var app = getApp();
// console.log(app.globalData.hotel)
var hotelId = app.globalData.hotel;
// 判断授权
function is_author(a) {
  wx.getSetting({
    success: function (res) {
      if (res.authSetting['scope.userInfo']) {
        a.success();
      } else {
        // wx.redirectTo({
        //   url: "/wxxq/view/home/author/author"
        // })
        //授权
        wx.authorize({
          scope: 'scope.userInfo',
          success:function(){
            wx.redirectTo({
              url: '/wxxq/view/home/index/index',
            })
          },
          fail:function(){
            wx.redirectTo({
              url: "/wxxq/view/home/author/author"
            })
          }
        })
        return;
      }
    }
  })
}
//联系酒店
function _calling() {
  wx.makePhoneCall({
    phoneNumber: '01082888',
    success: function () {
      console.log("拨打电话成功！")
    },
    fail: function () {
      console.log("拨打电话失败！")
    }
  })
}
// get请求参数
function _get(a) {
  var sessionid = wx.getStorageSync("sessionId");
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/json',
      "sessionId": sessionid, // 默认值
    },
    dataType: "json",
    method: "GET",
    success: function (e) {
      a.success(e)
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// get请求参数
function _spe_get(a) {
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/json',

    },
    dataType: "json",
    method: "GET",
    success: function (e) {
      a.success(e)
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// post请求参数
function _post(a) {
  var sessionId = wx.getStorageSync("sessionId")
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/x-www-form-urlencoded',// 默认值
      "sessionId": sessionId,
    },
    dataType: "json",
    method: "POST",
    success: function (e) {
      a.success(e);
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// post请求参数
function _spe_post(a) {
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/x-www-form-urlencoded',// 默认值
    },
    dataType: "json",
    method: "POST",
    success: function (e) {
      a.success(e);
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// 设置iphoneX
function set_iphoneX(a) {
  var isIphoneX = false;
  wx.getSystemInfo({
    success: res => {
      // console.log('手机信息res'+res.model)
      let modelmes = res.model;
      if (modelmes.search('iPhone X') != -1) {
        isIphoneX = true
      }
      a.setData({
        isIphoneX: isIphoneX,
      })

    }
  })
}
// 设置底部
function set_foot(a, b) {
  var footList = [
    {
      text: "首页",
      pressImg: "../../../images/common/tab_sy_mr@2x.png",
      seleImg: "../../../images/common/tab_sy_xz@2x.png",
      url: "/wxxq/view/home/index/index",
      active: false
    },
    {
      text: "早餐",
      pressImg: "../../../image/breakFast.png",
      seleImg: "../../../image/bearkFastcheck.png",
      url: "/wxxq/view/break/breakfast/breakfast",
      active: false
    },
    {
      text: "订单",
      pressImg: "../../../images/common/tab_dd_mr@2x.png",
      seleImg: "../../../images/common/tab_dd_xz@2x.png",
      url: "/wxxq/view/mine/order/allOrder?index=3",
      active: false
    },
    {
      text: "我的",
      pressImg: "../../../images/common/tab_wd_mr@2x.png",
      seleImg: "../../../images/common/tzb_wd_xz@2x.png",
      url: "/wxxq/view/mine/mine/mine",
      active: false
    }
  ];
  footList[b].active = true;

  a.setData({
    footList: footList
  })
}
// 登录
function _login(a) {
  wx.login({
    success: function (res) {
      var code = res.code;
      if (code) {
        _spe_post({
          url: a.web_url + "/members/login",
          data: { hotelId: a.hotelId, code: code },
          success: function (e) {
            console.log(e)
            wx.setStorage({
              key: "sessionId",
              data: e.data.data.sessionId,
            })
            wx.setStorage({
              key: "sessionId_old",
              data: e.data.data.sessionId,
            })
            wx.setStorage({
              key: "type",
              data: e.data.data.type,
            })
            a.success();
            // 获取用户信息
            wx.getUserInfo({
              success: function (res) {
                console.log(res.userInfo)
                var sessionid = wx.getStorageSync("sessionId")
                wx.request({
                  url: a.web_url + "/members/saveMemberInfo",
                  method: "POST",
                  header: {
                    'content-type': 'application/x-www-form-urlencoded',
                    "sessionId": sessionid,
                    // 默认值
                  },
                  data: {
                    name: res.userInfo.nickName,
                    mobile: '13436885017',
                    status: 1,
                    country: res.userInfo.country,
                    province: res.userInfo.province,
                    city: res.userInfo.city,
                    headImgUrl: res.userInfo.avatarUrl,
                    district: '评语',
                    language: res.userInfo.language,
                    sex: res.userInfo.gender,
                    nickName: res.userInfo.nickName
                  },
                  success: function (e) {
                    console.log(e)
                  }
                })
              }
            })
          }
        })
      } else {
        console.log('获取用户登录态失败：' + res.errMsg);
        return;
      }
    }
  })
}
// 判断用户是否已登陆
function is_login(a) {

  wx.getStorage({
    key: 'sessionId',
    success: function (res) {
      a.success();
    },
    fail: function (res) {
      _login({
        web_url: a.web_url,
        hotelId: hotelId,
        success: function (e) {
          a.success(e)
        }
      })
    }
  })
}
function GetNum(t) {
  let that = t;
  var sessionid = wx.getStorageSync("sessionId");
  // 判断会员是否登录 start
  var type = wx.getStorageSync('type');
  if (type == 2) {
    wx.request({
      url: app.globalData.web_url + "/orders/countMemberAllTypeOrders", //仅为示例，并非真实的接口地址
      data: {},
      header: {
        'content-type': 'application/json',
        "sessionId": sessionid, // 默认值
      },
      dataType: "json",
      method: "GET",
      success: function (e) {
        if (e.data.success){
            that.setData({
              order1: e.data.data.orderCount0,//待支付
              order2: e.data.data.orderCount1,//待确认
              order3: e.data.data.orderCount2,//待入住
            })
        }
      },
      fail: function (e) {

      }
    })
  }
}
module.exports = {
  _is_author: is_author,
  _get: _get,
  _set_iphoneX: set_iphoneX,
  _post: _post,
  _set_foot: set_foot,
  _login: _login,
  _is_login: is_login,
  _is_calling: _calling,
  _getN: GetNum,
}
