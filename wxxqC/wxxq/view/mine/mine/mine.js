// wxxq/view/mine/mine/mine.js
var c = require("../../common/common.js");
var app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    prompt: "确定要退出登录吗？",
    userInfo: null,
    isIphoneX: app.globalData.isIphoneX,
    index: 4,
    // 判断注册弹框是否出现
    showModal: false,
    //默认头像
    userImg: '../../../images/common/urseImg.png',
    // 底部导航
    top: {
      headImgUrl: "../../../images/mine/wd_tx@2x.png",
      nickName: "",
      memberName: "",
      vip: "../../../images/mine/wd_vip@2x.png"

    },
    vip: [
      { vip: '../../../images/icon/00.png', name: '预定保留' },
      { vip: '../../../images/icon/01.png', name: '房价折扣' },
      { vip: '../../../images/icon/02.png', name: '延迟退房' },
      { vip: '../../../images/icon/03.png', name: '免费洗衣' }
    ],
    jiantou: "../../../images/home/jt@2x.png",
    orderList: [{
      icon: "../../../images/mine/wd_dzf@2x.png",
      text: "待支付",
      url: "/wxxq/view/mine/order/allOrder"
    },
    {
      icon: "../../../images/mine/dai@2x.png",
      text: "待确认",
      url: "/wxxq/view/mine/order/allOrder"
    },
    {
      icon: "../../../images/mine/wd_dcx@2x.png",
      text: "待出行",
      url: "/wxxq/view/mine/order/allOrder"
    },


    ],
    balance: "808",
    discount: "2",
    realMoney: 600,
    virtMoney: 200,
    web_url: "https://192.168.1.129:8443",



  },
  
  // 退出登录操作
  loginoutClick: function (e) {
    var that = this;
    userInfo: null;
    var sessionid = wx.getStorageSync('sessionId');
    that.setData({
      is_login: 1,
      showModal: false
    })
    wx.request({
      url: app.globalData.web_url + "/members/unbindmobile",
      data: {},
      header: {
        'content-type': 'application/json',
        "sessionId": sessionid, // 默认值
      },
      dataType: "json",
      method: "GET",
      success: function (e) {
        wx.setStorageSync('type', 1);
        //保存手机号
        wx.setStorage({
          key: 'MOBILE',
          data: that.data.memberName
        })
        //保存头像
        wx.setStorage({
          key: 'HEADIMG',
          data: that.data.headImgUrl,
        })
        var sessionId_old = wx.getStorageSync("sessionId_old")
        wx.setStorageSync("sessionId", sessionId_old);
        wx.redirectTo({
          url: '/wxxq/view/home/index/index',
        })
      }
    })
  },
  /*
  
    微信分享
  
  */ 
  share:function(){
      
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    var myself = app.globalData.myself;
    that.setData({
      myself: myself,
    })
    var type = wx.getStorageSync('type');
    var userInfoTmp = app.globalData.userInfo;
    // 设置底部
    c._set_foot(that, 3);
    if (type == 1) {
      this.setData({
        is_login: 1,//1 为登录状态
        userInfo: userInfoTmp
      })
      wx.hideLoading();
      return;
    }
    if (type == 2) {
      this.setData({
        is_login: 2 //未登录
      })
    }
    // 设置iphoneX
    c._set_iphoneX(that);

    // 保存用户信息
    var web_url = app.globalData.web_url;
    var web_url2 = web_url + "/members/saveMemberInfo";
    var that = this;
    var nickName = that.data.nickName;
    var avatarUrl = that.data.avatarUrl;
    // wx.hideShareMenu()
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/members/getLoginUserInfo"; //获取用户基本信息
    var web_url2 = web_url + "/members/unbindmobile";     //退出接口
    var web_url3 = web_url + "/membercoupons/countNum";
    // 获取用户基本信息
    //获取订单数
    c._getN(that)
    // 获取优惠券数量 
    c._get({
      url: web_url1,
      data: {},
      success: function (res) {
        if(res.data.success){
          //手机号码隐私处理
          var mobile = res.data.data.mobile;
          wx.setStorage({
            key: 'Mobile2',
            data: mobile,
          })
          var mob1 = mobile.substring(0, 3);
          var mob2 = mobile.substring(8, 11);
          mobile = mob1 + "******" + mob2;
          that.setData({
            headImgUrl: res.data.data.headImgUrl,
            nickName: res.data.data.nickName,
            memberName: mobile,
            realMoney: res.data.data.realMoney.toFixed(2),
            virtMoney: res.data.data.virtMoney.toFixed(2),
            sumMoney: (res.data.data.realMoney + res.data.data.virtMoney).toFixed(2),
          })
          //设置用户余额
          wx.setStorageSync("realMoney", res.data.data.realMoney.toFixed(2));
          wx.setStorageSync("virtMoney", res.data.data.virtMoney.toFixed(2));
          wx.setStorageSync("sumMoney", (res.data.data.realMoney + res.data.data.virtMoney).toFixed(2));
        }else{
          wx.showModal({
            title: '登录失败',
            content: '获取用户信息失败，请重新登录',
            showCancel:false,
            success:function(res){
              if (res.confirm){
                var sessionid = wx.getStorageSync('sessionId');
                that.setData({
                  is_login: 1,
                  showModal: false
                })
                wx.request({
                  url: app.globalData.web_url + "/members/unbindmobile",
                  data: {},
                  header: {
                    'content-type': 'application/json',
                    "sessionId": sessionid, // 默认值
                  },
                  dataType: "json",
                  method: "GET",
                  success: function (e) {
                    wx.setStorageSync('type', 1);
                    var sessionId_old = wx.getStorageSync("sessionId_old")
                    wx.setStorageSync("sessionId", sessionId_old);
                    wx.redirectTo({
                      url: '/wxxq/view/mine/mine/mine',
                    })
                  }
                })
              }
            }

          })

        }
      }

    })
    // 获取优惠券数量
    c._get({
      url: web_url3,
      data: {},
      success: function (res) {
        console.log(res.data.data)
        that.setData({
          card: res.data.data
        })
      }
    })
    wx.hideLoading();
  },
  //分享
  onShareAppMessage:function(){
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
 
  // 弹窗1
  showDialogBtn: function () {
    this.setData({
      showModal: true,

    })
  },
  /*** 隐藏模态对话框1*/
  hideModal: function () {
    this.setData({
      showModal: false
    });

  },
  /*** 对话框取消按钮点击事件1*/
  onCancel: function () {
    this.hideModal();

  },
  /*** 对话框确认按钮点击事件1*/
  onConfirm: function () {
    this.hideModal();

  },
  //请先登录
  t_login: function () {
    wx.showToast({
      title: '请先登录',
      icon: "none",
      duration: 1500,
    })
  },
  onShow:function(){
    var that = this;
    //获取订单数
    c._getN(that)
  }


})