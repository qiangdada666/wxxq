// wxxq/view/mine/consume/consume.js
var c = require("../../common/common.js");
var Moment = require("../../../../utils/moment.js");
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    payList:[
      
      {
        payMode: "赠送",
        payTime: "",
        money: "",
        reset: "赠送金额清零"
      }

    ],
    
  
  },

  //转换时间
  dateFormat:function (date, format) {

    date = new Date(date);

    var o = {
      'M+': date.getMonth() + 1, //month
      'd+': date.getDate(), //day
      'H+': date.getHours(), //hour
      'm+': date.getMinutes(), //minute
      's+': date.getSeconds(), //second
      'q+': Math.floor((date.getMonth() + 3) / 3), //quarter
      'S': date.getMilliseconds() //millisecond
    };
    if (/(y+)/.test(format))
      format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));

    for (var k in o)
      if (new RegExp('(' + k + ')').test(format))
        format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));

    return format;
  },

dateToGMT:function(strDate) {
  var dateStr = strDate.split(" ");
  var strGMT = dateStr[0] + " " + dateStr[1] + " " + dateStr[2] + " " + dateStr[5] + " " + dateStr[3] + " GMT+0800";
  var date = new Date(Date.parse(strGMT));
  return date;
},










  /**
   * 生命周期函数--监听页面加载
   */
  // 获取消费明细接口
  onLoad: function (options) {
    var that = this;
    c._set_iphoneX(that);

    var sessionId = wx.getStorageSync('sessionId');
    
        
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/members/getConsumingRecord";
    wx.request({
      url:web_url1, //仅为示例，并非真实的接口地址
      data:{},
      header: {
        'content-type': 'application/json',
        "sessionId": sessionId , // 默认值
      },
      dataType: "json",
      method: "GET",
      success: function (res) {
      // for(var i=0;i<res.data.data.length;i++){
        
      //   res.data.data[i].payTime = that.dateFormat(that.dateToGMT(res.data.data[i].payTime), 'yyyy-MM-dd');
      // }
        console.log(res);
       that.setData({
       
          payList: res.data.data,

         
       })
        
      }
     
    })
        
    
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  }
})