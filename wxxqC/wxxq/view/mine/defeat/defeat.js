// wxxq/view/mine/defeat/defeat.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */ 
  data: {
    success: "../../../images/mine/defeat.png",
    notice: "",
    //判断是否为iPhone X 默认为值false，iPhone X 值为true
    isIphoneX: app.globalData.isIphoneX,
    //1 充值 2 订单
    coupon:0


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    var notice = options.notice;
    console.log(notice)
    that.setData({
      notice: notice


    })
    if (options.type == 1) {
      setTimeout(function () {

        wx.redirectTo({
          url: '/wxxq/view/mine/order/allOrder?index=3'
        })
      }, 2000)
    } else if (options.type == 2) {
      setTimeout(function () {

        wx.redirectTo({
          url: '/wxxq/view/break/breakfast/breakfast'
        })
      }, 2000)
    } else {
      setTimeout(function () {

        wx.redirectTo({
          url: '/wxxq/view/mine/recharge/recharge'
        })
      }, 2000)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})