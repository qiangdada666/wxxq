// wxxq/view/mine/order/allOrder.js
var c = require("../../common/common.js");

var app = getApp();

Page({
  submitInfo: function (e) { 
    var that = this;
    console.log(that)
    var formID=e.detail.formId;
    var sessionid=wx.getStorageSync("sessionId");
    var nowTimes = (new Date().getTime()+1000 * 60 * 60 * 24 * 7); //获取当前时间戳
    var web_url = app.globalData.web_url;
    console.log(web_url)
    wx.request({
      url:web_url +"/formid/saveformid",
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionid,
        // 默认值
      },
      data: {
        formId: formID,
        expire:nowTimes
      },
      success: function (e) {
       console.log("dui"+e)
      },fail:function(){
        console.log(e)
      }
      })
        
  },
  /**
   * 页面的初始数据
   */
  data: {
    // 弹框 start显示
    showModal: false,
    prompt: "确定要取消订单吗？",
    selected: false,
    selected1: false,
    selected2: false,
    selected3: true,
    selected4:false,
    noOrder:"../../../images/mine/noOrder@2x.png",
    allList: null,
    //当前页
    page:1,
    //每页数据条数
    rows:4,
    //默认显示全部订单
    // index:3,
    //酒店名称
    hotelName:'',
    //当前链接
    url:'',
    //最后页
    endpage:0,
    //orderid
    orderid:'',
    //开关
    flag : true,
    //
    fpage:3,

    //订单数
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var o = options;
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    
    // 判断会员是否登录 start
    var type = wx.getStorageSync('type');
    var userInfoTmp = app.globalData.userInfo;
    // 设置iphoneX
    c._set_iphoneX(that);
    // 设置底部
    c._set_foot(that, 2);
    if (type == 1) {
      this.setData({
        is_login: 1,
        userInfo: userInfoTmp
      })
      wx.hideLoading();
      return;
    }
    if (type == 2) {
      this.setData({
        is_login: 2
      })
    }
    //获取订单数
    c._getN(that)
 // 判断会员是否登录 end
    var hotelname = wx.getStorageSync("hotelName");
    that.setData({
      hotelName: hotelname
    })
    var web_url = app.globalData.web_url;
    //跳转订单类型素引index 0 待支付 1 待确认 2 待入住  3 全部 4已入住
    
    var v = wx.getStorageSync('Fpage');
    console.log(v)
    if(!v){
      var index = o.index;
    }else{
      var index = v;
      wx.removeStorageSync('Fpage')
    }
    if (index == 0){
      web_url = web_url + "/orders/waitPay";
      this.setData({
        selected1: false,
        selected: false,
        selected3: false,
        selected4: false,
        selected2: true,
        index: 0,
        fpage:0,
        url: web_url,
      })
    }
    if (index == 1){
      web_url = web_url + "/orders/waitConfirm";
      this.setData({
        selected: false,
        selected2: false,
        selected3: false,
        selected4: false,
        selected1: true,
        index: 1,
        fpage: 1,
        url: web_url,
      })
    }
    if (index == 2) {
      web_url = web_url + "/orders/waitCheckIn";
      that.setData({
        selected1: false,
        selected2: false,
        selected3: false,
        selected4: false,
        selected: true,
        index: 2,
        fpage: 2,
        url: web_url,
      })
    }
    if (index == 3){
      web_url = web_url + "/orders/listAll";
      this.setData({
        selected1: false,
        selected2: false,
        selected: false,
        selected3: true,
        selected4: false,
        index: 3,
        fpage: 3,
        url: web_url,
      })
    }
    if (index == 4) {
      web_url = web_url + "/orders/checkIn";
      this.setData({
        selected1: false,
        selected2: false,
        selected: false,
        selected3: false,
        selected4: true,
        index: 4,
        fpage: 4,
        url: web_url,
      })
    }
    var sessionId = wx.getStorageSync('sessionId');
    //请求对应的订单列表
    c._get({
      url: web_url,
      data: {
        page: that.data.page,
        rows: that.data.rows,
      },
      success: function (res) {
        that.check_order(res.data.data);
          that.setData({
            allList: res.data.data
          })    
      }
    })
    wx.hideLoading();
    wx.stopPullDownRefresh()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    var checkInDates = getDate.checkInDate.substring(5, 7) + "月" + getDate.checkInDate.substring(8, 11) + "日";
    var checkOutDates = getDate.checkOutDate.substring(5, 7) + "月" + getDate.checkOutDate.substring(8, 11) + "日";
    var inDate = getDate.checkInDate.substring(getDate.checkInDate.length - 2);
    var outDate = getDate.checkOutDate.substring(getDate.checkOutDate.length - 2);
    var day = outDate - inDate;
    this.setData({
      checkInDate: checkInDates,
      checkOutDate: checkOutDates,
      day: day,
      page:1,
    })
    var options = {index:3}
    this.onLoad(options)
    //获取订单数
    c._getN(this)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function (e) {
    var that =  this;
    // 全部
    if (that.data.is_login == 2 && that.data.index == 3) {
      that.setData({
        selected1: false,
        selected2: false,
        selected: false,
        selected3: true,
        selected4: false,
        allList: null,
        index: 3,
        fpage: 3,
        page: 1,
      })
      that.onLoad({ index: 3 })
    }
    // 待支付
    if (that.data.is_login == 2 && that.data.index == 0) {
      that.setData({
        selected1: false,
        selected: false,
        selected3: false,
        selected2: true,
        selected4: false,
        allList: null,
        index: 0,
        fpage: 0,
        page: 1,
      })
      that.onLoad({ index: 0 })
    }
    // 待确认
    if (that.data.is_login == 2 && that.data.index == 1) {
      that.setData({
        selected: false,
        selected2: false,
        selected3: false,
        selected1: true,
        selected4: false,
        index: 1,
        fpage: 1,
        page: 1,
        allList: null,
      })
      that.onLoad({ index: 1 })
    }
    // 待入住
    if (that.data.is_login == 2 && that.data.index == 2) {
      that.setData({
        selected1: false,
        selected2: false,
        selected3: false,
        selected: true,
        selected4: false,
        allList: null,
        index: 2,
        fpage: 2,
        page: 1,
      })
      that.onLoad({ index: 2 })
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  //滚动加载事件
  onReachBottom: function () {
    var that = this;
    var list = that.data.allList;
    var nowpage = that.data.page;//当前页
    var strpage = nowpage + 1;//下一页
    var endpage = that.data.endpage;//结尾页     连个分页都不给~`~
    if (endpage == nowpage){
      wx.showLoading({
        title: '数据加载完毕',
      }) 
      setTimeout(function(){
        wx.hideLoading();
      },2000)
      return false;
    }
    wx.showLoading({
      title: '玩命加载中',
    }) 
      c._get({
        url:that.data.url,
          data: {
            page: strpage,
            rows: that.data.rows,
            //wxcfgid:app.globalData.wxcfgid,
          },
        success: function (res) {
          var newinfo = res.data.data;
          //检测订单是否可以取消/退款策略(后台没有返回订单是否可以取消只有自己搞了)
          that.check_order(newinfo);
          if (newinfo.length < 4 || newinfo.length == 0) {
            if (newinfo.length !== 0){
              for (var i = 0; i < newinfo.length; i++) {
                list.push(newinfo[i]);
              }
              that.setData({
                allList: list,
                page: strpage,
              })
            }
            wx.hideLoading();
            wx.showLoading({
              title: '数据已加载完毕',
            })
            that.setData({
              endpage: strpage,
            })
            setTimeout(function () {
              wx.hideLoading();
            }, 2000)
            return false;
          } 
          for (var i = 0; i < newinfo.length; i++) {
            list.push(newinfo[i]);
          }
       
          that.setData({
            allList: list,
            page: strpage,
          })
          wx.hideLoading();
        }
      })

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
  //跳转订单类型素引 0 待支付 1 待确认 2 待出行  3 全部
  // 我的订单nav 切换
  selected: function (e) {
    var that = this;
    this.setData({
      selected1: false,
      selected2: false,
      selected3: false,
      selected: true,
      selected4:false,
      allList: null,
      index:2,
      fpage: 2,
      page:1,
    })
    if (that.data.is_login == 2) {
      that.onLoad({index:2})
    }
  },
  selected1: function (e) {
    var that = this;
    
    this.setData({
      selected: false,
      selected2: false,
      selected3: false,
      selected1: true,
      selected4: false,
      index:1,
      fpage: 1,
      page: 1,
      allList: null,
    })
    if (that.data.is_login == 2) {
      that.onLoad({ index: 1 })
    }
  },
  selected2: function (e) {
    var that = this;
    this.setData({
      selected1: false,
      selected: false,
      selected3: false,
      selected2: true,
      selected4: false,
      allList: null,
      index:0,
      fpage: 0,
      page: 1,
    })
    if (that.data.is_login == 2) {
      that.onLoad({ index: 0 })
    }
  },
  selected3: function (e) {
    var that = this;
    this.setData({
      selected1: false,
      selected2: false,
      selected: false,
      selected3: true,
      selected4: false,
      allList:null,
      index:3,
      fpage: 3,
      page: 1,
    })
    if (that.data.is_login == 2) {
      that.onLoad({ index: 3 })
    }
  },
  selected4: function (e) {
   
    var that = this;
    this.setData({
      allList: null,
      selected1: false,
      selected2: false,
      selected: false,
      selected3: false,
      selected4: true,
      fpage: 4,
      index: 4,
      page: 1,
    })
    if (that.data.is_login == 2) {
      that.onLoad({ index: 4 })
    }
  },
  getformID: function(e){
    
  }
  , 
  //弹框内容重定义(有不可退款策略的) 
  showDialogBtn1: function (e) {
    
    if (e.currentTarget.dataset.out !== null && e.currentTarget.dataset.out != ''){
      this.setData({
        prompt: "确定要取消订单吗(不可退款)？",
      })
    }else{
      this.setData({
        prompt: "确定要取消订单吗？",
      })
    }
    this.setData({
      showModal: true,
      orderid: e.currentTarget.dataset.orderid,
    });
  },
  //弹框内容重定义(有不可退款策略的) 
  showDialogBtn2: function (e) {
      wx.navigateTo({
        url: '/wxxq/view/mine/pay/pay?orderId=' + e.currentTarget.dataset.orderid + '&paytype=' + e.currentTarget.dataset.paytype,
      })
  },
  /*** 隐藏模态对话框1*/

  hideModal: function () {
    this.setData({
      showModal: false
    });

  },

  /*** 对话框取消按钮点击事件1*/
  onCancel: function () {
    this.hideModal();

  },
  /*** 对话框确认按钮点击事件1*/
  //取消订单接口
  onConfirm: function (e) {
    var  that = this;
    var web_url = app.globalData.web_url;
    if(that.data.flag){
      that.setData({
        flag:false
      })
      c._post({
        url: web_url + "/orders/" + that.data.orderid + "/cancelOrderByMember",
        data: {
          wxcfgid: app.globalData.wxcfgid,
        },
        success: function (e) {
          that.hideModal();
          that.onLoad({ index: 3 })
          that.setData({
            flag: true,
          })
        },
      })
    }
   
  },

  //验证订单是否可以取消
  check_order:function(e){

    var date = new Date(); //日期对象
    var  now = "";
    // 当前日期添加一天
    var nowDay = new Date();
    var now1 = date.getFullYear() + "-"; //读英文就行了
    var now2 = now + (date.getMonth() + 1) + "-"; //取月的时候取的是当前月-1如果想取当前月+1就可以了
    var now3 = now + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
    var nowDate = now1 + now2 + now3;
    nowDate =nowDate.slice(5);
     var nowHour = now + date.getHours();
     
    for(var i=0;i<e.length;i++){//以前：入住前一天可以取消，现在：入住当天18:00以前就可以取消。
      
      // var endtime = new Date(e[i].checkinTime.substr(0, 19).replace(/[-]/g, "/")).getTime();

      var endtime = new Date(e[i].checkinTime.substr(0, 19).replace(/[-]/g, "/")).getTime();
      // var nowtime = (new Date().getTime() + 60 * 60 * 24 * 1000); //获取当前时间戳
      var nowtime = (new Date()); //获取当前时间戳
      // console.log(nowHour)
      e[i].checkoutTime = e[i].checkoutTime.substring(5,10);
      e[i].checkinTime = e[i].checkinTime.substring(5,10);
      
      // console.log(e[i].checkinTime +"-----------checkinTime")
      if (nowDate == e[i].checkinTime) {
        if (nowHour < 18) {
          e[i].res = 1;
        }
      } else if (nowDate < e[i].checkinTime) {
        e[i].res = 1;
      } else {
        e[i].res = 2;
      }
      if (e[i].orderStatus == 2) {
        e[i].res = 3;
      }
    } 
  },
  cancel:function(e){

  },
  //去开票
  toInvoice: function (e) {
    console.log(e.currentTarget.dataset)
    wx.navigateTo({
      url: '/wxxq/view/invoice/home/home?orderId=' + e.currentTarget.dataset.orderid + '&price=' + e.currentTarget.dataset.price,
    })
  },
  toD:function(e){
    wx.redirectTo({
      url: '/wxxq/view/invoice/submitInvoice/submitInvoice?type=&orderId=' + e.currentTarget.dataset.orderid,
    })
  }

})