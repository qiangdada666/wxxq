// wxxq/view/mine/recharge/recharge.js
var c = require("../../common/common.js");
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowBtn: false,
    recvList: [


    ],
    idx: "",


  },
  goIndex(e) {
    let index = e.currentTarget.dataset.index;

    this.setData({
      idx: index
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    c._set_iphoneX(that);
    //充值页面初始化
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/membercoupons/coupons/fill";
    c._get({
      url: web_url1,
      data: {},
      success: function (res) {
        console.log(res);
        that.setData({
          recvList: res.data.data,
        })
      }
    })
  },

  //充值函数
  payCoupon: function (e) {
    console.log(e)
    var price = e.currentTarget.dataset.price;
    var coupon = e.currentTarget.dataset.coupon;
    var web_url = app.globalData.web_url;
    web_url = web_url + "/members/recharge";
    c._post({
      url: web_url,
      data: {
        couponId: coupon,
        money: price,
        wxcfgid: app.globalData.wxcfgid,
      },
      success: function (e) {
        console.log(e)
        if (e.data.success == true) {
          //微信支付调用
          wx.requestPayment({
            timeStamp: e.data.data['timeStamp'],
            nonceStr: e.data.data['nonceStr'],
            package: e.data.data['package'],
            signType: e.data.data['signType'],
            paySign: e.data.data['paySign'],
            success: function (res) {
              setTimeout(function () {
                wx.redirectTo({
                  url: '/wxxq/view/mine/success/success?coupon=1&notice=充值成功&type=2',
                })
              }, 2000)
            },
            fail: function (res) {
              console.log(res);
              wx.showToast({
                title: '支付失败',
                icon: 'none',
                duration: 4000
              })
              setTimeout(function () {
                wx.redirectTo({
                  url: '/wxxq/view/mine/defeat/defeat?coupon=1&notice=充值失败&type=2',
                })
              }, 2000)
            }
          })
        }
      }
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  }
  ,
  //微信支付
  wxpay: function (e) {
    var that = this;
    console.log(that.data.orderId)
    var sessionid = wx.getStorageSync("sessionId");
    wx.request({
      url: that.data.web_url + '/orders/' + that.data.orderId + '/payOrder',
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionid,
        // 默认值
      },
      data: {
        wxcfgid: app.globalData.wxcfgid,
      },
      success: function (e) {
        console.log("pay")
        if (e.data.success == true) {

        }
      }
    })
  },
})