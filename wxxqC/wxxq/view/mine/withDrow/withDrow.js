// wxxq/view/mine/withDrow/withDrow.js
var app = getApp();
Page({
  data: {
    selected: true,
    selected1: false,
    focus: false,
    inputValue: '',
    curIdx: null,
    getManey: '',
    getManey1: '',
    clearGetManey: false,
    listInfo: [
      {
        imgUrl: '../../image/home/pay/pressed@2x.png',
        curUrl: '../../image/home/pay/selected@2x.png',
      },
    ],
    // 弹框 start显示
    showModal: false,
    showShare: true,
    // 按钮切换
    isFold3: false,
    isFold4: false,
    income: '0',
    userInfo: [],
    radio: {
      selected: '../../../images/mine/pressed@2x.png',
      unselected: '../../../images/mine/selected@2x.png'
    },
    tishi: " 备注：平台统一收取5元手续费",
    tishi1: "实际到账金额：提现金额-手续费",
    prompt: "预计24小时内到账"
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu()
    var uid = app.globalData.uid;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/Dnjk/getUserInfo";
    var that = this;
    wx.request({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        uid: uid
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        that.setData({
          income: res.data.data.u_balance,
          userInfo: res.data.data,
          carname: res.data.data.u_username
        })
      }
    })
  },

  bindMoneyChange: function (e) {
    console.log('Money1发送选择改变，携带值为', e.detail.value)
    this.setData({
      money1: e.detail.value
    })
  },
  bindMoneyChange1: function (e) {
    console.log('Money2发送选择改变，携带值为', e.detail.value)
    this.setData({
      money2: e.detail.value
    })
  },
  bindCarnameChange: function (e) {
    console.log('carname发送选择改变，携带值为', e.detail.value)
    this.setData({
      carname: e.detail.value
    })
  },
  bindCarnumChange: function (e) {
    console.log('carnum发送选择改变，携带值为', e.detail.value)
    this.setData({
      carnum: e.detail.value
    })
  },
  bindBankChange: function (e) {
    console.log('bank发送选择改变，携带值为', e.detail.value)
    this.setData({
      bank: e.detail.value
    })
  },
  // 弹窗1
  showDialogBtn1: function () {
    console.log(1)
    var that = this;
    var u_id = app.globalData.uid;



    var money = this.data.money1;
    if (!money) {
      wx.showToast({
        title: '请输入提现金额',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (money < 5) {
      wx.showToast({
        title: '提现金额不能低于5元',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var isFold3 = this.data.isFold3;
    if (!isFold3) {
      wx.showToast({
        title: '请同意提现协议',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/Dnjk/setIncome";
    wx.request({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        cl_u_id: u_id,
        cl_type: 1,
        cl_money: money,
        cl_wechat: wechat,
        cl_tel: tel
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        that.setData({
          prompt: res.data.message,
          showModal: true
        })
      }
    })
  },
  // 弹窗2
  showDialogBtn2: function () {
    console.log(2)
    var that = this;
    var u_id = app.globalData.uid;
    var money = this.data.money2;
    if (!money) {
      wx.showToast({
        title: '请输入提现金额',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    if (money < 5) {
      wx.showToast({
        title: '提现金额不能低于5元',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    console.log(this.data.carname)
    var carname = this.data.carname;
    if (!carname) {
      wx.showToast({
        title: '请输入持卡人姓名',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var carnum = this.data.carnum;
    if (!carnum) {
      wx.showToast({
        title: '请输入银行卡号',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var bank = this.data.bank;
    if (!bank) {
      wx.showToast({
        title: '请输入开户银行',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var isFold4 = this.data.isFold4;
    if (!isFold4) {
      wx.showToast({
        title: '请同意提现协议',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/Dnjk/setIncome";
    wx.request({
      url: web_url1, //仅为示例，并非真实的接口地址
      data: {
        cl_u_id: u_id,
        cl_type: 2,
        cl_money: money,
        cl_car_name: carname,
        cl_car_num: carnum,
        cl_bank: bank
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        that.setData({
          prompt: res.data.message,
          showModal: true
        })
      }
    })
  },

  // /** 弹出框蒙层截断touchmove事件

  preventTouchMove: function () {

  },

  /*** 隐藏模态对话框1*/

  hideModal: function () {
    this.setData({
      showModal: false
    });

  },

  /*** 对话框取消按钮点击事件1*/
  onCancel: function () {
    this.hideModal();

  },
  /*** 对话框取消按钮点击事件2*/
  onCancelShare: function () {
    this.hideModalShare();

  },

  /*** 对话框确认按钮点击事件1*/
  onConfirm: function () {
    this.hideModal();

  },
  /*** 对话框确认按钮点击事件2*/
  onConfirmShare: function () {
    this.hideModalShare();

  },
  //  弹框 end



  chooseImg: function (e) {
    this.setData({
      curIdx: e.currentTarget.dataset.current
    })
  },
  bindButtonTap: function () {
    this.setData({
      focus: true
    })
  },
  selected: function (e) {
    this.setData({
      selected1: false,
      selected: true
    })
  },
  selected1: function (e) {
    this.setData({
      selected: false,
      selected1: true
    })
  },
  /**
  * 切换同意信息
  */
  changeAgree: function () {
    this.setData({
      isFold3: !this.data.isFold3,

    })
  },
  changeAgree1: function () {
    this.setData({
      isFold4: !this.data.isFold4,
    })
  },

  /**
   *  微信全部提现 start
   */
  allGet: function () {
    this.setData({
      getManey: this.data.income,
      clearGetManey: true
    });
  },

  /**
   * 清除输入的金额
   */
  clearGetManey: function () {
    this.setData({
      getManey: 0,
      clearGetManey: false
    });
  },
  /**
  *  银行卡全部提现 start
  */
  allGet1: function () {
    this.setData({
      getManey1: this.data.income,
      clearGetManey: true
    });
  },

  /**
   * 清除输入的金额
   */
  clearGetManey1: function () {
    this.setData({
      getManey1: 0,
      clearGetManey: false
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})