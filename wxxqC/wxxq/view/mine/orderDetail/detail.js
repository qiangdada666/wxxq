// wxxq/view/mine/orderDetail/detail.js
var c = require("../../common/common.js");
var app = getApp();
var Moment = require("../../../../utils/moment.js");
Page({

  /**
   * 页面的初始数据
   */
  data: { 
    // 弹框 start显示
    showModal: false,
    prompt: "确定要取消订单吗？",
    payStatus: "",
    time: "",
    totalprice: "",
    refund: "../../../images/mine/refund@2x.png",
    cancle: "../../../images/mine/cancle@2x.png",
    roomType: "",
    num: "1",
    limit: "",
    checkIn: "",
    leave: "",
    day: "",
    roomNum: "",
    stipulate: "入住时间14:00/退房时间12:00",
    faclityList: [],
    policiy:[
      
    ],
    resident: "",
    mobile: "",
    mailbox: "",
    orderSn: "",
    bookTime: "",
    allmsg:'',
    showtime:'',
    //余额
    summoney:0,
    //订单id
    id:null,
    //当前时间
    nowtime:0,
    //下单时间
    booktime:0,
    //
    flag:true,





  },
  submitInfo: function (e) {
    var that = this;
    console.log(that)
    var formID = e.detail.formId;
    var sessionid = wx.getStorageSync("sessionId");
    var nowTimes = (new Date().getTime() + 1000 * 60 * 60 * 24 * 7); //获取当前时间戳
    var web_url = app.globalData.web_url;
    console.log(web_url)
    wx.request({
      url: web_url + "/formid/saveformid",
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionid,
        // 默认值
      },
      data: {
        formId: formID,
        expire: nowTimes
      },
      success: function (e) {
        console.log("dui" + e)
      }, fail: function () {
        console.log(e)
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    wx.setStorage({
      key: 'Fpage',
      data: options.index,
    })
    wx.showLoading({
      title: '数据加载中.',
      mask:true,
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/orders/" + options.id;
    var sessionId = wx.getStorageSync("sessionId")
    // console.log(sessionId)
    that.setData({
      id:options.id,
    })
    //请求订单详情
    c._get({
      url: web_url1, 
      data: {
      },
      success: function(res) {
        //console.log(res)
        // start
        var date = new Date(); //日期对象
        var now = "";
        // 当前日期添加一天
        var nowDay = new Date();
        var now1 = date.getFullYear() + "-"; //读英文就行了
        var now2 = now + (date.getMonth() + 1) + "-"; //取月的时候取的是当前月-1如果想取当前月+1就可以了
        var now3 = now + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate());
        var nowDate = now1 + now2 + now3;
        nowDate = nowDate.slice(5);
        var nowHour = now + date.getHours();
        // end
        // 入住几晚
        var day = that.getDays(res.data.data.checkinTime.substring(0, 10), res.data.data.checkoutTime.substring(0, 10)) 
        var endtime = new Date(res.data.data.checkinTime.substr(0, 19).replace(/[-]/g, "/")).getTime();
        var nowtime = (new Date()); //获取当前时间戳
        res.data.data.checkoutTime = res.data.data.checkoutTime.substring(5, 10);
        res.data.data.checkinTime = res.data.data.checkinTime.substring(5, 10);
        //检测订单是否可以取消(需求改过多次,么办法,总不能一直重构代码吧)
        if (nowDate == res.data.data.checkinTime) {
          if (nowHour < 18) {
            res.data.data.res = 1;
          }
        } else if (nowDate < res.data.data.checkinTime) {
          res.data.data.res = 1;
        } else {
          res.data.data.res = 2;
        }
        if (res.data.data.orderStatus == 2) {
          res.data.data.res = 3;
        }
        if (res.data.data.payStatus == 0) {
          res.data.data.res = 1;
        }
        if (res.data.data.roomInfo.notCancle !== null && res.data.data.roomInfo.notCancle !== "") {
          //不可退款
          that.setData({
            prompt: "确定要取消订单吗(不可退款)？",
          })
        }
        var showtime = that.dateAdd(res.data.data.bookTime);
         
        //每个变量都有一个对应的,allmsg是全部的   
        that.setData({
          showtime: showtime,
          roomInfo: res.data.data.roomInfo,
          roomType: res.data.data.roomInfo.name,
          facilitiy: res.data.data.roomInfo.facilities,
          residentLimit: res.data.data.roomInfo.residentLimit,
          policiy: res.data.data.roomInfo.policies,
          orderStatus: Number(res.data.data.orderStatus),
          moneyPaid: res.data.data.moneyPaid,
          roomCount: res.data.data.roomCount,
          mobile: res.data.data.mobile,
          mailbox: res.data.data.email,
          orderSn: res.data.data.orderSn,
          roomCount: res.data.data.roomCount,
          bookTime: Moment(new Date(new Date(res.data.data.bookTime.replace(/[-]/g, "/")).getTime() + 60 * 30 * 1000)).format("YYYY-MM-DD HH:mm:ss"),
          payType: res.data.data.payType,
          resident: res.data.data.resident,
          totalprice: res.data.data.totalPrice,
          checkinTime: res.data.data.checkinTime,
          checkoutTime: res.data.data.checkoutTime,
          hours: 30,
          day:day,
          payStatus: res.data.data.payStatus,
          time: res.data.data.bookTime.substring(11, 14) + res.data.data.bookTime.substring(14, 16),
          allmsg: res.data.data,//该变量包含所有数据,直接使用该变量后面跟相应的素引即可
          nowtime:new Date().getTime(),
          booktime: (new Date(res.data.data.bookTime.replace(/[-]/g,"/")).getTime()) + 60 * 30 * 1000,
        })
        wx.hideLoading();
        // console.log(res.data.data.bookTime.replace(/[-]/g, "/"))
        // console.log(that.data.nowtime)
        // console.log(that.data.booktime)
        // console.log("time")
        // console.log(that.data.bookTime)
      }
    })

  },
  //日期计算
  dateAdd: function(startDate) {
    startDate = new Date(startDate);
    startDate = +startDate + 1000 * 60 * 60 * 24;
    startDate = new Date(startDate);
    var nextStartDate = startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate();
    return nextStartDate;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {


  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    

  },
  //日期相减得到天数
  getDays: function (stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
  showDialogBtn1: function() {
    this.setData({
      showModal: true
    });
  },

  /*** 隐藏模态对话框1*/

  hideModal: function() {
    this.setData({
      showModal: false
    });

  },

  /*** 对话框取消按钮点击事件1*/
  onCancel: function() {
    this.hideModal();

  },
  /*** 对话框确认按钮点击事件1*/
  //取消订单事件
  onConfirm: function() {
    var that = this;
    
    this.hideModal();
    var web_url = app.globalData.web_url;
    var web_url2 = web_url + "/orders/"+that.data.id+"/cancelOrderByMember";
    var sessionId = wx.getStorageSync("sessionId")
    if(that.data.flag){
      that.setData({
        flag: false,
      })
      c._post({
        url: web_url2,
        data: {
          sessionId: sessionId,
          orderId: that.data.id,
          userId: "",
          accountId: "",
        },
        success: function (e) {
          console.log(e)
          if (e.data.success == true) {
            wx.showToast({
              title: '取消订单成功',
              icon: 'none'
            })
            that.setData({
              flag:true,
            })
            setTimeout(function (e) {
              wx.navigateBack({
                delta: 1
              })
            }, 2000)
          }
        }
      })

    }
    
  },

})