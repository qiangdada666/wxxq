// wxxq/view/mine/pay/pay.js
// wxxq/view/mine/orderDetail/detail.js
var c = require("../../common/common.js");
var app = getApp()
var wxconfig = app.globalData.wxcfgid;
Page({
  submitInfo: function (e) {
    var that = this;
    var formID = e.detail.formId;
    var sessionid = wx.getStorageSync("sessionId");

    var nowTimes = (new Date().getTime() + 1000 * 60 * 60 * 24 * 7); //获取当前时间戳
    var web_url = app.globalData.web_url;
    wx.request({
      url: web_url + "/formid/saveformid",
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionid,
        // 默认值
      },
      data: {
        formId: formID,
        expire: nowTimes
      },
      success: function (e) {
       
      }, fail: function () {
        
      }
    })

  },
  /**
   * 页面的初始数据
   */
  data: {
    status: "待支付",
    time: "",
    vipPrice: "",
    originalPrice: "",
    refund: "../../../images/mine/refund@2x.png",
    cancle: "../../../images/mine/cancle@2x.png",
    roomType: "",
    num: "",
    limit: "",
    checkIn: "",
    leave: "",
    day: "",
    roomNum: "",
    stipulate: "入住时间14:00/退房时间12:00",
    faclityList: '',
    names: "",
    mobile: "",
    mailbox: "",
    orderSn: "",
    bookTime: "",
    orderPrice: "",
    originalPrice: "",
    vip: "",
    discount: "",
    totalprice: "",
    telphone: "../../../images/mine/telphone@2x.png",
    //订单id
    orderId: "",
    //优惠券
    coupon: 0,
    //余额
    balance: 0,

    //临时ip
    web_url: '',
    //支付类型
    paytype: 3,
    //余额
    summoney: 0,
    //会员价
    vipprice: 0,
    //总信息
    allmsg: '',
    //支付开关
    f : true,




  },

  // 拨打酒店电话
  // 拨打酒店电话
  calling: function () {
    var telphone1 = wx.getStorageSync('telphone1');
    var telphones = "\'" + telphone1 + "\'";
    wx.makePhoneCall({
      phoneNumber: telphones  //仅为示例，并非真实的电话号码
    })
  },
  // 到店支付下单成功提示
  go:function(){
    wx.showToast({
      title: '下单成功',
      icon:'success',
      success:function(){
        wx.redirectTo({
          url: '/wxxq/view/mine/order/allOrder?index=3'
        })
      }
    }) 
  },
  //余额支付提示(余额支付走流程,不调微信支付)
  yue: function (e) {
    var that = this;
    if (that.data.allmsg.payType == 1) {
      wx.showToast({
        title: '支付成功',
      })
      setTimeout(function () {
        wx.redirectTo({
          url: '/wxxq/view/mine/order/allOrder?index=3'
        })
      }, 2000)
      return;
    }
    var sessionid = wx.getStorageSync("sessionId");
    wx.request({
      url: that.data.web_url + '/orders/' + that.data.orderId + '/payOrder',
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionid,
        // 默认值
      },
      data: {
        wxcfgid: wxconfig,
      },
      success: function (e) {
  
        if (e.data.success == true) {

          var agree = wx.getStorageSync("sumMoney");
          wx.setStorageSync("sumMoney", Number(Number(agree) - Number(that.data.allmsg.moneyPaid)));

          wx.showToast({
            title: '支付成功',
          })
          setTimeout(function () {
            wx.redirectTo({
              url: '/wxxq/view/mine/order/allOrder?index=3'
            })
          }, 2000)
        } else {
          wx.showToast({
            title: '支付失败',
            icon: 'none',
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var paytype = options.paytype;
    var orderId = options.orderId;
    //支付显示控制
    var roomdetail_auth = app.globalData.roomdetail;
    var that = this;
    c._set_iphoneX(that);
    that.setData({
      orderId: orderId,
      paytype: paytype,
      roomdetail_auth: roomdetail_auth,

    })
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var money = wx.getStorageSync("sumMoney");
    that.setData({
      web_url: web_url,
      summoney: money,
    })
    //获取订单详情
    wx.request({//chayver
      url: web_url + '/orders/' + orderId,
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionId,
        // 默认值
      },
      method: "GET",
      success: function (res) {
        console.log(res)
        var day = that.getDays(res.data.data.checkinTime.substr(0, 10), res.data.data.checkoutTime.substr(0, 10));
        that.setData({
          checkinTime: res.data.data.checkinTime.substring(0, 10),
          checkoutTime: res.data.data.checkoutTime.substring(0, 10),
          allmsg: res.data.data,//这个变量涵盖返回值的所有变量
          orderStatus: res.data.data.orderStatus,
          originalPrice: res.data.data.roomInfo.originalPrice * res.data.data.roomCount * day,//order_amount
          moneyPaid: res.data.data.moneyPaid,
          roomCount: res.data.data.roomCount,
          mobile: res.data.data.mobile,
          mailbox: res.data.data.email,
          orderSn: res.data.data.orderSn,
          roomCount: res.data.data.roomCount,
          bookTime: res.data.data.bookTime,
          payType: res.data.data.payType,
          resident: res.data.data.resident,
          checkInDate: res.data.data.checkinTime,
          checkOutDate: res.data.data.checkoutTime,
          vipPrice: res.data.data.discountPrice * res.data.data.roomCount * day,
          coupon: options.coupon,
          roomInfo: res.data.data.roomInfo,
          roomType: res.data.data.roomInfo.name,
          facilities: res.data.data.roomInfo.facilities,
          residentLimit: res.data.data.roomInfo.residentLimit,
          policiy: res.data.data.roomInfo.policies,
          balance: res.data.data.moneyPaid,
          totalprice: res.data.data.totalPrice,//total_price
          day: day,
          vipprice: Number((Number(res.data.data.roomInfo.originalPrice) - Number(res.data.data.discountPrice)) * res.data.data.roomCount * day)//discount_price 
        })
        if (res.data.data.payType == 2) {
          that.setData({
            vipprice: Number((Number(res.data.data.roomInfo.originalPrice) - Number(res.data.data.discountPrice)) * res.data.data.roomCount)
          })
        }
      }
    })
    wx.hideLoading()


  },

  //微信支付
  wxpay: function (e) {
    var that = this;
    var sessionid = wx.getStorageSync("sessionId");
    if(that.data.f){
      that.setData({
        f:false,
      })
      wx.request({
        url: that.data.web_url + '/orders/' + that.data.orderId + '/payOrder',
        method: "POST",
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          "sessionId": sessionid,
          // 默认值
        },
        data: {
          wxcfgid: wxconfig,
        },
        success: function (e) {
          if (e.data.success == true) {
            //微信支付调用
            wx.requestPayment({
              timeStamp: e.data.data['timeStamp'],
              nonceStr: e.data.data['nonceStr'],
              package: e.data.data['package'],
              signType: e.data.data['signType'],
              paySign: e.data.data['paySign'],
              success: function (res) {
                var balance = wx.getStorageSync("sumMoney");
                balance = Number(balance) - Number(that.data.balance);
                wx.setStorageSync("sumMoney", balance);
                var notice = "支付成功";
                setTimeout(function () {
                  wx.redirectTo({
                    url: '/wxxq/view/mine/success/success?coupon=1&notice=' + notice + "&index=3&type=1",
                  })
                }, 2000)
              },
              fail: function (res) {
                var notice = "支付失败";
                setTimeout(function () {
                  wx.redirectTo({
                    url: '/wxxq/view/mine/defeat/defeat?coupon=1&notice=' + notice + "&index=3&type=1",
                  })
                }, 2000)
              }
            })
          }
        }
      })

    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {



  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
  //日期相减得到天数(日期)
  getDays: function (stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },
})