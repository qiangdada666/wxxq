// wxxq/view/mine/vip/vip.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [
      { img: "../../../images/vip/by.png", text:"一年内入住5晚即可升级为白金会员",clas:"sw0"},
      { img: "../../../images/vip/hj.png", text: "一年内白金会员入住10晚即可升级为黄金会员", clas: "sw1"},
      { img: "../../../images/vip/bj.png", text: "一年内黄金会员入住15晚即可升级为铂金会员", clas: "sw2"},
      { img: "../../../images/vip/zs.png", text: "一年内铂金会员入住20晚即可升级为钻石会员", clas: "sw3" },
      { img: "../../../images/vip/heij.png", text: "一年内入住25晚即可升级为黑金会员", clas: "sw4"}
    ],
    indicatorDots: false,
    autoplay: false,
    duration: 500,
    index:0,
    by:[
      { img: '../../../images/icon/00.png', name: '预定保留', text: '保留至19点',flag:true},
      { img: '../../../images/icon/01.png', name: '房价折扣', text: '房价9折', flag: true},
      { img: '../../../images/icon/02.png', name: '延迟退房', text: '保留至19点', flag: true},
      { img: '../../../images/icon/03.png', name: '免费洗衣', text: '24小时随时洗衣', flag: true},
      { img: '../../../images/icon/44.png', name: '欢迎水果', text: '', flag: false},
      { img: '../../../images/icon/55.png', name: '赠送早餐', text: '', flag: false},
      { img: '../../../images/icon/66.png', name: '生日礼', text: '', flag: false},
      { img: '../../../images/icon/77.png', name: '赠送夜宵', text: '', flag: false},
      { img: '../../../images/icon/88.png', name: '房间升级', text: '', flag: false},
      { img: '../../../images/icon/99.png', name: '会议室', text: '', flag: false},
    ],
    hj: [
      { img: '../../../images/icon/00.png', name: '预定保留', text: '保留至19点', flag: true },
      { img: '../../../images/icon/01.png', name: '房价折扣', text: '房价9折', flag: true },
      { img: '../../../images/icon/02.png', name: '延迟退房', text: '保留至19点', flag: true },
      { img: '../../../images/icon/03.png', name: '免费洗衣', text: '24小时随时洗衣', flag: true },
      { img: '../../../images/icon/04.png', name: '欢迎水果', text: '时令水果会员尊享', flag: true },
      { img: '../../../images/icon/55.png', name: '赠送早餐', text: '', flag: false },
      { img: '../../../images/icon/66.png', name: '生日礼', text: '', flag: false },
      { img: '../../../images/icon/77.png', name: '赠送夜宵', text: '', flag: false },
      { img: '../../../images/icon/88.png', name: '房间升级', text: '', flag: false },
      { img: '../../../images/icon/99.png', name: '会议室', text: '', flag: false },
    ],
    bj: [
      { img: '../../../images/icon/00.png', name: '预定保留', text: '保留至19点', flag: true },
      { img: '../../../images/icon/01.png', name: '房价折扣', text: '房价9折', flag: true },
      { img: '../../../images/icon/02.png', name: '延迟退房', text: '保留至19点', flag: true },
      { img: '../../../images/icon/03.png', name: '免费洗衣', text: '24小时随时洗衣', flag: true },
      { img: '../../../images/icon/04.png', name: '欢迎水果', text: '时令水果会员尊享', flag: true },
      { img: '../../../images/icon/05.png', name: '赠送早餐', text: '免费赠送一份早餐', flag: true },
      { img: '../../../images/icon/06.png', name: '生日礼', text: '生日当天入住享惊喜', flag: true },
      { img: '../../../images/icon/77.png', name: '赠送夜宵', text: '', flag: false },
      { img: '../../../images/icon/88.png', name: '房间升级', text: '', flag: false },
      { img: '../../../images/icon/99.png', name: '会议室', text: '', flag: false },
    ],
    zs: [
      { img: '../../../images/icon/00.png', name: '预定保留', text: '保留至19点', flag: true },
      { img: '../../../images/icon/01.png', name: '房价折扣', text: '房价9折', flag: true },
      { img: '../../../images/icon/02.png', name: '延迟退房', text: '保留至19点', flag: true },
      { img: '../../../images/icon/03.png', name: '免费洗衣', text: '24小时随时洗衣', flag: true },
      { img: '../../../images/icon/04.png', name: '欢迎水果', text: '时令水果会员尊享', flag: true },
      { img: '../../../images/icon/05.png', name: '赠送早餐', text: '免费赠送一份早餐', flag: true },
      { img: '../../../images/icon/06.png', name: '生日礼', text: '生日当天入住享惊喜', flag: true },
      { img: '../../../images/icon/07.png', name: '赠送夜宵', text: '午夜梦回送您一份夜宵', flag: true },
      { img: '../../../images/icon/88.png', name: '房间升级', text: '', flag: false },
      { img: '../../../images/icon/99.png', name: '会议室', text: '', flag: false },
    ],
    heij: [
      { img: '../../../images/icon/00.png', name: '预定保留', text: '保留至19点', flag: true },
      { img: '../../../images/icon/01.png', name: '房价折扣', text: '房价9折', flag: true },
      { img: '../../../images/icon/02.png', name: '延迟退房', text: '保留至19点', flag: true },
      { img: '../../../images/icon/03.png', name: '免费洗衣', text: '24小时随时洗衣', flag: true },
      { img: '../../../images/icon/04.png', name: '欢迎水果', text: '时令水果会员尊享', flag: true },
      { img: '../../../images/icon/05.png', name: '赠送早餐', text: '免费赠送一份早餐', flag: true },
      { img: '../../../images/icon/06.png', name: '生日礼', text: '生日当天入住享惊喜', flag: true },
      { img: '../../../images/icon/07.png', name: '赠送夜宵', text: '午夜梦回送您一份夜宵', flag: true },
      { img: '../../../images/icon/08.png', name: '房间升级', text: '入住尊享免费房间升级', flag: true },
      { img: '../../../images/icon/09.png', name: '会议室', text: '会议室免费预约使用', flag: true },
    ]
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      index:options.index,
    })
  },
  //轮播
  swImg:function(e){
    this.setData({
      index: e.detail.current
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  },
})