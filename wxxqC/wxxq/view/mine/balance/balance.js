// wxxq/view/mine/balance/balance.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money:"../../../images/mine/money@2x.png",
    sumMoney:"0",
    actualAmount:"0",
    presented:"0"

  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取余额实际金额与虚拟金额
    var realMoney = (Number(wx.getStorageSync('realMoney'))).toFixed(2);
    var virtMoney = (Number(wx.getStorageSync('virtMoney'))).toFixed(2);
    var sumMoney = (Number(wx.getStorageSync('sumMoney'))).toFixed(2);
    var that =this;
    //控制按钮   appglobalData 可控制
    var balance_auth = app.globalData.balance;//按钮控制
    that.setData({
      realMoney: realMoney,
      virtMoney: virtMoney,
      sumMoney: sumMoney,
      balance_auth: balance_auth,
    })
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  }
})