// wxxq/view/mine/discount/discount
var c = require("../../common/common.js");
// var time = require("../../../../utils/util.js");


var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isUsable: 1,
    noCard: "../../../images/mine/discount@2x.png",
    cardList: [

    ],
    couponType:"小程序可用"
   

  },
  userCoupon: function() {
    wx.redirectTo({
      url: '/wxxq/view/home/reserve/reserve?couponId=2&firstValue=500' 

    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
 
  onLoad: function(options) {
  
    var that = this;
    //  设置底部
    c._set_iphoneX(that);
// 获取优惠券列表接口
    var sessionId = wx.getStorageSync('sessionId');
    console.log(sessionId)
    var web_url = app.globalData.web_url;
    
    var web_url1 =web_url + "/membercoupons";

    //获取优惠券列表
    c._get({
      url: web_url1,
      data: {},
      success: function(res) {
        console.log(res);
        that.setData({
            cardList: res.data.data,   
        })
        // 优惠券截止日期计算
        var coupon_list = that.data.cardList;
        for (var i = 0; i < coupon_list.length; i++) {
          var nowtime = (new Date().getTime()) / 1000; //获取当前时间戳
          var nowtime = nowtime.toFixed(0);
          var endTime = coupon_list[i].validEnd;
          console.log(nowtime)
          var oldTime = (new Date(endTime)).getTime() / 1000;
          var nTime = (oldTime - nowtime) / 24 / 60 / 60;
          var nTime = nTime.toFixed(0);
         
          if (nTime == '-0') nTime = 1;
        
          coupon_list[i].ntime = nTime;
          console.log(nTime);
          that.setData({
            cardList: coupon_list
          })
        }
        }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
   


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var hotelname = wx.getStorageSync("hotelName");
    return {
      title: hotelname,
      path: '/wxxq/view/home/index/index',
      imageUrl: app.globalData.share_url
    }
  }
})