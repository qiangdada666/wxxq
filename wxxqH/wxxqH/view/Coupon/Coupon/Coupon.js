// wxxqH/view/Coupon/Coupon.js
var c = require("../../common/common.js");
var app =getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isshow:2,
    noCard: "../../../images/mine/discount@2x.png",
    cardList: [],

  },
  bindUser: function () {
    wx.redirectTo({
      url: '/wxxq/view/home/index/index'
    })
  },
  //优惠券跳转
  coupon:function(e){
    var firstValue = e.currentTarget.dataset.firstvalue;
    var secondValue = e.currentTarget.dataset.secondvalue;
    var couponId = e.currentTarget.dataset.couponid;
    var ntime = e.currentTarget.dataset.ntime;
    if(Number(ntime) > -1){
       wx.navigateTo({
         url: '/wxxqH/view/CouponDetail/CouponDetail/CouponDetail?firstValue=' + firstValue + '&secondValue=' + secondValue + "&couponId=" + couponId,
       })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask:true,
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url +"/usercoupons/coupons"
    // 获取优惠券列表
    var sessionId = wx.getStorageSync('sessionId');
    var o = wx.getStorageSync("hotelId")
    wx.request({
      url:web_url1,
      data:{
        hotelId:o,
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded',// 默认值
        "sessionId":sessionId, // 默认值
      },
      dataType: "json",
      method: "POST",
      success:function(res){
        that.setData({
          cardList:res.data.data,

        })

        // 优惠券截止日期计算
        var coupon_list = that.data.cardList;
        for (var i = 0; i < coupon_list.length; i++) {
          var nowtime = (new Date().getTime()) / 1000; //获取当前时间戳
          var nowtime = nowtime.toFixed(0);
          var endTime = coupon_list[i].validEnd;
          var oldTime = (new Date(endTime)).getTime() / 1000;
          var nTime = (oldTime - nowtime) / 24 / 60 / 60;
          var nTime = nTime.toFixed(0);
          console.log(nTime)
          if (nTime == '-0') nTime = 1;
          coupon_list[i].ntime = nTime;
        }
        that.setData({
          cardList: coupon_list
        })
       }

    })
     wx.hideLoading();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})