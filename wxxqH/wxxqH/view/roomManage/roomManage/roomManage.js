// wxxqH/view/roomManage/roomManage.js
var Moment = require("../../../../utils/moment.js");
var c = require("../../common/common.js");

var DATE_LIST = [];
var DATE_YEAR = new Date().getFullYear()
var DATE_MONTH = new Date().getMonth() + 1
var DATE_DAY = new Date().getDate()
var date = new Date();

var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
   
    // 弹框 start显示
    showModal: false,
    // 价格修改
    u_price:"",
    checkInDate: "2018-01-01",
    checkOutDate: "2018-01-06",
    houseType: "标准大床房",
    houseprice:"房间价格",
    footList: [],
    houseList:[],
    hotelId:"",
    roomId:"20",
    day1:"",
    nowtime:'',
    //当前时间
    showtime:null,
    //入住时间戳
    checkindate_time:null,
    //点击索引
    index:0,
    //权限
    auth:null,
    order_num:'',
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask:true,
    })
    var that = this;
    //权限
    var auth = wx.getStorageSync("auth")
    // 获取当前时间
    var day1 = new Date();//当前时间
    var showtime = (new Date().getTime() - 24*60*60*1000);
    var nowtime = "20" + Moment(new Date()).format('YY-MM-DD');
    that.setData({
      day1: day1,
      nowtime:nowtime,
      checkInDate: nowtime,
      hotelId:app.globalData.hotel,
    })
    //
    var checkindate_time = new Date(that.data.checkInDate).getTime();
    that.setData({
      checkindate_time: checkindate_time,
      showtime: showtime,
      auth:auth,
      order_num:app.globalData.num
    })
    // 设置iphoneX
    c._set_iphoneX(that);
    // 设置底部
    c._set_foot(that,2);
    
    // 设缓存缓存起来的日期
    wx.setStorage({
      key: 'ROOM_SOURCE_DATE',
      data: {
        checkInDate: nowtime,
        checkOutDate: Moment(new Date()).add(1, 'day').format('YYYY-MM-DD'),     
      }
    });
   
    // 获取房态管理接口
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/rooms/roomsAdmin";
    //房态管理数据请求
    wx.request({
      url: web_url1,
      data: {
      
        beginTimeStr: "20" + Moment(new Date()).format('YY-MM-DD')
      },
      header: {
        'content-type': 'application/json',
        "sessionId": sessionId, // 默认值
      },
      dataType: "json",
      method: "GET",
      success: function (res) {
        that.setData({
          houseList:res.data.data,
         })    
      }
    })
    that.time({type:1});
    wx.hideLoading();
    },
  switch1Change: function (e) {
   var that =this
   var able = e.currentTarget.dataset.able;
   //房间预定状态
   if(Number(able) ==  1){
     able = 0;
   }else{
     able = 1;
   }
    var index = e.currentTarget.dataset.index;
    that.setData({
      shows: e.detail.value,
      houseList: that.data.houseList    
    });
   // 房间是否可预订按钮切换
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var web_url2 = web_url + "/rooms/switchBookable"
    //更新房间预定状态
    wx.request({
      url: web_url2,
      data: {
        hotelId: that.data.hotelId,
        roomId: e.currentTarget.dataset.room,
        date:  that.data.checkInDate,
        bookable: able,
        price: e.currentTarget.dataset.price,
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionId, // 默认值
      },
      dataType: "json",
      method: "POST",
      success: function (res) {
        if(res.data.success == true){
          that.data.houseList[index].bookable = able;
          that.setData({
            showModal: false,
            houseList: that.data.houseList,
          })
        }

        //that.onLoad()
      }
    })
  
  
},
// 修改入住时间，前一天，后一天，当前时间数据动态改变
  changeDate:function(d){
    var that=this;
    var day1 = that.data.day1; //月份为0-11
    //获取时间戳
    if (d.currentTarget.dataset.type == 1){
      day1 = day1.getTime() - 1000 * 60 * 60 * 24;
      //时间段
      var time = new Date();
      var time = time.setTime(day1);
      var todays = Moment(time).format('MM-DD');
      //  获取明天的时间
      var day2 = new Date(day1);
      var m_time = day2.getTime() + 1000 * 60 * 60 * 24;
      var tomorrow = Moment(m_time).format('MM-DD');
    }else{
      day1 = day1.getTime() + 1000 * 60 * 60 * 24;
      //时间段
      var time = new Date();
      var time = time.setTime(day1);
      var todays = Moment(time).format('MM-DD');
      //  获取明天的时间
      var day2 = new Date(day1);
      var m_time = day2.getTime() + 1000 * 60 * 60 * 24;
      var tomorrow = Moment(m_time).format('MM-DD');
    }
    var yesterday = new Date();
    var nowtime =  yesterday.setTime(day1);
    //获取指定时间
    var date = new Date(nowtime);
    //拼接时间
    var strYesterday = "20" + Moment(nowtime).format('YY-MM-DD');
    var checkindate_time = new Date(strYesterday).getTime();
    that.setData({
      checkInDate: strYesterday,
      day1: date,
      todays: todays,
      tomorrow: tomorrow,
      checkindate_time: checkindate_time
    })
    that.search({ time: strYesterday})
  },
  //
  //切换时间请求
  search:function(e){
    var that = this;
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/rooms/roomsAdmin";
    wx.request({
      url: web_url1,
      data: {
        beginTimeStr: e.time
      },
      header: {
        'content-type': 'application/json',
        "sessionId": sessionId, // 默认值
      },
      dataType: "json",
      method: "GET",
      success: function (res) {
       // that.onShow();
        that.setData({
          houseList: res.data.data,
        })
      }
    })
  },
 // 获取对应的hotelId,roomId
  acquireId:function(e){
    var that = this;
    var hotelId = e.currentTarget.dataset.hotelid;
    var roomId = e.currentTarget.dataset.roomid;
    var bookable = e.currentTarget.dataset.bookable;
    that.setData({
      hotelId: hotelId,
      roomId: roomId,
      bookable: bookable,
    })
  },

  // 获取修改价格房间值
  changePrice:function(e){
    var that = this;
    var reg = /^[0-9]+(.[0-9]{1,3})?$/;
    if (!reg.test(e.detail.value)){
      wx.showToast({
        title: '请输入数字',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    that.setData({
      u_price: e.detail.value,
    })

  },
  rechange:function(){
  var that = this;
  var sessionId = wx.getStorageSync('sessionId');
  var web_url = app.globalData.web_url;
  var web_url2 = web_url + "/rooms/switchBookable"
    if (that.data.u_price == ""){
      return false;
    }
  // 房间价格修改
  wx.request({
    url: web_url2,
    data: {
      hotelId:that.data.hotelId,
      roomId:that.data.roomId,
      date: that.data.checkInDate,
      bookable:that.data.bookable,
      price: that.data.u_price,

    },
    header: {
      'content-type': 'application/x-www-form-urlencoded',
      "sessionId": sessionId, // 默认值
    },
    dataType: "json",
    method: "POST",
    success: function (res) {
      var list = that.data.houseList;
      list[that.data.index].originalPrice = that.data.u_price;
      that.setData({
        showModal: false,
        houseList: list
      })
    //  that.onLoad()
    }
  })
},


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  //选择日期后的请求
  onShow: function (e) {
    var  that  = this;
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    //时间转换成为js时间
    var date = new Date(getDate.checkInDate);
    //存data
    var checkindate_time = new Date(getDate.checkInDate).getTime();
    that.setData({
      checkindate_time: checkindate_time,
    })
    this.setData({
       checkInDate: getDate.checkInDate,
       checkOutDate: getDate.checkOutDate,
       day1: date
    })
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/rooms/roomsAdmin";
    wx.request({
      url: web_url1,
      data: {
        beginTimeStr: getDate.checkInDate
      },
      header: {
        'content-type': 'application/json',
        "sessionId": sessionId, // 默认值
      },
      dataType: "json",
      method: "GET",
      success: function (res) {
        that.setData({
          houseList: res.data.data,
        })
      }
    })
    //重构今天明天时间
    that.time({ type: 1 });
  },

  //时间处理
  time:function(e){
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    // 获取今天的时间
    var days = this.data.day1;
    days.setTime(days.getTime());
    var todays = (days.getMonth() + 1) + "-" + days.getDate();
    //  获取明天的时间
    days.setTime(days.getTime() + 24 * 60 * 60 * 1000);
    var tomorrow = (days.getMonth() + 1) + "-" + days.getDate();
    this.setData({
      //checkInDate: getDate.checkInDate,
      //checkOutDate: getDate.checkOutDate,
      todays: todays,
      tomorrow: tomorrow
    })
    days.setTime(days.getTime() - 24 * 60 * 60 * 1000);
  },



  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  // 修改房间价格
  bindPriceChange: function (e) {
    this.setData({
      u_price: e.detail.value,
    })
  },
  onShareAppMessage: function () {
  
  },
  showDialogBtn1: function (e) {
    this.setData({
      showModal: true,
      index:e.currentTarget.dataset.index,
    });
  },

  /*** 隐藏模态对话框1*/

  hideModal: function () {
    this.setData({
      showModal: false
    });

  },

  /*** 对话框取消按钮点击事件1*/
  onCancel: function () {
    this.hideModal();

  },
  /*** 对话框确认按钮点击事件1*/
  onConfirm: function () {
    this.hideModal();

  },

})
