// wxxq/view/home/login/index.js

var c = require("../../common/common.js");
var app = getApp()
Page({
  data: {
    send: false,
    alreadySend: false,
    second: 60,
    disabled: true,
    buttonType: '',
    phoneNum: '',
    code: '',
    otherInfo: '',
    mobiles: "../../../images/common/mobile@2x.png ",
    codes: "../../../images/common/security@2x.png",
    //判断是否为iPhone X 默认为值false，iPhone X 值为true
    isIphoneX: app.globalData.isIphoneX,
  },
  onLoad: function (e) {
    wx.showLoading({
      title: '加载中',
      mask:true,
    })
    var that = this;
    c._set_iphoneX(that);
    var sessionId = wx.getStorageSync('sessionId');
    wx.hideLoading();
  },

  // 手机号部分
  inputPhoneNum: function (e) {
    let phoneNum = e.detail.value
    this.setData({
      phoneNum: phoneNum
    })
    this.showSendMsg()
    this.activeButton()
  },
  //检测手机号
  checkPhoneNum: function (phoneNum) {
    let str = /^[1][3,4,5,7,8][0-9]{9}$/
    if (!str.test(phoneNum)) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none',
        duration: 2000
      });
      return false
    } else {
      return true
    }
  },

  showSendMsg: function () {
    if (!this.data.alreadySend) {
      this.setData({
        send: true
      })
    }
  },

  hideSendMsg: function () {
    this.setData({
      send: false,
      disabled: false,
    })
  },
  //发送验证码
  sendMsg: function () {
    var that = this;
    var phone = that.data.phoneNum;
    var check_res = this.checkPhoneNum(phone);
    if (!check_res) {
      send: false;
      return false;
    }
    var web_url = app.globalData.web_url;
    var sessionId = wx.getStorageSync('sessionId');


    var web_url1 = web_url + "/sms/sendvercode";
    c._post({
      url: web_url1,
      data: {
        mobile: phone,
        hotelId: app.globalData.hotel,

        type: 2,
        sign: "1",
      },
      success: function (res) {
      }
    })
    this.setData({
      alreadySend: true,
      send: false
    })
    this.timer()
  },

  timer: function () {
    let promise = new Promise((resolve, reject) => {
      let setTimer = setInterval(
        () => {
          this.setData({
            second: this.data.second - 1
          })
          if (this.data.second <= 0) {
            this.setData({
              second: 60,
              alreadySend: false,
              send: true
            })
            resolve(setTimer)
          }
        }, 1000)
    })
    promise.then((setTimer) => {
      clearInterval(setTimer)
    })
  },



  // 验证码
  addCode: function (e) {
    this.setData({
      code: e.detail.value
    })
    this.activeButton()
  },

  // 按钮
  activeButton: function () {
    let {
      phoneNum,
      code,

    } = this.data
    // 正式获取验证码时
    if (phoneNum && code) {
      // if (phoneNum) {
      this.setData({
        disabled: false,

      })
    } else {
      this.setData({
        disabled: true,

      })
    }
  },

  onSubmit: function () {
    wx.showLoading({
      title: '登录中',
      mask: true,
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url2 = web_url + "/accounts/login";
    //var sessionId = wx.getStorageSync('sessionId');
    var type = wx.getStorageSync('type')
    
    wx.login({
      success: function (res) {
    c._post({
      url: web_url2,//"https://192.168.1.127:8443/rest/accounts/login",//,
      data: {
        mobile: that.data.phoneNum,
        vercode: that.data.code,
      
        code: res.code,
      },
      success: function (res) {
        if (res.data.success == true) {
          wx.setStorageSync("request", 1)
          var auth = res.data.data.privilege;
          auth = that.check_auth(auth);
          wx.setStorageSync("auth", auth)
          wx.setStorageSync("sessionId", res.data.data.sessionId);
          // wx.setStorage({
          //   key: "type",
          //   data: res.data.data.type,
          // })
            wx.setStorage({
              key: 'accountName',
              data: res.data.data.accountName,
            })
            wx.setStorage({
              key: 'mobile',
              data: res.data.data.mobile,
            })
          app.globalData.hotel = res.data.data.hotelId;
          wx.setStorage({
            key:"hotelId",
            data: res.data.data.hotelId,
          })
          wx.setStorage({
            key: 'sex',
            data: res.data.data.sex,
          })
          wx.setStorage({
            key: 'userInfo',
            data: {
              accountName: res.data.data.accountName,
              mobile: res.data.data.mobile,
              sex: res.data.data.sex,
            }
          });
          c._get({
            url: web_url +"/orders/countWaitOrder",
            data: {

            },
            success: function (e) {
              app.globalData.num = e.data.data;
              if (e.data.data >= 100) {
                app.globalData.num = "99+";
              }
            }
          })
          wx.hideLoading()
          wx.showToast({
            title: '登录成功',
            icon: 'success'
          })
          
          setTimeout(function () {
            wx.reLaunch({
              url: '/wxxqH/view/home/index/index',
            })
          }, 1000)
        } else {
          wx.hideLoading();
          wx.showToast({
            // title: res.data.returnMessage,
            icon: 'none',
            title: "登录失败",
            icon: 'none',
            duration: 1000

          })

        }
      },
      fail: function (res) {
      }
    })
      }
    })
  },
  //检测权限
  check_auth:function(e){
    if (e.order_all == undefined ){
      e.order_all = false;
    }
    if (e.order_cancelled == undefined) {
      e.order_cancelled = false;
    }
    if (e.order_confirmed == undefined) {
      e.order_confirmed = false;
    }
    if (e.order_wait_confirm == undefined) {
      e.order_wait_confirm = false;
    }
    if (e.order_wait_pay == undefined) {
      e.order_wait_pay = false;
    }
    if (e.room_status == undefined){
      e.room_status = false;
    }
    return e;
  },
  /**
  * 生命周期函数--监听页面初次渲染完成
  */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})