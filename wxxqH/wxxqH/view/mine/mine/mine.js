
// wxxq/view/mine/mine/mine.js
var c = require("../../common/common.js");
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isIphoneX: app.globalData.isIphoneX,
    // 判断注册弹框是否出现
   
    // 弹框 start显示
    showModal: false,
    prompt: "确定要退出登录吗？",
    // 底部导航
    top: {
      iconBoy: "../../../images/mine/wd_tx@2x.png",
      iconGirl: "../../../images/mine/girl.png",
      userName: "",
      userId: "",

    },
    jiantou: "../../../images/mine/jt@2x.png",
    orderList: [
      {
        icon: "../../../images/mine/dai@2x.png",
        text: "待确认",
        url: "/wxxqH/view/orderManage/orderManage/orderManage?index=0?"
      },
      {
        icon: "../../../images/common/tab_ddgl_mr@2x.png",
        text: "待入住",
        url: "/wxxqH/view/orderManage/orderManage/orderManage?index=1"
      },
      {
        icon: "../../../images/mine/check@2x.png",
        text: "全部",
        url: "/wxxqH/view/orderManage/orderManage/orderManage?index=4"
      },
      
    

    ],
    balance: "808",
    discount: "2",
    footList: [
    ],
    arr_auth:'',
    //确认按钮权限
    right:true,
    //取消按钮权限
    cancel:true,
    //权限
    auth:'',
    order_num:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/usercoupons/countNum";
    var auth = web_url + '/accounts/privilege';
    var sessionId = wx.getStorageSync('sessionId');
    wx.request({
      url: auth,
      data: {},
      header: {
        'content-type': 'application/json',
        "sessionId": sessionId, // 默认值
      },
      dataType: "json",
      method: "POST",
      success: function (res) {
        var auth = res.data;
        auth = that.check_authNull(auth);
        wx.setStorageSync("auth", auth)
      }

    })
    //权限判断
    that.check_auth();
    // 设置iphoneX
    c._set_iphoneX(that);
    // 设置底部
    c._set_foot(that,3);
    //通计优惠券总数
    
    wx.request({
      url: web_url1,
      data:{},
      header: {
        'content-type': 'application/json',
        "sessionId":sessionId, // 默认值
      },
      dataType: "json",
      method: "GET",
      success:function(res){
        that.setData({
          num:res.data.data
        })
      }

    })
    
  //获取个人基本信息
    let getUserInfo = wx.getStorageSync("userInfo");
    this.setData({
      userName: getUserInfo.accountName,
      userId: getUserInfo.mobile,
      sex: getUserInfo.sex,
      order_num:app.globalData.num
    })
  },
  //权限规范
  check_auth:function(e){
    var that = this;
    var auth = wx.getStorageSync("auth");
    var orderList = that.data.orderList;
    if (auth.order_wait_confirm !== false){
      orderList[0].auth = auth.order_wait_confirm.list;
    }else{
      orderList[0].auth = false;
    }
    if (auth.order_confirmed !== false){
      orderList[1].auth = auth.order_confirmed.list;
    }else{
      orderList[1].auth = false;
    }
    if (auth.order_all !== false){
      orderList[2].auth = auth.order_all.list;
    }else{
      orderList[2].auth = false;
    }
    // if (auth.order_wait_pay !== false){
    //   orderList[3].auth = auth.order_wait_pay.list;
    // }else{
    //   orderList[3].auth = false;
    // }
    //orderList[3].auth = false;
    that.setData({
      orderList: orderList,
      auth:auth,
    })
    wx.hideLoading();
  },
  //检测权限
  check_authNull: function (e) {
    if (e.order_all == undefined) {
      e.order_all = false;
    }
    if (e.order_cancelled == undefined) {
      e.order_cancelled = false;
    }
    if (e.order_confirmed == undefined) {
      e.order_confirmed = false;
    }
    if (e.order_wait_confirm == undefined) {
      e.order_wait_confirm = false;
    }
    if (e.order_wait_pay == undefined) {
      e.order_wait_pay = false;
    }
    if (e.room_status == undefined) {
      e.room_status = false;
    }
    return e;
  },

  // 退出登录操作
  loginoutClick: function (e) {
    var that = this;
    userInfo: null;
    var sessionid = wx.getStorageSync('sessionId');
    that.setData({
      is_login: 1,
      showModal: false
    })
    wx.removeStorage("ROOM_H_DATE");
    var web_url = app.globalData.web_url;
    wx.request({
      url: web_url+'/accounts/logout',
      data: {},
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "sessionId": sessionid, // 默认值
      },
      dataType: "json",
      method: "POST",
      success: function (e) {
       
        wx.setStorageSync("sessionId", '');
        wx.redirectTo({
          url: '/wxxqH/view/login/login/login',
        })
      }
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // 弹窗1
  showDialogBtn: function () {
    this.setData({
      showModal: true,

    })
  },
  /*** 隐藏模态对话框1*/
  hideModal: function () {
    this.setData({
      showModal: false

    });

  },
  /*** 对话框取消按钮点击事件1*/
  onCancel: function () {
    this.hideModal();

  },
  /*** 对话框确认按钮点击事件1*/
  onConfirm: function () {
    this.hideModal();

  },
  // 获取手机验证码 start
  input_val: function (e) {
    var userphone = e.detail.value;
    this.setData({
      login_member: userphone
    })
  },
  check: function () {
    if (!this.data.get_code_status) {
      wx.showToast({
        title: '正在获取',
        icon: 'loading',
        duration: 1000
      })
      return;
    } else {
      if (this.data.login_member.length == 11) {
        var myreg = /^1\d{10}$/;
        if (!myreg.test(this.data.login_member)) {
          wx.showToast({
            title: '请输入正确的手机号',
            icon: 'loading',
            duration: 1000
          });
          return;
        } else {
          this.get_code();
        }
      } else {
        wx.showToast({
          title: '请输入完整手机号',
          icon: 'loading',
          duration: 1000
        })
        return;
      }
    }
  },
  // 获取短信验证 接口
  get_code: function () {
    var that = this;
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "sms/sendvercode";
    wx.request({
      url: 'web_url1',
      data: {
        mobile: that.data.login_member
      },
      success: function (res) {
        if (res.data.code == 1) {
          var timer = setInterval(function () {
            if (that.data.get_code_time > 0) {
              that.setData({
                get_code_time: that.data.get_code_time - 1,
                show_get_code: '剩余' + (that.data.get_code_time - 1) + '秒',
                get_code_status: false
              });
            } else {
              clearInterval(timer);
              that.setData({
                get_code_time: 60,
                show_get_code: '获取验证码',
                get_code_status: true
              });
            }
          }, 1000);
          that.setData({
            login_code: res.data.data.code       //后台返回的验证码，可以做判断用
          });
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'loading',
            duration: 1000
          });
        }
      },
      fail: function (res) {

        wx.showToast({
          title: '请求失败',
          icon: 'loading',
          duration: 1000
        });

      }

    });
  }
})