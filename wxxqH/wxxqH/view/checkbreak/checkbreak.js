// wxxqH/view/checkbreak/checkbreak.js
var c = require("../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNum:2,
    //历史
    usedarr: [
      {
        t: 0, //0：赠送  1：自购
        usedTime: '2010-10-18 09:11', //使用时间
        serialNumber: '9292187129873987',//流水号
        room:'5002',
        num:6,
        tel:'18812345678',
      },
     
    ],
    flag:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    //通计优惠券核销
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/breakfastcoupon/checkRecord";
    var sessionId = wx.getStorageSync('sessionId');
    var that = this;
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data:{
        page:1,
        row:10,
      },
      success: function (res) {
        console.log(res)
        if (res.data.success) {
          that.setData({
            usedarr:res.data.data,
            pageNum: 2,
          })
        }
      },
      fail: function (e) {

      }
    });
    wx.hideLoading();
  },
  onShow: function (options) {
    //通计优惠券核销
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/breakfastcoupon/checkRecord";
    var sessionId = wx.getStorageSync('sessionId');
    var that = this;
    c._get({
      url: web_url1, //仅为示例，并非真实的接口地址
      data:{
        page:1,
        row:10,
      },
      success: function (res) {
        console.log(res)
        if (res.data.success) {
          that.setData({
            usedarr: res.data.data,
            pageNum:2,
          })
        }
      },
      fail: function (e) {

      }
    });
  },
  change: function () {
    console.log(1)
    var that = this;
    wx.scanCode({
      onlyFromCamera: true,
      scanType: ['qrCode'],
      success: function (res) {
        var da = res.result;
        ////核销
        var web_url = app.globalData.web_url;
        var web_url2 = web_url + "/breakfastcoupon/cancelAfterVerification";
        var sessionId = wx.getStorageSync('sessionId');
        c._get({
          url: web_url2, //仅为示例，并非真实的接口地址
          data:{
            data:da,
          },
          success: function (res) {
            console.log(res)
            if (res.data.success) {
              //统计核销
              wx.showToast({
                title: '成功核销'+res.data.data+'张',
                icon: 'success',
                duration:1500,
              })
              var web_url3 = web_url + "/breakfastcoupon/checkRecord";
              var sessionId = wx.getStorageSync('sessionId');
              c._get({
                url: web_url3, //仅为示例，并非真实的接口地址
                data: {
                  page: 1,
                  row: 10,
                },
                success: function (res) {
                  console.log(res)
                  if (res.data.success) {
                    wx.pageScrollTo({
                      scrollTop: 0,
                    })
                    that.setData({
                      usedarr: res.data.data,
                      pageNum: 2,
                    })
                  }
                },
                fail: function (e) {

                }
              });
            }else{
              wx.showToast({
                title: '二维码核销失败',
                icon: 'loading',
                duration: 1500,
              })
            }
          },
          fail: function (e) {
              wx.showToast({
                title: '二维码无效',
                icon:'loading',
                duration:1500,
              })
          }
        });
        
      },
      fail: function (res) {
        // console.log(res)
      },
      complete: function (res) {
        // console.log(res)
      }

    })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var web_url = app.globalData.web_url;
    var web_url1 = web_url + "/breakfastcoupon/checkRecord";
    var sessionId = wx.getStorageSync('sessionId');
    var that = this;
    if(that.data.flag){
      that.setData({
        flag:false,
      })
      wx.showToast({
        icon:'loading',
        title: '玩命加载中',
      })
      c._get({
        url: web_url1, //仅为示例，并非真实的接口地址
        data: {
          page: that.data.pageNum,
          row: 10,
        },
        success: function (res) {
          console.log(res)
          if (res.data.success && res.data.data.length >0) {
            wx.hideToast()
            that.setData({
              usedarr: that.data.usedarr.concat(res.data.data),
              pageNum: that.data.pageNum+1,
              flag: true,
            })
          }else{
            wx.hideToast()
          }
        },
        fail: function (e) {
            wx.showToast({
              title: '无效二维码',
              duration:1500,
              icon:'loading'
            })
        }
      });
    }else{
      wx.showToast({
        icon: 'success',
        title: '已加载到底..',
      })
    }
    
  },
  
})
