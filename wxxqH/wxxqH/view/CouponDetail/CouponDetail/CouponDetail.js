// wxxqH/view/CouponDetail/CouponDetail.js
var c = require("../../common/common.js");
var app = getApp();
var g_url = app.globalData.g_url;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ewm:"../../../images/mine/ewm@2x.png",
    couponType:"满减优惠券",
    full:"600",
    discount:"20",
    reminder:"仅限微官网小程序使用",
    //判断是否为iPhone X 默认为值false，iPhone X 值为true
    isIphoneX: app.globalData.isIphoneX,
    g_url: g_url,
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //优惠券页面,可以转发  
    console.log(options)
    var that=this;
    var firstValue = options.firstValue;
    var secondValue = options.secondValue;
    var couponId = options.couponId;
   that.setData({
     firstValue: firstValue,
     secondValue: secondValue,
     couponId: couponId,
   })
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let that = this;
    var uid = wx.getStorageSync('uid');
    return {
      title: '分享的标题',
      path: '/wxxqH/view/share/share/share?id=' + that.data.couponId,
      success: function (res) {
         console.log("分享成功")
      }
    }
  }
})