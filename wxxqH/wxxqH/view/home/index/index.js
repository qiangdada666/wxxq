// wxxqH/view/home/index/index.js
var Moment = require("../../../../utils/moment.js");
var c = require("../../common/common.js");

var DATE_LIST = [];
var DATE_YEAR = new Date().getFullYear()
var DATE_MONTH = new Date().getMonth() + 1
var DATE_DAY = new Date().getDate()
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 导航nav start
    selected: true,
    selected1: false,
    serch: "../../../images/common/serch@2x.png",
    footList: [],
    // 今日 内容
    newUserTotal: "",
    newUserType: "新增用户",
    vipTotal: "",
    vipType: "新增会员用户",
    dealTotal: "",
    dealType: "成交订单",
    rechargeTotal: "",
    rechargeType: "充值金额",
    incomeTotal: "",
    incomeType: "总收入",
    // 累计内容

    addNewUserTotal: "",
    addNewUserType: "新增用户",
    addVipTotal: "",
    addVipType: "新增会员用户",
    addDealTotal: "",
    addDealType: "成交订单",
    addRechargeTotal: "",
    addRechargeType: "充值金额",
    addIncomeTotal: "",
    addIncomeType: "总收入",
    // / 入住日期start
    checkInDate: "",
    checkOutDate: "",
    //地址
    web_url: '',
    dollars1: "",
    dollars2: "",
    dollars3: "",
    dollars4: "",
    //
    order_num:'',
    // 起始、结束时间
    StartDate:"",
    StartDate2:"",
    EndDate:""
  },

  // 计算统计数据
  count: function() {


  },


  /**
   * 生命周期函数--监听页面加载
   */

  // 计算数据
  onLoad: function(options) {
    var d = DATE_YEAR + "-" + (DATE_MONTH < 10 ? "0" + DATE_MONTH : DATE_MONTH) + "-" + (DATE_DAY <10 ? "0" +DATE_DAY : DATE_DAY)
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that = this;
    that.setData({
      EndDate:d,
      checkInDate: Moment(new Date()).add(-3, 'day').format('YYYY-MM-DD'),
      checkOutDate: Moment(new Date()).format('YYYY-MM-DD')
    })
    var arr = []
    var obj = wx.getStorageSync("auth")
    for (var i in obj) {
      arr.push(obj[i]); //属性
    }
    if(wx.getStorage({
      key: '',
      success: function(res) {},
    }))
    
    
    
    //登录确认
    c._is_author({
      success: function() {
        c._is_login({
         
        })
      }
    });

      
      var web_url = app.globalData.web_url;
      that.setData({
        web_url: web_url,
        order_num:app.globalData.num,
      })
      // 设置iphoneX
      c._set_iphoneX(that);
      // 设置底部
      c._set_foot(that, 0);
      // 判断是否授权
      // 统计当天数据接口
      var web_url1 = web_url + "/dashboard/today";
      // 统计累计接口
      var web_url2 = web_url + "/dashboard/total";
      //订单数
      var web_url3 = web_url + "/orders/countWaitOrder"
      //酒店信息
    var h_Id = wx.getStorageSync("hotelId");
      var web_url4 = web_url + "/hotels/" + h_Id + "?type=1";
    var request = wx.getStorageSync("sessionId");
     // if(request){
      //酒店信息
      
      c._get({
        url: web_url4,
        data: {
          hotelId: app.globalData.hotel,
        },
        success: function (res) {
          var sD = that.Todate(res.data.data.addTime)
          
          console.log()
          that.setData({
            StartDate:sD,
            StartDate2:sD,
          })
          wx.setNavigationBarTitle({
            title: res.data.data.hotelName//页面标题为路由参数
          })

        }
      })
      
      c._get({
        url: web_url1,
        data: {},
        success: function(res) {
          that.money_change(res);
          that.setData({
            depositMoney: res.data.data.depositMoney,
            orderCount: res.data.data.orderCount,
            totalIncome: res.data.data.totalIncome,
            userCount: res.data.data.userCount,
            vipCount: res.data.data.vipCount,
          })
        }
      })
      // 订单管理的订单数
      c._get({
        url: web_url3,
        data: {
        },
        success: function (e) {

          app.globalData.num = e.data.data;
          that.setData({
            order_num: e.data.data,
          })
          if (e.data.data >= 100) {
            app.globalData.num = "99+";
            that.setData({
              order_num: "99+",
            })
          }

        }
      });
      c._get({
        url: web_url2,
        data: {
          startTime: that.data.checkInDate,
          endTime: that.data.checkOutDate,
        },
        success: function(res) {
          that.totalMoney_change(res);
          that.setData({
            depositMoneyTotal: res.data.data.depositMoney,
            orderCountTotal: res.data.data.orderCount,
            totalIncomeTotal: res.data.data.totalIncome,
            userCountTotal: res.data.data.userCount,
            vipCountTotal: res.data.data.vipCount,
          })
        }
      })
   // }
    wx.hideLoading();
  },

  format_number: function(n) {
    var b = parseInt(n).toString();
    var len = b.length + 1;
    if (len <= 3) {
      return b;
    }
    var r = len % 3;
    return r > 0 ? b.slice(0, r) + "," + b.slice(r, len).match(/\d{3}/g).join(",") : b.slice(r, len).match(/\d{3}/g).join(",");
  },
  //转换参数格式
  check_param: function(e) {

  },
  Todate: function (num) { //Fri Oct 31 18:00:00 UTC+0800 2008
    var str = "";
    num = num + "";
    var date = "";
    var month = new Array();
    month["Jan"] = 1; month["Feb"] = 2; month["Mar"] = 3; month["Apr"] = 4; month["May"] = 5; month["Jan"] = 6;
    month["Jul"] = 7; month["Aug"] = 8; month["Sep"] = 9; month["Oct"] = 10; month["Nov"] = 11; month["Dec"] = 12;
    var week = new Array();
    week["Mon"] = "一"; week["Tue"] = "二"; week["Wed"] = "三"; week["Thu"] = "四"; week["Fri"] = "五"; week["Sat"] = "六"; week["Sun"] = "日";
    str = num.split(" ");
    date = str[5] + "-";
    date = date + month[str[1]] + "-" + str[2];
    return date;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  bindDateChange:function(e){
    var web_url = app.globalData.web_url;
    var that = this;
    var web_url2 = web_url + "/dashboard/total";
    this.setData({
      checkInDate: e.detail.value,
      StartDate2:e.detail.value,
    })
    c._get({ 
      url: web_url2,
      data: {
        startTime: that.data.checkInDate,
        endTime: that.data.checkOutDate,
      },
      success: function (res) {
        that.totalMoney_change(res);
        that.setData({
          depositMoneyTotal: res.data.data.depositMoney,
          orderCountTotal: res.data.data.orderCount,
          totalIncomeTotal: res.data.data.totalIncome,
          userCountTotal: res.data.data.userCount,
          vipCountTotal: res.data.data.vipCount,
        })
      }
    })

  },
  bindDateChange2: function (e) {
    var web_url = app.globalData.web_url;
    var that = this;
    var web_url2 = web_url + "/dashboard/total";
    this.setData({
      checkOutDate: e.detail.value
    })
    c._get({
      url: web_url2,
      data: {
        startTime: that.data.checkInDate,
        endTime: that.data.checkOutDate,
      },
      success: function (res) {
        that.totalMoney_change(res);
        that.setData({
          depositMoneyTotal: res.data.data.depositMoney,
          orderCountTotal: res.data.data.orderCount,
          totalIncomeTotal: res.data.data.totalIncome,
          userCountTotal: res.data.data.userCount,
          vipCountTotal: res.data.data.vipCount,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  //选完日期返回加载页面
  onShow: function() {
    var that = this;
    that.onLoad();
    this.setData({
      checkInDate: Moment(new Date()).add(-3, 'day').format('YYYY-MM-DD'),
      checkOutDate: Moment(new Date()).format('YYYY-MM-DD')
    })
    // 统计累计接口
   
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  // nav 切换 start
  selected: function(e) {
    this.setData({
      selected1: false,
      selected: true
    })
  },
  selected1: function(e) {
    this.setData({
      selected: false,
      selected1: true
    })
  },

  //今日金额转换
  money_change: function(e) {
    var that = this;
    // if (e.data.data.vipCount > 10000) {

      if (e.data.data.vipCount > 1000) {
        // var vipCount = (e.data.data.vipCount / 10000).toFixed(1);
        var vipCount = e.data.data.vipCount;
        vipCount = vipCount;
        vipCount = that.format_number(vipCount);
        e.data.data.vipCount = vipCount;
      } else {
        // e.data.data.vipCount = (e.data.data.vipCount / 10000).toFixed(1);
        e.data.data.vipCount = e.data.data.vipCount;
      }
    // }
    // if (e.data.data.orderCount > 10000) {
      if (e.data.data.orderCount > 1000) {
        var orderCount = e.data.data.orderCount;
        orderCount = orderCount;
        orderCount = that.format_number(orderCount);
        e.data.data.orderCount = orderCount;
      } else {
        e.data.data.depositMoney = e.data.data.depositMoney;
      }
    // }
    if (e.data.data.userCount > 10000) {
      // if (e.data.data.userCount > 100000000) {
        // var userCount = (e.data.data.userCount / 10000).toFixed(1);
      var userCount = e.data.data.userCount;
        userCount = userCount;
        userCount = that.format_number(userCount);
        e.data.data.userCount = userCount;
      } else {
        
        e.data.data.userCount = e.data.data.userCount ;
      }
    // }
    if (e.data.data.depositMoney > 10000) {
      var dollars11 = "万";
      that.setData({
        dollars1: dollars11
      })

      if (e.data.data.depositMoney > 100000000) {
        var depositMoney = (e.data.data.depositMoney / 10000).toFixed(1);
        depositMoney = depositMoney.split(".");
        depositMoney[0] = that.format_number(depositMoney[0]);
        e.data.data.depositMoney = depositMoney[0] + "." + depositMoney[1]
      } else {
        e.data.data.depositMoney = (e.data.data.depositMoney / 10000).toFixed(1)
      }
    } else {
      e.data.data.depositMoney = e.data.data.depositMoney;
      var dollars11 = "元";
      that.setData({
        dollars1: dollars11
      })
    }

    if (e.data.data.totalIncome > 10000) {
      var dollars22 = "万";
      that.setData({
        dollars2: dollars22
      })
      if (e.data.data.totalIncome > 100000000) {
        var totalIncome = (e.data.data.totalIncome / 10000).toFixed(1);
        totalIncome = totalIncome.split(".");
        totalIncome[0] = that.format_number(totalIncome[0]);
        e.data.data.totalIncome = totalIncome[0] + "." + totalIncome[1]
      } else {
        e.data.data.totalIncome = (e.data.data.totalIncome / 10000).toFixed(1);
      }
    } else {
      e.data.data.totalIncome = e.data.data.totalIncome;
      var dollars22 = "元";
      that.setData({
        dollars2: dollars22
      })

    }
  },
  //累计金额转换
  totalMoney_change: function(e) {
    var that = this;
    // if (e.data.data.vipCount > 10000) {

      if (e.data.data.vipCount > 1000) {
        var vipCount =e.data.data.vipCount;
        vipCount = vipCount;
        vipCount= that.format_number(vipCount);
        e.data.data.vipCount = vipCount 
      } else {
        e.data.data.vipCount = e.data.data.vipCount;
      }
    // }
    // if (e.data.data.orderCount > 10000) {
      if (e.data.data.orderCount > 1000) {
        var orderCount = e.data.data.orderCount;
        orderCount = orderCount;
        orderCount = that.format_number(orderCount);
        e.data.data.orderCount = orderCount;
      } else {
        e.data.data.depositMoney = e.data.data.depositMoney;
      }
    // }
    // if (e.data.data.userCount > 10000) {
      if (e.data.data.userCount > 100000000) {
        var userCount = e.data.data.userCount;
        userCount = userCount;
        userCount= that.format_number(userCount);
        e.data.data.userCount = userCount;
      } else {
       
        e.data.data.userCount = e.data.data.userCount;
      }
    // }
    if (e.data.data.depositMoney > 10000) {
      var dollars33 = "万";
      that.setData({
        dollars3: dollars33
      })
      if (e.data.data.depositMoney > 100000000) {
        var depositMoney = (e.data.data.depositMoney / 10000).toFixed(1);
        depositMoney = depositMoney.split(".");
        depositMoney[0] = that.format_number(depositMoney[0]);
        e.data.data.depositMoney = depositMoney[0] + "." + depositMoney[1]
      } else {
        e.data.data.depositMoney = (e.data.data.depositMoney / 10000).toFixed(1)
      }
    } else {
      e.data.data.depositMoney = e.data.data.depositMoney;
      var dollars33 = "元";
      that.setData({
        dollars3: dollars33
      })
    }
    if (e.data.data.totalIncome > 10000) {
      var dollars44 = "万";
      that.setData({
        dollars4: dollars44
      })
      if (e.data.data.totalIncome > 100000000) {
        var totalIncome = (e.data.data.totalIncome / 10000).toFixed(1);
        totalIncome = totalIncome.split(".");
        totalIncome[0] = that.format_number(totalIncome[0]);
        e.data.data.totalIncome = totalIncome[0] + "." + totalIncome[1]
      } else {
        e.data.data.totalIncome = (e.data.data.totalIncome / 10000).toFixed(1);
      }
    } else {
      e.data.data.totalIncome = e.data.data.totalIncome;
      var dollars44 = "元";
      that.setData({
        dollars4: dollars44
      })

    }
  },
})