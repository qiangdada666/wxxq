// wxxqH/view/orderManage/orderManage.js
// wxxq/view/mine/order/allOrder.js
var c = require("../../common/common.js");

var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    informs:"33",
    // 弹框 start显示
    showModal: false,
    noOrder: "../../../images/common/noOrder@2x.png",
    prompt: "确定要取消订单吗？",
    selected: true,
    selected1: false,
    selected2: false,
    selected3: false,
    isShow:2,
    //  状态一
    allList: null,
    // 待出行列表
    unconfirmedList: null,
    // 待确认列表
    confirmedList: null,
    // 待支付列表
    cancleList: null,
    //  状态二
    // 待入住列表
   checkInList:null,

   // 待入住列表
   cancleList: null,

    // 底部导航
    footList: [],
    //订单类型索引
    index:0,
    //所有数据
    allList:null,
    //当前页
    page:1,
    endpage:0,
    //每页数据条数
    rows:4,
    //点击订单id
    orderid:0,
    //auth权限
    auth:'',
    //确认按钮
    right:true,
    //取消按钮
    cancel:true,
    //是否可以退款
    out:'',
    order_num:'',
    //取消按钮kaig
    f: true,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var that =this;
    var sessionId = wx.getStorageSync('sessionId');
    var web_url = app.globalData.web_url;
    var auth = web_url + '/accounts/privilege';
    //权限重新获取
    wx.request({
      url: auth,
      data: {},
      header: {
        'content-type': 'application/json',
        "sessionId": sessionId, // 默认值
      },
      dataType: "json",
      method: "POST",
      success: function (res) {
        var auth = res.data;
        auth = that.check_authNull(auth);
        wx.setStorageSync("auth", auth)
      }
    })
    var auth = wx.getStorageSync("auth");
    that.setData({
      auth:auth,
    })
    // 设置iphoneX
    c._set_iphoneX(that);
    // 设置底部
    c._set_foot(that,1);
    //跳转订单类型素引 0 待确认 1 已确认 2 已取消  3 全部
    
     var index = options.index;
    if (index == 0) {
      web_url = web_url + "/orders/waitConfirm";
      this.setData({
        selected1: false,
        selected: true,
        selected3: false,
        selected2: false,
        selected4: false,
        index: 0,
        endpage: 0,
        page: 1,
        url: web_url,
      })
      //权限控制
      if (that.data.auth.order_wait_confirm !== false) {
        that.setData({
          right: that.data.auth.order_wait_confirm.confirm_button == undefined ? false : true,
          cancel: that.data.auth.order_wait_confirm.cancel_button == undefined ? false : true,
        })
      } else {
        that.setData({
          right: false,
          cancel: false,
        })
      }
    }
    if (index == 1) {
      web_url = web_url + "/orders/haveBeenConfirmed";
      this.setData({
        selected: false,
        selected2: false,
        selected3: false,
        selected1: true,
        selected4: false,
        index: 1,
        page: 1,
        endpage: 0,
        url: web_url,
      })
      if (that.data.auth.confirmed !== false) {
        that.setData({
          cancel: that.data.auth.order_wait_confirm.cancel_button == undefined ? false : true,
          right:false,
        })
      } else {
        that.setData({
          right: false,
          cancel: false,
        })
      }
    }
    if (index == 2) {
      web_url = web_url + "/orders/haveBeenCancelled";
      that.setData({
        selected1: false,
        selected2: true,
        selected3: false,
        selected: false,
        selected4: false,
        index: 2,
        page: 1,
        url: web_url,
        endpage: 0,
      })
    }
    if (index == 3) {
      web_url = web_url + "/orders/waitPay";
      this.setData({
        selected1: false,
        selected2: false,
        selected: false,
        selected3: true,
        selected4: false,
        index: 3,
        page: 1,
        url: web_url,
        endpage: 0,
      })
    }
    if (index == 4) {
      web_url = web_url + "/orders/listAll";
      this.setData({
        selected1: false,
        selected2: false,
        selected: false,
        selected3: false,
        selected4: true,
        index: 4,
        endpage: 0,
        page: 1,
        url: web_url,
      })
      if (that.data.auth.order_all !== false) {
        that.setData({
          right: that.data.auth.order_all.confirm_button == undefined ? false : true,
          cancel: that.data.auth.order_all.cancel_button == undefined ? false : true,
        })
      } else {
        that.setData({
          right: false,
          cancel: false,
        })
      }
    }
    
    //请求列表数据
    c._get({
      url: web_url,
      data: {
        page: that.data.page,
        rows: that.data.rows,
      },
      success: function (res) {
        for(var i=0;i<res.data.data.length;i++){
          var day = that.getDays(res.data.data[i].checkinTime.substring(0, 10), res.data.data[i].checkoutTime.substring(0, 10));
          res.data.data[i].checkinTime = res.data.data[i].checkinTime.substring(5,10);
          res.data.data[i].checkoutTime = res.data.data[i].checkoutTime.substring(5, 10);
          res.data.data[i].daynum = day;
        }
        that.setData({
          allList: res.data.data
        })
      }
    })
    //待确认数量统一接口
    c._get({
      url: app.globalData.web_url + "/orders/countWaitOrder",
      data: {

      },
      success: function (e) {
        
        app.globalData.num = e.data.data;
        that.setData({
          order_num: e.data.data,
        })
        if (e.data.data >= 100){
          app.globalData.num = "99+";
          that.setData({
            order_num: "99+",
          })
        }

      }
    })
    wx.hideLoading();
  },
  //检测权限
  check_authNull: function (e) {
    if (e.order_all == undefined) {
      e.order_all = false;
    }
    if (e.order_cancelled == undefined) {
      e.order_cancelled = false;
    }
    if (e.order_confirmed == undefined) {
      e.order_confirmed = false;
    }
    if (e.order_wait_confirm == undefined) {
      e.order_wait_confirm = false;
    }
    if (e.order_wait_pay == undefined) {
      e.order_wait_pay = false;
    }
    if (e.room_status == undefined) {
      e.room_status = false;
    }
    return e;
  },

  //跳转订单详情
  orderDetail:function(e){
     var id = e.currentTarget.dataset.id;
     var right = e.currentTarget.dataset.right;
     var cancel = e.currentTarget.dataset.cancel;
     var status = e.currentTarget.dataset.orderstatus;
    if (Number(status) !== 2){
      wx.navigateTo({
        url: '/wxxqH/view/orderDetail/orderDetail/orderDetail?id=' + id +"&right=" + right + "&cancel="+cancel,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      var options = {
          index:this.data.index
        }
    this.onLoad(options)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  //下拉加载更多
  onReachBottom: function () {
    var that = this;
    var list = that.data.allList;
    var nowpage = that.data.page;
    var strpage = nowpage + 1;
    var endpage = that.data.endpage;
    if (endpage == strpage) {
      wx.showLoading({
        title: '没有更多了~',
      })
      setTimeout(function () {
        wx.hideLoading();
      }, 2000)
      return false;
    }
    wx.showLoading({
      title: '玩命加载中',
    })
    c._get({
      url: that.data.url,//请求地址  已处理过直接用
      data: {
        page: strpage,
        rows: that.data.rows,
        //wxcfgid: app.globalData.wxcfgid,
      },
      success: function (res) {
        var newinfo = res.data.data;
          if (newinfo.length < 4 || newinfo.length == 0) {
            if (newinfo.length !== 0){
              for (var i = 0; i < newinfo.length; i++) {
                var day = that.getDays(newinfo[i].checkinTime.substring(0, 10), newinfo[i].checkoutTime.substring(0, 10));
                //截取时间
                newinfo[i].checkinTime = newinfo[i].checkinTime.substring(5, 10);
                newinfo[i].checkoutTime = newinfo[i].checkoutTime.substring(5, 10);
                newinfo[i].daynum = day;
                list.push(newinfo[i]);
              }
              that.setData({
                allList: list,
                page: strpage,
              })
            }
            wx.hideLoading();
            wx.showLoading({
              title: '数据已加载完毕',
            })
            that.setData({
              endpage: strpage,
            })
            setTimeout(function () {
              wx.hideLoading();
            }, 2000)
            return false;
          } 
          //时间格式化
        for (var i = 0; i < newinfo.length; i++) {
          var day = that.getDays(newinfo[i].checkinTime.substring(0, 10), newinfo[i].checkoutTime.substring(0, 10));
          newinfo[i].checkinTime = newinfo[i].checkinTime.substring(5, 10);
          newinfo[i].checkoutTime = newinfo[i].checkoutTime.substring(5, 10);
          newinfo[i].daynum = day;
          list.push(newinfo[i]);
        }
        that.setData({
          allList: list,
          page: strpage,
        })
        wx.hideLoading();
      }
    })
  },


  // 我的订单nav 切换
  // 我的订单nav 切换
  selected: function (e) {
    var that = this;
    this.setData({
      selected1: false,
      selected2: false,
      selected3: false,
      selected: true,
      allList:null,
      index: 0,
      page:1,
    })
    if (that.data.auth.order_wait_confirm !== false){
      //权限赋值
       that.setData({
         right: that.data.auth.order_wait_confirm.confirm_button == undefined ? false : true,
         cancel: that.data.auth.order_wait_confirm.cancel_button == undefined ? false : true,
       })
    }else{
      that.setData({
        right: false,
        cancel: false,
      })
    }
    that.onLoad({ index: 0 })
  },
  selected1: function (e) {
    var that = this;
    this.setData({
      selected: false,
      selected2: false,
      selected3: false,
      selected1: true,
      allList: null,
      index: 1,
      page:1,
    })
    if (that.data.auth.confirmed !== false) {
      that.setData({
        cancel: that.data.auth.order_wait_confirm.cancel_button == undefined ? false : true,
      })
    } else {
      that.setData({
        right: false,
        cancel: false,
      })
    }
    that.onLoad({ index: 1 })
  },
  selected2: function (e) {
    var that = this;
    this.setData({
      selected1: false,
      selected: false,
      selected3: false,
      selected2: true,
      allList: null,
      index: 2,
      page:1,
    })
    that.onLoad({ index: 2 })
  },
  selected3: function (e) {
    var that = this;
    this.setData({
      selected1: false,
      selected2: false,
      selected: false,
      selected3: true,
      allList: null,
      index: 3,
      page:1,
    })
    that.onLoad({ index: 3 })
  },
  selected4: function (e) {
    var that = this;
    this.setData({
      selected1: false,
      selected2: false,
      selected: false,
      selected3: false,
      allList: null,
      selected4: true,
      index: 4,
      page: 1,
    })
    if (that.data.auth.order_all !== false) {
      that.setData({
        right: that.data.auth.order_all.confirm_button == undefined ? false : true,
        cancel: that.data.auth.order_all.cancel_button == undefined ? false : true,
      })
    } else {
      that.setData({
        right: false,
        cancel: false,
      })
    }
    that.onLoad({ index: 4 })
  },
  //不可退款策略提醒
  showDialogBtn1: function (e) {
    var orderid = e.currentTarget.dataset.orderid;
    var out = e.currentTarget.dataset.out;
    if (out !== undefined) {
      this.setData({
        prompt: "确认取消订单?(不可退款)"
      })
    }
    this.setData({
      showModal: true,
      orderid: orderid,
      out:out,
    });
  },

  /*** 隐藏模态对话框1*/

  hideModal: function () {
    this.setData({
      showModal: false
    });

  },

  /*** 对话框取消按钮点击事件1*/
  onCancel: function () {
    this.hideModal();

  },
  /*** 对话框确认按钮点击事件1*/
  onConfirm: function () { 
    var  that  = this;
    if(that.data.f){
      that.setData({
        f : false,
      })
      c._post({
        url: app.globalData.web_url + "/orders/" + that.data.orderid + "/cancelOrderByHotel",
        success: function (e) {
          if (e.data.success == true) {
            that.hideModal();
            that.setData({
              f: true,
            })
            that.onLoad({ index: that.data.index })
          }
        }
      })
    }
  },
  //日期相减得到天数
  getDays: function (stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },

})