// wxxqH/view/orderDetail/orderDetail.js
var c = require("../../common/common.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    houseType: "",
    status: "",
    roomtype: "",
    roomnum: "1",
    checkIn: "",
    leave: "",
    orderId: "",
    price: "",
    nightNum:"",
    person:"",
    mobile:"",
    // 弹框 start显示
    showModal: false,
    prompt:"确定要取消订单吗？",
    orderinfo:'',
    //确认按钮
    right:true,
    //取消按钮
    cancel:true,
    //确认开关
    flag:true,
    flag2:true,
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var  that  = this;
    var  id = options.id;
    var that = this;
    var right = options.right;
    var cancel = options.cancel;
      that.setData({
        right: right,
        cancel: cancel,
      })
      //订单详情
    c._get({
      url: app.globalData.web_url + "/orders/" + id,
      success:function(e){
        var day = that.getDays(e.data.data.checkinTime.substring(0, 10), e.data.data.checkoutTime.substring(0, 10));
        e.data.data.checkinTime = e.data.data.checkinTime.substring(5, 10);
        e.data.data.checkoutTime = e.data.data.checkoutTime.substring(5, 10);
        e.data.data.daynum = day;
        that.setData({
          orderinfo:e.data.data,
        })
        //订单不可取消策略提醒
        if (e.data.data.roomInfo.policies[1] !== undefined){
          that.setData({
            prompt:"确认取消订单?(不可退款)",
          })
        }
        
       // list.push(newinfo[i]);
        return false;
      }
    })
    wx.hideLoading();
  },

  //确认订单
  confirm:function(e){
    var that = this;
    var orderid = that.data.orderinfo.orderId;
    if(that.data.flag){
      that.setData({
        flag: false,
      })
      c._post({
        url: app.globalData.web_url + "/orders/" + that.data.orderinfo.orderId + "/confirm",
        success: function (e) {
          if (e.data.success == true) {
            wx.showToast({
              title: '确认订单成功',
              icon: 'none',
              duration: 2000
            })
            setTimeout(function () {
              that.hideModal();
              wx.redirectTo({
                url: '/wxxqH/view/orderManage/orderManage/orderManage?index=0',
              })
            }, 2000)
          }else{
            if (e.data.returnMessage == 1044){
              wx.showToast({
                title: '该房型已满',
                icon: 'none',
                duration: 2000
              })
            }else{
              wx.showToast({
                title: '确认订单失败',
                icon: 'none',
                duration: 2000
              })
            }
            setTimeout(function () {
              that.hideModal();
              wx.redirectTo({
                url: '/wxxqH/view/orderManage/orderManage/orderManage?index=0',
              })
            }, 2000)
          }
        }
      })
    }
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  showDialogBtn1: function (e) {
    this.setData({
      showModal: true,
     // orderid: orderid,
    });
  },

  /*** 隐藏模态对话框1*/

  hideModal: function () {
    this.setData({
      showModal: false
    });

  },

  /*** 对话框取消按钮点击事件1*/
  onCancel: function () {
    this.hideModal();

  },
  //取消事件
  /*** 对话框确认按钮点击事件1*/
  onConfirm: function () {
    var that = this;
    if(that.data.flag2){
      that.setData({
        flag2:false,
      })
      c._post({
        url: app.globalData.web_url + "/orders/" + that.data.orderinfo.orderId + "/cancelOrderByHotel",
        success: function (e) {
          if (e.data.success == true) {
            that.hideModal();
            wx.redirectTo({
              url: '/wxxqH/view/orderManage/orderManage/orderManage?index=4',
            })
          }
        }
      })
    }

  },

  //日期相减得到天数
  getDays: function (stratDateStr, endDateStr) {
    var stratDateArr, endDateArr, days;
    stratDateArr = stratDateStr.split('-');
    endDateArr = endDateStr.split('-');
    var newDateS = new Date(Date.UTC(stratDateArr[0], stratDateArr[1] - 1, stratDateArr[2]));
    var newDateE = new Date(Date.UTC(endDateArr[0], endDateArr[1] - 1, endDateArr[2]));
    days = parseInt(Math.abs(newDateE - newDateS) / 1000 / 60 / 60 / 24);
    return days;
  },

})