// wxxqH/view/common/foot.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //判断是否为iPhone X 默认为值false，iPhone X 值为true

    isIphoneX: app.globalData.isIphoneX,
    footList: [
      {
        text: "首页",
        pressImg: "../../../images/common/tab_sy_mr@2x.png",
        seleImg: "../../../images/common/tab_sy_xz@2x.png",
        url: "/wxxqH/view/home/index/index",
        active: true
      },
      {
        text: "受理订单",
        pressImg: "../../../images/common/tab_wd_mr@2x.png",
        seleImg: "../../../images/common/tzb_wd_xz@2x.png",
        url: "/wxxqH/view/mine/order/allOrder",
        active: false
      },
      {
        text: "房态管理",
        pressImg: "../../../images/common/tab_wd_mr@2x.png",
        seleImg: "../../../images/common/tzb_wd_xz@2x.png",
        url: "/wxxqH/view/mine/order/allOrder",
        active: false
      },
      {
        text: "个人中心",
        pressImg: "../../../images/common/tab_wd_mr@2x.png",
        seleImg: "../../../images/common/tzb_wd_xz@2x.png",
        url: "/wxxqH/view/mine/mine/mine",
        active: false
      }



    ]




  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let isIphoneX = app.globalData.isIphoneX;
    this.setData({
      isIphoneX: isIphoneX,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})