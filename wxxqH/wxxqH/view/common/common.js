// 判断授权
function is_author(a) {
  wx.getSetting({
    success: function (res) {
      // return;
      if (res.authSetting['scope.userInfo'] !== undefined) {
        a.success();
      } else {
        wx.redirectTo({
          url: "/wxxqH/view/home/author/author"
        })
        return;
      }
    },
  })
}
//联系酒店
function _calling() {
  wx.makePhoneCall({
    phoneNumber: '01082888',
    success: function () {
      console.log("拨打电话成功！")
    },
    fail: function () {
      console.log("拨打电话失败！")
    }
  })
}
// get请求参数
function _get(a) {
  var sessionid = wx.getStorageSync("sessionId");
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/json',
      "sessionId": sessionid, // 默认值
    },
    dataType: "json",
    method: "GET",
    success: function (e) {
      if (e.statusCode == 401){
        wx.redirectTo({
          url: '/wxxqH/view/login/login/login',
        })
      }
      a.success(e)
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// get请求参数
function _spe_get(a) {
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/json',

    },
    dataType: "json",
    method: "GET",
    success: function (e) {
      if (e.statusCode == 401) {
        wx.redirectTo({
          url: '/wxxqH/view/login/login/login',
        })
      }
      a.success(e)
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// post请求参数
function _post(a) {
  var sessionId = wx.getStorageSync("sessionId")
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/x-www-form-urlencoded',// 默认值
      "sessionId": sessionId,
    },
    dataType: "json",
    method: "POST",
    success: function (e) {
      if (e.statusCode == 401) {
        wx.redirectTo({
          url: '/wxxqH/view/login/login/login',
        })
      }
      a.success(e);
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// post请求参数
function _spe_post(a) {
  wx.request({
    url: a.url, //仅为示例，并非真实的接口地址
    data: a.data,
    header: {
      'content-type': 'application/x-www-form-urlencoded',// 默认值
    },
    dataType: "json",
    method: "POST",
    success: function (e) {
      if (e.statusCode == 401) {
        wx.redirectTo({
          url: '/wxxqH/view/login/login/login',
        })
      }
      a.success(e);
    },
    fail: function (e) {
      a.fail(e);
    }
  })
}
// 设置iphoneX
function set_iphoneX(a) {
  var isIphoneX = false;
  wx.getSystemInfo({
    success: res => {
      let modelmes = res.model;
      if (modelmes.search('iPhone X') != -1) {
        isIphoneX = true
      }
      a.setData({
        isIphoneX: isIphoneX,
      })

    }
  })
}
 // 设置底部
    function set_foot(a, b) {
     
      var footList = [
        {
          text: "首页",
          pressImg: "../../../images/common/tab_sy_mr@2x.png",
          seleImg: "../../../images/common/tab_sy_xz@2x.png",
          url: "/wxxqH/view/home/index/index",
          active: false
        },
        {
          text: "订单管理",
          pressImg: "../../../images/common/tab_ddgl_mr@2x.png",
          seleImg: "../../../images/common/tab_ddgl_xz@2x.png",
          url: "/wxxqH/view/orderManage/orderManage/orderManage?index=0",
          active: false
        },
        {
          text: "房态管理",
          pressImg: "../../../images/common/tab_ftgl_mr@2x.png",
          seleImg: "../../../images/common/tab_ftgl_xz@2x.png",
          url: "/wxxqH/view/roomManage/roomManage/roomManage",
          active: false
        },
        {
          text: "个人中心",
          pressImg: "../../../images/common/tab_wd_mr@2x.png",
          seleImg: "../../../images/common/tzb_wd_xz@2x.png",
          url: "/wxxqH/view/mine/mine/mine",
          active: false
        }
      ];
      footList[b].active = true;

      a.setData({
        footList: footList
      })
    }
// 登录
function _login(a) {
  wx.login({
    success: function (res) {
      var code = res.code;
      if (code) {
       // a.success();
        wx.redirectTo({
          url: '/wxxqH/view/login/login/login',
        })
        return;
      } else {
        console.log('获取用户登录态失败：' + res.errMsg);
        return;
      }
    }
  })
}
// 判断用户是否已登陆
function is_login(a) {

  wx.getStorage({
    key: 'sessionId',
    success: function (res) {
      wx.setStorageSync("request", 1)
      //a.success();
    },
    fail: function (res) {
      wx.setStorageSync("request", 0)
      wx.login({
        success: function (res) {
          var code = res.code;
          if (code) {
            // a.success();
            wx.redirectTo({
              url: '/wxxqH/view/login/login/login',
            })
          } else {
            console.log('获取用户登录态失败：' + res.errMsg);
            return;
          }
        }
      })
    }
  })
}
module.exports = {
  _is_author: is_author,
  _get: _get,
  _set_iphoneX: set_iphoneX,
  _post: _post,
  _set_foot: set_foot,
  _login: _login,
  _is_login: is_login,
  
}
