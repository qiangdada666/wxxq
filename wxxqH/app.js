//app.js
var app = getApp()
var c = require("./wxxqH/view/common/common.js");
App({
  globalData: {
    userInfo: null,
    isIphoneX: false,
    code: null,
    web_url: "",
    g_url: "https://miniprogram.umessage.com.cn/",
    share_url: "/wxxq/images/common/share.jpg",//转发图片
    num:'',
    hotel:"",
    wxcfgid: 438,
  },

  onLaunch: function () {
    var that = this;
    var web_url = "https://miniprogram.umessage.com.cn/uhotel/rest";
    this.globalData.web_url = web_url;
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs);
    //强制版本跟新
    const updateManager = wx.getUpdateManager()
    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调    
      console.log(res.hasUpdate)
    })
    updateManager.onUpdateReady(function () {
      // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启 
      updateManager.applyUpdate()
    })
    updateManager.onUpdateFailed(function () {
      // 新的版本下载失败    
      wx.showModal({
        title: '更新提示',
        content: '新版本下载失败',
        showCancel: false
      })
    })
  },

  onShow: function () {
    let that = this;
    wx.getSystemInfo({
      success: res => {
        // console.log('手机信息res'+res.model)
        let modelmes = res.model;
        if (modelmes.search('iPhone X') != -1) {
          that.globalData.isIphoneX = true
        }
      }
    })
  }
})
